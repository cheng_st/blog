---
layout: home

hero:
  name: 程有财
  text: This is my blog
  tagline: 技术博客
  image:
    src: /logo.png
    alt: 网站的 logo 图片
  actions:
    - theme: brand
      text: 快速查看
      link: /guide/index
    # - theme: alt
    #   text: 在 github 上查看
    #   link: https://github.com/vuejs/vitepress
features:
  - icon: ⚡️
    title: 前端
    details: vue axois router elementui...
  - icon: 🛠️
    title: 后端
    details: springboot mysql redis nginx...
  - icon: 🚀
    title: 移动端
    details: uniapp 微信小程序 ios...
---


 