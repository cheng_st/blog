
# vite+ts+vue3+axios+elementui-plus

## 1. 使用vite初步搭建

根据官网指示，vite搭建系统共5步:

官网地址：`https://vitejs.cn/vite3-cn/guide/#community-templates`

1. 在磁盘新建空白文件夹

2. cd进入文件夹，打开cmd输入指令:

```
npm create vite@latest
```

3. 根据控制台提示，输入项目名map，选择vue、ts模板

4. 进入项目根目录

```
cd map
```


5. 下载依赖启动

```
npm install

npm run dev
```


![Alt text](/images/vue/vite.jpg)



## 2. 引入route4

1. 安装router4

```
npm install vue-router@4
```

2. 在src文件下新建一个router文件夹，然后在这下面新建一个index.ts文件

```index.ts
// createWebHashHistory 是hash模式就是访问链接带有#
// createWebHistory  是history模式
import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
 
// 引入文件，动态路由
const Login = () => import("../views/Login.vue");
const Home = () => import("../views/Home.vue");
const About = () => import("../views/About.vue");
const NotFound = () => import("../views/NotFound.vue");
 
// 这里要注意一点，如下面这种写的话会报错，ts
// {
//    path: "/",
//    name: "Home",
//    component: () => import("../views/Home.vue")
//  },
 
 
const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/Home",
    name: "Home",
    component: Home,
  },
  {
    path: "/About",
    name: "About",
    component: About,
  },
  {
    path: "/:pathMatch(.*)*", // 代替vue2的通配符path: "*",
    name: "NotFound",
    component: NotFound,
  },
];
 
const router = createRouter({
    history: createWebHistory(), //history模式 这个模式就是去掉#号
	//history:createWebHashHistory() //hash模式
    routes
})
 
export default router
```

3. 在main.ts注入

```main.ts
import router from './router'

createApp(App).use(router).mount('#app')
```

4. 使用

```App.vue
<script setup lang="ts">
</script>

<template>
  <div>
    <router-view></router-view>
  </div>
</template>

<style >
</style>

```


## 3. 使用elementui-plus

1. 安装

```
npm install element-plus --save
```

2. 引用

```main.ts
// main.ts
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')
```

3. 页面使用

```Login.vue
<template>
    <div class="cont">
        <div class="div-login">
            <div class="div-login-title">后台管理系统</div>
            <div>
                <el-input v-model="account" class="w-50 m-2 input" placeholder="请输入账号" prefix-icon="User" />
            </div>
            <div>
                <el-input v-model="password" class="w-50 m-2 input" placeholder="请输入密码" prefix-icon="Unlock" />
            </div>

            <div>
                <el-button type="primary" @click="login()">登录</el-button>
            </div>
        </div>
    </div>
</template>

<style scoped>
.cont {
    height: 100vh;
    width: 100vw;
}

.div-login {
    position: absolute;
    top: 100px;
    left: 30%;
    background-color: #fff;
    border-radius: 10px;
    width: 600px;
    height: 400px;
    border: 1px solid #ddd;
    box-shadow: 5px 5px 5px #ddd;
}

.div-login-title {
    font-size: 20px;
    font-weight: bold;
    text-align: center;
    margin: 30px auto;
}

.input {
    height: 50px;
    width: 80%;
    margin: 20px auto;
}

.el-button {
    margin-top: 30px;
    width: 80%;
    height: 50px;
}
</style>

<script lang="ts" setup >
import { ref } from 'vue';

var account = ref<String>("1997")
var password = ref<String>("123")



import { useRouter } from 'vue-router'

import request from '../api/request-api'

const router = useRouter();


function login() {


    let params = {
        "account": account.value,
        "password": password.value
    }

    // 使用 asyncFunction  
    request.login(params).then(result => {
        console.log(result);  // 输出：操作成功完成  

        localStorage.setItem("tokenValue",result.others.tokenValue)
        localStorage.setItem("tokenName",result.others.tokenName)

        
        if (result.code == 0) {
            router.push({
                path: '/Home'
            })
        }
    });

}

</script>
```


## 4. 引入Axios获取数据

封装一个全局请求:

```
import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
// import { Local } from '@/utils/storage'
import { ElMessage } from 'element-plus'
// import { useUserStore } from '@/store/user'
class Api {
    instance: AxiosInstance
    config: AxiosRequestConfig

    constructor(option: AxiosRequestConfig) {
        this.config = option
        // 配置全局参数
        this.instance = axios.create(this.config)
        this.interceptors()
    }

    interceptors() {
        this.instance.interceptors.request.use(
            (config) => {
                removePending(config)

                addPending(config)

                const tokenName = localStorage.getItem("tokenName") || ''

                const tokenValue = localStorage.getItem("tokenValue") || ''

                if (tokenValue) {
                    config.headers[tokenName] = tokenValue;
                }

                // if (token) {
                //     //@ts-ignore
                //     config.headers.satoken = "0675e84d-4f44-41c1-9a1a-82cbc64f08b7"
                //     config.headers.Authorization = "0675e84d-4f44-41c1-9a1a-82cbc64f08b7"
                // }
                return config
            },
            (error) => Promise.reject(error)
        )

        this.instance.interceptors.response.use(
            (response) => {
                removePending(response.config)

                const res = response.data
                if (res.code !== 0) {
                    ElMessage.error(res.msg)
                }
                return res
            },
            (error) => {
                error.config && removePending(error.config)
                httpErrorStatusHandle(error)
                return Promise.reject(error)
            }
        )
    }
    async request<T = any>(config: AxiosRequestConfig): Promise<TResponseData<T>> {
        return this.instance.request<TResponseData<T>, TResponseData<T>>(config)
    }

    // async request<T = any>(config: AxiosRequestConfig): Promise<T,any> {
    //     return this.instance.request<T>(config)
    // }
}

const api = new Api({
    baseURL: 'http://localhost:8088/api',
    timeout: 10 * 1000,
})

export default api

/**
 * 处理异常
 * @param {*} error
 */
function httpErrorStatusHandle(error: any) {
    // const userStore = useUserStore()
    // 处理被取消的请求
    if (axios.isCancel(error)) return console.error('请求的重复请求：' + error.message)
    let message = ''
    if (error && error.response) {
        switch (error.response.status) {
            case 302:
                message = '接口重定向了！'
                break
            case 400:
                message = '参数不正确！'
                break
            case 401:
                // userStore.clearLoginInfo()
                message = '您未登录，或者登录已经超时，请先登录！'
                break
            case 403:
                message = '您没有权限操作！'
                break
            case 404:
                message = `请求地址出错: ${error.response.config.url}`
                break // 在正确域名下
            case 408:
                message = '请求超时！'
                break
            case 409:
                message = '系统已存在相同数据！'
                break
            case 500:
                message = '服务器内部错误！'
                break
            case 501:
                message = '服务未实现！'
                break
            case 502:
                message = '网关错误！'
                break
            case 503:
                message = '服务不可用！'
                break
            case 504:
                message = '服务暂时无法访问，请稍后再试！'
                break
            case 505:
                message = 'HTTP版本不受支持！'
                break
            default:
                message = '异常问题，请联系管理员！'
                break
        }
    }
    if (error.message.includes('timeout')) message = '网络请求超时！'
    if (error.message.includes('Network'))
        message = window.navigator.onLine ? '服务端异常！' : '您断网了！'

    ElMessage({
        type: 'error',
        message,
    })
}

const pendingMap = new Map()

/**
 * 储存每个请求的唯一cancel回调, 以此为标识
 * @param {*} config
 */
function addPending(config: AxiosRequestConfig) {
    const pendingKey = getPendingKey(config)
    config.cancelToken =
        config.cancelToken ||
        new axios.CancelToken((cancel) => {
            if (!pendingMap.has(pendingKey)) {
                pendingMap.set(pendingKey, cancel)
            }
        })
}

/**
 * 删除重复的请求
 * @param {*} config
 */
function removePending(config: AxiosRequestConfig) {
    const pendingKey = getPendingKey(config)
    if (pendingMap.has(pendingKey)) {
        const cancelToken = pendingMap.get(pendingKey)
        // 如你不明白此处为什么需要传递pendingKey可以看文章下方的补丁解释
        cancelToken(pendingKey)
        pendingMap.delete(pendingKey)
    }
}

/**
 * 生成唯一的每个请求的唯一key
 * @param {*} config
 * @returns
 */
function getPendingKey(config: AxiosRequestConfig) {
    let { url, method, params, data } = config
    if (typeof data === 'string') data = JSON.parse(data) // response里面返回的config.data是个字符串对象
    return [url, method, JSON.stringify(params), JSON.stringify(data)].join('&')
}



// 后端统一返回数据模型
export type TResponseData<T> = {
    code: 0 | number // 0 => ok
    msg: string
    data: T,
    others: any
}

// 后端分页数据模型
export type PageData<T = any> = {
    total: 0 | number
    records: Array<T>
}
```


## 5. 使用示例

在新建一个统一管理api

然后在每个页面引入后直接使用就行：

```

<script lang="ts" setup>

import { reactive, onMounted } from 'vue'

// var dataList: Array<String> = [];


var dataList = reactive([]) as any[];//现在setup中定义


import request from '../api/request-api'

// 创建完成(可以读取dom)
onMounted(() => {
    console.log("创建完成");

    queryData()
});

const queryData = async () => {

    let params = {
        "roleId": 1,
        "account": "1997"
    }

    request.listManagerMenu(params).then(result => {
        console.log("🚀 ~ file: Menu.vue:76 ~ request.listManagerMenu ~ result:", result)
        result.data.map((item: any) => {
            console.log(item);
            dataList.push(item)
        })
    })

}

</script>
  
```

```request-api.ts
import http from '../common/HttpApi'
// import { PageData } from '@/types/global/request'
// import { UserInfo } from '@/types/user'

class RequestApi {


    /**
     * 
     * @param params 登录管理系统
     * @returns 
     */
    login(params: object) {
        return http.request({
            url: '/manager/login-manager',
            method: 'POST',
            data: params
        })
    }

    /**
     * 查询账号的菜单权限
     * @param params 
     * @returns 
     */
    listManagerMenu(params: object) {
        return http.request({
            url: '/menu/list-manager-menu',
            method: 'POST',
            data: params
        })
    }

    /**
     * 获取分页表格数据
     * @param params 查询参数
     * @returns
     */
    // getPageData(params: object) {
    //     return http.request<PageData<UserInfo>>({
    //         url: '/sys/userInfo/page',
    //         method: 'GET',
    //         params,
    //     })
    // }

    listManager(params: object) {
        return http.request({
            url: '/manager/list',
            method: 'POST',
        })
    }

    /**
     * 根据id删除
     * @param id id
     * @returns
     */
    removeById(id: string) {
        return http.request({
            url: '/sys/userInfo/removeById/' + id,
            method: 'POST',
        })
    }

    /**
     * 保存接口
     * @param params 保存参数
     * @returns
     */
    saveOrUpdate(params: object) {
        return http.request({
            url: '/sys/userInfo/saveOrUpdate',
            method: 'POST',
            data: params,
        })
    }
}

const requestApi = new RequestApi()

export default requestApi
```



## 7. vue3生命周期

```
import { reactive, onBeforeMount ,onMounted } from 'vue'


// 创建之前(无法读取dom)
onBeforeMount(() => {
  console.log("创建之前");
});
// 创建完成(可以读取dom)
onMounted(() => {
  console.log("创建完成");
});
// 更新之前(读取更新之前的dom)
onBeforeUpdate(() => {
  console.log("更新之前");
});
// 更新完成(读取更新之后的dom)
onUpdated(() => {
  console.log("更新完成");
});
// 卸载之前
onBeforeUnmount(() => {
  console.log("卸载之前");
});
// 卸载完成
onUnmounted(() => {
  console.log("卸载完成");
});
// 收集依赖
onRenderTracked((e) => {
  console.log(e);
});
 
// 更新依赖
onRenderTriggered((e) => {
  console.log(e);
});
```


## 8. 菜单显示

```
<el-row class="tac">
	<el-col :span="24">
		<!-- <h5 class="mb-2">Default colors</h5> -->
		<el-menu default-active="0" class="el-menu-vertical-demo">
			<div v-for="(menu, index)  in dataList" :key="index">
				<!--只有一级菜单-->
				<el-menu-item v-if="!menu.children" :index="index + ''" @click="clickItem(menu.menu_url)">
					<el-icon>
						<component :is="menu.icon"></component>
					</el-icon>
					{{ menu.menu_name }}
				</el-menu-item>
				<!-- 多级菜单 -->
				<el-sub-menu v-else :index="index + ''">
					<template #title>
						<el-icon>
							<component :is="menu.icon"></component>
						</el-icon>
						<span>{{ menu.menu_name }}</span>
					</template>
					<el-menu-item v-for="(subMenu, subIndex) in menu.children" :index="subMenu.menu_id + ''" :key="subIndex"
						@click="clickItem(subMenu.menu_url)">
						<el-icon>
							<component :is="subMenu.icon"></component>
						</el-icon>
						{{ subMenu.menu_name }}
					</el-menu-item>
				</el-sub-menu>
			</div>
		</el-menu>
	</el-col>
</el-row>
```

后台处理好数据格式：
```
{
	"code": 0,
	"msg": "成功",
	"data": [
		{
			"id": 336,
			"role_id": "1",
			"menu_id": "43",
			"parent_id": "0",
			"menu_url": "home",
			"menu_name": "首页",
			"icon": "el-icon-s-home",
			"children": null,
			"order": "0",
			"delete_flag": "0"
		},
		{
			"id": 337,
			"role_id": "1",
			"menu_id": "1",
			"parent_id": "0",
			"menu_url": "/",
			"menu_name": "系统管理",
			"icon": "el-icon-s-tools",
			"children": [
				{
					"id": 338,
					"role_id": "1",
					"menu_id": "3",
					"parent_id": "1",
					"menu_url": "managerList",
					"menu_name": "账户管理",
					"icon": "el-icon-user-solid",
					"children": null,
					"order": "1",
					"delete_flag": "0"
				},
				{
					"id": 339,
					"role_id": "1",
					"menu_id": "2",
					"parent_id": "1",
					"menu_url": "rolesList",
					"menu_name": "角色管理",
					"icon": "el-icon-s-custom",
					"children": null,
					"order": "2",
					"delete_flag": "0"
				},
				{
					"id": 340,
					"role_id": "1",
					"menu_id": "4",
					"parent_id": "1",
					"menu_url": "menuList",
					"menu_name": "菜单管理",
					"icon": "el-icon-menu",
					"children": null,
					"order": "3",
					"delete_flag": "0"
				}
			],
			"order": "2",
			"delete_flag": "0"
		},
		{
			"id": 341,
			"role_id": "1",
			"menu_id": "44",
			"parent_id": "0",
			"menu_url": "/",
			"menu_name": "日志",
			"icon": "el-icon-notebook-1",
			"children": [
				{
					"id": 342,
					"role_id": "1",
					"menu_id": "47",
					"parent_id": "44",
					"menu_url": "loginLogList",
					"menu_name": "登录日志",
					"icon": "el-icon-tickets",
					"children": null,
					"order": "1",
					"delete_flag": "0"
				}
			],
			"order": "3",
			"delete_flag": "0"
		}
	],
	"others": null
}
```

 