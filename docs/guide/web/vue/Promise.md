
# Promise使用示例

return new Promise 是一种在 JavaScript 中创建并处理异步操作的方式。Promise 对象代表一个异步操作的最终完成（或失败）及其结果值。

下面是一个基本的使用示例：

```javascript
function asyncFunction() {  
    return new Promise((resolve, reject) => {  
        // 这是一个异步操作，可能需要一些时间来完成  
        setTimeout(() => {  
            // 异步操作完成，我们将其结果值传递给 resolve 函数  
            resolve("操作成功完成");  
			
			
			// 操作失败
			reject("操作失败");
        }, 1000);  
    });  
}  
```


  
```javascript

// 使用 asyncFunction  
asyncFunction().then(result => {  
    console.log(result);  // 输出：操作成功完成  
}).catch(error => {  
    console.error(error);  // 输出：错误信息，如果 Promise 被 reject 的话  
});
```
在这个示例中，asyncFunction 是一个返回 Promise 的异步函数。在 Promise 的构造函数中，我们传入了一个函数，该函数接收两个参数：resolve 和 reject，这些都是函数。我们调用 setTimeout 来模拟一个异步操作，当这个操作完成时，我们调用 resolve 函数，并将操作的结果作为参数传递。如果在执行过程中出现错误，我们可以调用 reject 函数并传递错误信息。

然后，我们使用 .then 和 .catch 方法来处理 Promise 的结果。.then 方法用于处理 Promise 成功的结果，.catch 方法用于处理 Promise 被拒绝的情况。