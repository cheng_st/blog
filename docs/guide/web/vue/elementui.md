
# hbuilderx新建项目，选择vue2

## 1. 使用router

```
npm i vue-router@3.5.2 -S
```


src下创建router目录，在里面新建index.js

```JavaScript
   /* 1.导入Vue和VueRouter的包 */
   import Vue from 'vue'
   import VueRouter from 'vue-router'
   
   /* 导入需要的组件 */
   import Main from '@/pages/Main.vue'
   import Login from '@/pages/Login.vue'
   
   
   /* 2.调用Vue.use()函数，把VueRouter安装为Vue的插件 */
   Vue.use(VueRouter)
   
   /* 3.创建路由实例对象 */
   const router = new VueRouter({
   	routes: [
   		// 路由规则
   		{
   			path: '/login',
   			component: Login
   		},
   		{
   			path: '/main',
   			component: Main
   		}
   	]
   })
   
   /* 为router实例对象，声明全局前置导航守卫 */
   /* 只要发生了路由的跳转，都会触发回调函数 */
   router.beforeEach(function(to, from, next) {
   	// to表示将要访问的路由的信息对象
   	// from表示将要离开的路由信息对象
   	// next()函数表示放行的意思
   /* 分析：
   1.拿到用户将要访问的hash地址
   2.判断hash地址是否等于/main，
   2.1如果等于/main，证明需要登录之后才能访问成功
   2.2如果不等于/main，则不需要登录，直接放行next（）
   3.如果访问的地址是/main，则需要读取localStorage中的token值
   3.1如果有token，则放行
   3.2如果没有token则强制跳转到/login登录页 */
   if (to.path === '/main') {
   	/* 要访问后台主页，需要判断是否有token */
   	const token = localStorage.getItem('token')
   	if (token) {
   		next()
   	} else {
   		// 没有登录跳转login页面
   		// next('/login')
   		next()
   	}
   } else {
   	next()
   }
   })
   
      /* 4.向外共享router */
      export default router
   
```

src/main.js 入口文件中，导入并挂载路由模块.

```main.js
import router from '@/router'

new Vue({
     render: h => h(App),
     router,
   }).$mount('#app')
```

## 2. 使用elementui

引入elementui   main.js中导入

```Plain Text
npm i element-ui -S

```

```main.js

import ElementUI from 'element-ui'; 

import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
```

## 3. 使用axios

```
npm install axios
```


```main.js
import axios from 'axios' // 导入 axios

import {
	getRequest,
	postRequest
} from './common/requestHttp.js'
Vue.prototype.$getRequest = getRequest;
Vue.prototype.$postRequest = postRequest;

Vue.use(axios)

```


## 4. 封装requestHttp

新建请求文件  /src/common/requestHttp.js 

```js
import axios from 'axios'

import ElementUI from 'element-ui';
import {
	Loading
} from 'element-ui';

let loading;

axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('sessionId')
// import qs from 'qs'
axios.defaults.withCredentials = true; //	允许跨域
axios.interceptors.request.use(config => {
	let append = document.getElementsByName('body')
	append.innerHTML = '<img style="position:fixed;\n' +
		' left:47%;\n' +
		' top:40%;\n' +
		' transform: translateY(-50%),translateX(-50%);"' +
		' src="../../static/img/a.jpg"/>'
	return config
}, err => {
	return Promise.resolve(err)
})

let base = "http://localhost:8099/api/" // 接口域名

export const request = (url, params, method, Func, isJson) => {
	// console.log("请求的接口--->" + base + url);
	// console.log("请求的方法--->" + method);
	// console.log("请求格式--->" + isJson);
	// console.log("请求的参数--->" + JSON.stringify(params));
	startLoding()
	var _this = this;
	axios({
		method: method,
		url: `${base}${url}`,
		data: method === 'post' ? params : '',
		transformRequest: [function(data) {
			if (isJson === 1) {
				// console.log("判断是否json格式或者是表单提交形式");
				// debugger       // 判断是否json格式或者是表单提交形式
				return JSON.stringify(data)
			}
			let ret = ''
			for (let it in data) {
				ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
			}
			return ret // 便于直接取到内部data
		}],
		headers: {
			// 认证和请求方式
			'Content-Type': isJson === 1 ? 'application/json' : 'application/x-www-form-urlencoded',
			'token': sessionStorage.getItem('token'),
			'Authorization': sessionStorage.getItem('sessionId'),
		},
		params: method === 'get' ? params : '',
	}).then(data => {
		endLoading()
		// console.log("返回结果：----》" + JSON.stringify(data.data));
		if (data.data.code == 0) {
			Func(data.data)
		} else {
			//	错误提示
			ElementUI.Message({
				message: data.data.msg,
				type: 'error'
			});
		}
	})
}
// uploadFileRequest  图片上传
export const uploadFileRequest = (url, params) => {
	return axios({
		method: 'post',
		url: `${base}${url}`,
		data: params,
		headers: {
			'Content-Type': 'multipart/form-data',
			'token': localStorage.getItem('token')
			// 'authorization':'admin',
			// 'token':'740a1d6be9c14292a13811cabb99950b'
		}
	})
}

// get 

export const getRequest = (url, params, Func, isJson) => {
	request(url, params, 'get', Func, isJson)
}

// post
export const postRequest = (url, params, Func, isJson) => {
	request(url, params, 'post', Func, isJson)
}
function startLoding() {
	loading = Loading.service({
		lock: true,
		text: '加载中...',
		spinner: 'el-icon-loading',
	});
}

function endLoading(){
	loading.close()
}
```

​

