# 使用uniapp上传视频




## 1. 前端uniapp代码:

```Shell
uni.chooseVideo({
					sourceType: ['camera', 'album'],
					success: function(res) {
						let src = res.tempFilePath;

						console.log(src);
						uni.uploadFile({
							url: 'http://192.168.3.5:8081/befapi/services/uploadVideo.do', //仅为示例，非真实的接口地址
							filePath: src,
							fileType: "video",
							name: 'multipartFile',
							formData: {
								'gsxx_id': '2022',
								"tb_mc": that.tb_mc,
								"tb_id": that.tb_id,
								"shouji": "app",
								"houzhui": "mp4",
							},
							success: (uploadFileRes) => {
								console.log(uploadFileRes.data);
							},
							fail(res) {
								console.log(res);
							},
							complete(res) {
								console.log(res);
							}
						});
					}
				});
```






## 2. 后端接受数据接口:

```Shell
@RequestMapping(value = "uploadVideo.do", method = RequestMethod.POST)
    @DataSourcelAnnotation.Action(flag = "false")
    public Map uploadVideo(Video video) throws IOException {

        MultipartFile multipartFile = video.getMultipartFile();
        String originalFilename = multipartFile.getOriginalFilename();

        String gsxxId = video.getGsxx_id();
        System.out.println(gsxxId);

        String videoPath = "D:" + File.separator + "video" + File.separator + "bef" + gsxxId;
        File file = new File(videoPath);
        //如果文件夹不存在
        if (!file.exists()) {
            //创建文件夹
            file.mkdir();
        }

        String filePath = "D:" + File.separator + "video" + File.separator + "bef" + gsxxId + File.separator + originalFilename;

        File file1 = new File(filePath);

        multipartFile.transferTo(file1);

        return null;
    }
```


