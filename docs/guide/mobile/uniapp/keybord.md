###        🔖**因为项目需要一个字母+数字显示的全键盘，本着不造轮子的想法，找遍了插件市场，也没发现一个适合的，那就自己写一个好了。**



## 一. 📊效果图

#### ios效果图

![ios效果图](../../../images/vue/keybord/k1.jpg)

#### 安卓效果图

 ![安卓效果图](../../../images/vue/keybord/image.png)


## 二. 实现思路

### 一.  📚实现步骤，初步展示键盘模样



在项目的指定文件夹在新建一个vue文件

![Alt text](../../../images/vue/keybord/image1.png)



写一个view，给他一个样式，定义组件名后添加一个显示的方法.

![Alt text](../../../images/vue/keybord/image2.png)



然后在项目的其他页面直接通过前面定义好的名字使用这个组件，通过show(）方法即可展示.

![Alt text](../../../images/vue/keybord/image3.png)

![Alt text](../../../images/vue/keybord/image4.png)


通过自定义样式，整体轮廓已经出来了

![](../../../images/vue/keybord/image5.png) 

![Alt text](../../../images/vue/keybord/image6.png) 

![Alt text](../../../images/vue/keybord/k3.jpg) 



然后在view上添加一个点击事件clickCode，获取点击到的每个字母的值

再获取到值以后传到父组件上

![Alt text](../../../images/vue/keybord/image7.png) 






在父组件上绑定一个监听事件，就可以同步获取到子组件点击的值了

![Alt text](../../../images/vue/keybord/image8.png) 

![Alt text](../../../images/vue/keybord/image9.png) 

![Alt text](../../../images/vue/keybord/image10.png)


### 二.  📚实现删除和大小写转换功能

#### 1. 先找两个图标放上去，然后设置点击事件

![Alt text](../../../images/vue/keybord/k2.png)

![Alt text](../../../images/vue/keybord/k4.png)

![Alt text](../../../images/vue/keybord/image11.png)


#### 2. 删除的方法只需要将得到的数字减去最后一位然后传到父级去就好了。

```JavaScript
delCode() {
				this.showText = this.showText.slice(0, this.showText.length - 1)
				console.log(this.showText);
				this.$emit('listenKeyboard', this.showText)
			},
```


#### 3. 大写转换更简单，定义一个全局变量cutFlag，然后直接写死完事

```JavaScript
cut() { //	切换大小写
				if (!this.cutFlag) {
					this.letter1 = ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"]
					this.letter2 = ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L']
					this.letter3 = ['Z', 'X', 'C', 'V', 'B', 'N', 'M']

					this.cutFlag = true;
					return
				}
				if (this.cutFlag) {
					this.letter1 = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p']
					this.letter2 = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l']
					this.letter3 = ['z', 'x', 'c', 'v', 'b', 'n', 'm']

					this.cutFlag = false;
					return
				}
			}
```








## 二. 📚源码



#### 1. 子组件源码

```JavaScript
<template>
	<view>
		<view class="view" v-if="showFlag">
			<!-- 符号 -->
			<view class="view-code" v-if="showCharLetter">
				<view v-for='item in charletter1' @click="clickCode(item)">
					{{item}}
				</view>
			</view>
			<view class="view-code" v-if="showCharLetter">
				<view v-for='item in charletter2' @click="clickCode(item)">
					{{item}}
				</view>
			</view>

			<!-- 数字 -->
			<view class="view-code">
				<view v-for='index in 10' :key='index' @click="clickCode(index -1)">
					{{index -1}}
				</view>
			</view>

			<!-- 字母 -->
			<view class="view-code">
				<view v-for='item in letter1' @click="clickCode(item)">
					{{item}}
				</view>
			</view>

			<view class="view-code">
				<view v-for='item in letter2' @click="clickCode(item)">
					{{item}}
				</view>
			</view>

			<view class="view-code">
				<view @click="cut()" class="view-code-other">
					<image src="../../static/keyboard/top.png" class="image">
					</image>
				</view>
				<view v-for='item in letter3' @click="clickCode(item)">
					{{item}}
				</view>
				<view @click="delCode()" class="view-code-other">
					<image src="../../static/keyboard/del.png" class="image">
					</image>
				</view>
			</view>




			<view class="view-code">
				<view class="view-code-close" @click="close()">关闭</view>
				<view class="view-code-char" @click="clickCharLetter()">符</view>
				<view class="view-code-space" @click="addSpace()">空格</view>
				<view class="view-code-done" @click="done()">完成</view>
			</view>
		</view>
	</view>
</template>

<script>
	export default {
		name: "yc-keyboard",
		data() {
			return {
				showFlag: false,
				letter1: ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
				letter2: ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'],
				letter3: ['z', 'x', 'c', 'v', 'b', 'n', 'm'],
				charletter1: ['-', '/', ':', ';', '(', ')', '!', '@', '#', '$'],
				charletter2: ['%', '^', '&', '*', '+', "=", ',', '.', "'", '"'],
				showText: '', //	键盘输入后的显示文字
				cutFlag: false, //	默认小写键盘
				showCharLetter: false,
			};
		},
		onLoad() {
			this.showText = '' //	设置默认值

		},
		methods: {
			show() {
				this.showFlag = true;
				this.cutFlag = false; //	默认小写
				this.showCharLetter = false; //	收起符号
			},
			clickCode(item) {
				this.showText += item.toString();
				this.$emit('listenKeyboard', this.showText)
			},
			delCode() {
				this.showText = this.showText.slice(0, this.showText.length - 1)
				this.$emit('listenKeyboard', this.showText)
			},
			cut() { //	切换大小写
				if (!this.cutFlag) {
					this.letter1 = ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"]
					this.letter2 = ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L']
					this.letter3 = ['Z', 'X', 'C', 'V', 'B', 'N', 'M']

					this.cutFlag = true;
					return
				}
				if (this.cutFlag) {
					this.letter1 = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p']
					this.letter2 = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l']
					this.letter3 = ['z', 'x', 'c', 'v', 'b', 'n', 'm']

					this.cutFlag = false;
					return
				}
			},
			clickCharLetter() { //	切换符号
				if (!this.showCharLetter) {
					this.showCharLetter = true;
					return
				}
				if (this.showCharLetter) {
					this.showCharLetter = false;
					return
				}
			},
			addSpace() {	//	空格
				this.showText += ' ';
			},
			close() {	//	关闭
				this.showText = '' //	设置默认值
				this.showFlag = false;
			},
			done() {
				this.showFlag = false;
			}
		}
	}
</script>

<style>
	.view {
		position: fixed;
		bottom: 0;
		width: 100vw;
		z-index: 99;
		border-top: 1px solid #8f949a;
		background-color: #d0d5db;
		padding-bottom: 10px;
	}

	.view-code {
		display: flex;
		justify-content: center;
		margin-top: 10px;
	}

	.view-code>view {
		background-color: #fff;
		border-radius: 5px;
		width: 8%;
		height: 40px;
		line-height: 40px;
		text-align: center;
		margin: 0 4px;
	}

	.view-code-other {
		position: relative;
		background: #adb4be !important;
		width: 20px !important;
		border: none !important;
		width: 50px !important;
	}

	.view-code-close {
		background: #adb4be !important;
		width: 15% !important;
		border: none !important;
		height: 28px;
		line-height: 28px;
	}

	.view-code-char {
		background: #adb4be !important;
		width: 10% !important;
		border: none !important;
		height: 28px;
		line-height: 28px;
	}

	.view-code-space {
		background: #ffffff !important;
		width: 55% !important;
		border: none !important;
		height: 28px;
		line-height: 28px;
	}

	.view-code-done {
		height: 28px;
		line-height: 28px;
		color: #fff;
		background: #0c7ffe !important;
		width: 20% !important;
		border: none !important;
	}

	.image {
		width: 20px;
		height: 20px;
		position: absolute;
		left: 15px;
		top: 10px;
	}
</style>
```




#### 2. 父组件使用方法

项目导入插件后，在父组件引用：

```JavaScript
<yc-keyboard ref="keyboard" v-on:listenKeyboard='showMsgfromChild'></yc-keyboard>
```




使用此方法即可唤起键盘：

```JavaScript
//  不要直接再onLoad使用，没有dom节点

this.$refs.keyboard.show()

```




监听键盘的点击事件：

```JavaScript
showMsgfromChild(data) {
    console.log(data);
}
```




父组件代码：

```JavaScript
<template>
  
  <yc-keyboard ref="keyboard" v-on:listenKeyboard='showMsgfromChild'></yc-keyboard>
  
</template>


<script>
    export default {
        
        data() {
            return {
                
            }
        },
        onLoad() {
        
        },
        methods: {
            show() {
                this.$refs.keyboard.show()
            },
            showMsgfromChild(data){
                console.log(data);
            }
        }
    }

</script>
```


