
# mybatis-plus常用api



表明class为manager表：

```Java
@TableName(value ="manager")
```




1. id自增：

```Java
@TableId(value="id",type = IdType.AUTO)
private Integer id;
```




2. 自动插入

```Java
 @TableField(fill = FieldFill.INSERT)
 private Date insertDt;
```




3. 分页查询

```Java
QueryWrapper<Manager> wrapper = new QueryWrapper<>();
        wrapper.like("name", jsonObject.getString("name"));
        Page page = new Page<>(1, 10);
        Page selectPage = managerMapper.selectPage(page, wrapper);
        List list = selectPage.getRecords();
```


