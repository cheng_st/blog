# win使用nacos

> nacos文档地址

```Plain Text
https://nacos.io/zh-cn/docs/v2/quickstart/quick-start.html
```

![](/images/springboot/nacos/739aa85fcbc49c796ab6e4b769e3e91.png)

> 下载nacos项目

```Plain Text
https://github.com/alibaba/nacos/releases
```


> 运行nacos

```Plain Text
解压nacos项目

cd conf

打开application.properties，找到nacos.core.auth.plugin.nacos.token.secret.key

设置key=012345678901234567890123456789012345678901234567890123456789

cd nacos/bin

打开cmd,启动命令

startup.cmd -m standalone

(standalone代表着单机模式运行，非集群模式)
```


1. 浏览器访问

[http://192.168.3.5:8848/nacos/index.html#/login](http://192.168.3.5:8848/nacos/index.html#/login)

2. 默认登录
nacos/nacos

