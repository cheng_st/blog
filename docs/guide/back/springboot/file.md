# 上传文件

前端上传文件，无法使用json，只能用表单提交的方式

## 前端代码


```html
<form id="uploadForm" class="form-check-inline" enctype="multipart/form-data">
	<input id="uploadFile" style="width: 200px" class="input-group-append" type="file"
		name="file" />
</form>
```


上传文档需要使用formdata格式,后端还需要其他参数，所以需要如下传参方式

```
let json = {
	"参数1":1
}
let data = new FormData();
data.append('file', $("#uploadFile")[0].files[0]);	//	文件
data.append('params', JSON.stringify(json))	//	其他的参数

--------然后在ajax的将这个form表单传到后端
```


示例代码:
```javascript
let uploadFileName = $('#uploadFile').val();
if (uploadFileName.length === 0) {
	mini.alert('请选择需要上传的文档信息');
	return;
}

var fd = new FormData();
fd.append('file', $("#uploadFile")[0].files[0]);

//	ajax
befAjaxFormData('/openApi/uploadDocs.do', selectData, fd, function(datas) {
	showTips(datas.msg);

})


//封装一个方法
function befAjaxFormData(url, params, formData, callback) {

	params.gsxx_id = isEmpty(params.gsxx_id) ? sessionStorage.getItem('gsxx_id') : params.gsxx_id;
	params.login_id = isEmpty(params.login_id) ? sessionStorage.getItem('login_id') : params.login_id;
	params.login_bm = isEmpty(params.login_bm) ? sessionStorage.getItem('login_bm') : params.login_bm;
	params.mac = isEmpty(params.mac) ? localStorage.getItem('mac') : params.mac;
	params.bef_token = "web_token:" + params.gsxx_id + ":" + params.login_id;


	formData.append("params", JSON.stringify(params))

	var jzmessageid = mini.loading("正在加载,请稍等...", "加载提示框");

	$.ajax({
		method: 'POST',
		url: uploadUrl + url,
		cache: false,
		data: formData,
		processData: false,
		contentType: false,
		success: function(datas) {
			console.log(datas);
			mini.hideMessageBox(jzmessageid);
			if (datas.code == 0) {
				if (callback != "undefined" && callback != null) {
					callback(datas);
				}
			} else {
				mini.alert(datas.msg, "错误信息", function() {
					if (datas.code == -11) { //token 验证失效
						top["win"].logout();
					}
				});
			}
		},
		error: function(e) {
			console.log(e);
		}
	})
}
```
 

## 后端代码

在后端的项目里，写一个controller


定义一个方法，参数如下，且需要和前端传过来的formdata的名称保持一致:

file:file

params:params


示例代码：

```java
@ApiOperation("上传文档")
@RequestMapping(value = "uploadDocs.do", method = RequestMethod.POST)
@OthersAnnotation("docs")	//	需要切换数据源所以加个注解
public Map uploadDocs(@RequestPart("file") MultipartFile file, String params) {

	JSONObject jsonObject = JSONObject.parseObject(params);
	if (file.isEmpty()) {
		return Result.fail("文件不能为空...");
	}
	/*String fileType = file.getContentType();
	if (fileType != null && !fileType.contains("pdf")) {
		// 处理PDF文件
		return Result.fail("只能上传pdf文件");
	}*/


	//  路径 D:\\docs\\1108
	String imgFilePath = "D:" + File.separator + "docs" + File.separator + jsonObject.getString("gsxx_id") + File.separator;

	try {
		// 文件全称
		String fileName = file.getOriginalFilename();

		//获取后缀名
		String sname = fileName.substring(fileName.lastIndexOf("."));
		UUID uuid = UUID.randomUUID();

		String newFileName = uuid + sname;

		File paths = new File(imgFilePath + newFileName);
		if (!paths.exists()) {
			paths.mkdirs();
		}

		file.transferTo(paths);

		Map<String, String> hashmap = new HashMap<>();
		
		//https://api.beferp.com/docs/1108\8443294f-6b90-41f2-97a7-e12a7b02a557.pdf
		jsonObject.put("osspath", "https://api.beferp.com/docs/" + jsonObject.getString("gsxx_id")+ File.separator + newFileName);

		hashmap.put("gsxx_id", jsonObject.getString("gsxx_id"));
		hashmap.put("login_id", jsonObject.getString("login_id"));
		hashmap.put("error_code", "");
		hashmap.put("jsonData", jsonObject.toJSONString());
		List list = mobileCommonMapper.P_bef_file_p_i(hashmap);
		return Result.success("上传成功");

	} catch (Exception e) {
		e.printStackTrace();
		return Result.fail(e.getMessage());
	}
}
```


@OthersAnnotation注解：

定义一个注解，在ApiAspet切面里使用

```

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OthersAnnotation {

    String value() default "";

}

```


因为需要切换数据源，所以在进入接口之前需要通过gsxx_id来判断切换至哪个数据源

```
@Pointcut("@annotation(com.bef.annotation.OthersAnnotation)")
public void otherAspect() {
}


@Before("otherAspect()")
public void otherBefore(JoinPoint joinPoint) {

	MethodSignature signature = (MethodSignature) joinPoint.getSignature();
	Method method = signature.getMethod();
	OthersAnnotation annotation = method.getAnnotation(OthersAnnotation.class);

	Object[] args = joinPoint.getArgs();

	//  上传文档
	if (Objects.equals(annotation.value(), "docs")) {
		HashMap hashMap = JSON.parseObject(args[1].toString(), HashMap.class);

		// 从 http 请求头中取出 token
		String tokenKey = hashMap.get("bef_token").toString();
		// 执行认证
		if (StringUtils.isEmpty(tokenKey)) {
			throw new MyException(-11, "无token，请重新登录");
		}
		try {
			tokenKey = URLDecoder.decode(tokenKey, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}


		Token token = (Token) redisUtil.get(tokenKey);

		commonUtils.findLockByUrl(token.getDatasourceKey(), Integer.parseInt(token.getGsxx_id()), true);

	}
	//	查询及删除
	if (Objects.equals(annotation.value(), "idocs")) {

		for (Object obj : args) {
			HashMap hashMap = JSON.parseObject(obj.toString(), HashMap.class);

			String tokenKey = hashMap.get("bef_token").toString();
			// 执行认证
			if (StringUtils.isEmpty(tokenKey)) {
				throw new MyException(-11, "无token，请重新登录");
			}
			try {
				tokenKey = URLDecoder.decode(tokenKey, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}


			Token token = (Token) redisUtil.get(tokenKey);

			commonUtils.findLockByUrl(token.getDatasourceKey(), Integer.parseInt(token.getGsxx_id()), true);
		}
	}
}
```





后端发送formdata格式

```java
@RequestMapping(value = "uploadDocs.do", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
public Map uploadDocs(@RequestPart("file") MultipartFile file, String params) {

    JSONObject jsonObject = JSONObject.parseObject(params);
    if (file.isEmpty()) {
        return Result.fail("文件不能为空...");
    }

    try {
        Resource resource = file.getResource();

        MultiValueMap multiValueMap = new LinkedMultiValueMap();
        multiValueMap.add("file", resource);
        multiValueMap.add("params", params);
        String string = HttpClientUtils.sendFormDataRequest("https://api.beferp.com/befapi/openApi/uploadDocs.do", multiValueMap);
        JSONObject json = JSONObject.parseObject(string);
        return json;
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
    
}
```



HttpClientUtils

```
/**
 * 发送formData格式的数据
 *
 * @param url
 * @param params
 * @return
 */
public static String sendFormDataRequest(String url, MultiValueMap<String, String> params) {
    RestTemplate client = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    HttpMethod method = HttpMethod.POST;
    // 以表单的方式提交
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    //将请求头部和参数合成一个请求
    HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, headers);
    //执行HTTP请求，将返回的结构使用String 类格式化
    ResponseEntity<String> response = client.exchange(url, method, requestEntity, String.class);


    return response.getBody();
}
```





application.yml

```
spring:
  servlet:
    multipart:
      # 默认支持文件上传
      enabled: true
      # 最大支持文件大小
      max-file-size: 50MB
      # 最大支持请求大小
      max-request-size: 100MB
      # 文件支持写入磁盘
      file-size-threshold: 0
      # 上传文件的临时目录
      location: /test
```