# springboot集成支付宝扫码支付

只写了扫码支付功能

maven依赖:
```
<!-- 支付宝支付jar包 -->
<dependency>
	<groupId>com.alipay.sdk</groupId>
	<artifactId>alipay-sdk-java</artifactId>
	<version>3.1.0</version>
</dependency>
```

## 1. 注册登录

1. 登录支付宝的支付平台，注册开发者账户，使用沙箱环境

`https://openhome.alipay.com/develop/sandbox/account`

2. 在沙箱应用里有默认配置，记住默认配置后开始写代码

## 2. java扫码支付代码

```AlipayBean.java
package com.bef.controller.weChat;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Intellij IDEA.
 * User:  Administrator
 * Date:  2024-01-13
 */
@Data
public class AlipayBean implements Serializable {

    /**
     * 商户订单号
     */
    private String out_trade_no;

    /**
     * 订单名称
     */
    private String subject;

    /**
     * 付款金额
     */
    private String total_amount;

    /**
     * 商品描述
     */
    private String body;

    /**
     * 产品编号，支付方式不同，传的数据不同
     */
    //如果是PC网页支付，这个是必传参数
//    private String product_code = "FAST_INSTANT_TRADE_PAY";
    //如果是扫码支付，这个是选传参数
    private String product_code = "FACE_TO_FACE_PAYMENT";
}


```
 
3. 填入对应的沙箱环境的配置信息:

```

    /**
     * 手机扫码支付
     * @param alipayBean
     * @return
     * @throws Exception
     */
    @RequestMapping("/pay2")
    @ResponseBody
    public Map<String, Object> pay2(AlipayBean alipayBean) throws Exception {

//        # 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号，在沙箱应用中获取
       String appId = "";
//# 支付宝公钥,在沙箱应用获取，通过应用公钥生成支付宝公钥

        String publicKey = "";
//# 商户私钥，您的PKCS8格式RSA2私钥，通过开发助手生成的应用私钥
		String privateKey ="";
//# 服务器异步通知页面路径需http://格式的完整路径，不能加?id=123这类自定义参数
        String notifyUrl = "http://zdk75v.natappfree.cc/befapi/alipay/notify";
//# 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数
        String returnUrl = "http://zdk75v.natappfree.cc/befapi/alipay/notify";
//# 签名方式
        String signType = "RSA2";
//# 字符编码格式
        String charset = "utf-8";
//# 支付宝网关，在沙箱应用中获取
        String gatewayUrl = "https://openapi-sandbox.dl.alipaydev.com/gateway.do";

        //接口模拟数据
        UUID uuid = UUID.randomUUID();
        alipayBean.setOut_trade_no(uuid.toString());
        alipayBean.setSubject("订单名称");
        alipayBean.setTotal_amount(String.valueOf(new Random().nextInt(100)));
        alipayBean.setBody("商品描述");

        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl, appId, privateKey, "json", charset, publicKey, signType);
        //扫码支付使用AlipayTradePrecreateRequest传参，下面调用的是execute方法
        AlipayTradePrecreateRequest precreateRequest = new AlipayTradePrecreateRequest();
        precreateRequest.setReturnUrl(returnUrl);
        precreateRequest.setNotifyUrl(notifyUrl);
        precreateRequest.setBizContent(JSONObject.toJSONString(alipayBean));
        log.info("封装请求支付宝付款参数为:{}", JSONObject.toJSONString(precreateRequest));

        AlipayTradePrecreateResponse response = null;
        try {
            response = alipayClient.execute(precreateRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new Exception(String.format("下单失败 错误代码:[%s], 错误信息:[%s]", e.getErrCode(), e.getErrMsg()));
        }
        log.info("AlipayTradePrecreateResponse = {}", response.getBody());
 
        if (!response.isSuccess()) {
            throw new Exception(String.format("下单失败 错误代码:[%s], 错误信息:[%s]", response.getCode(), response.getMsg()));
        }
        // TODO 下单记录保存入库
        // 返回结果，主要是返回 qr_code，前端根据 qr_code 进行重定向或者生成二维码引导用户支付
        JSONObject jsonObject = new JSONObject();
        //支付宝响应的订单号
        String outTradeNo = response.getOutTradeNo();
        jsonObject.put("outTradeNo",outTradeNo);
        //二维码地址，页面使用二维码工具显示出来就可以了
        jsonObject.put("qrCode",response.getQrCode());
        return Result.success(jsonObject);
    }

```


将地址用草料二维码打开后，需要使用沙箱支付宝扫码才能支付，普通支付宝扫码会显示二维码无效。

沙箱支付宝在沙箱工具里下载.

4. 响应结果:
```
{
	"success": true,
	"data": {
		"qrCode": "https://qr.alipay.com/bax04441vxcsp2bwfy3f000a",
		"outTradeNo": "85b0dd4b-141b-4ae5-b80a-e5041a32abdd"
	},
	"msg": "请求成功",
	"code": 0
}
```

## 3. 回调接口

```

    /**
     * 支付宝支付成功回调
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/notify")  // 注意这里必须是POST接口
    public String payNotify(HttpServletRequest request) throws Exception {
        if (request.getParameter("trade_status").equals("TRADE_SUCCESS")) {
            System.out.println("=========支付宝异步回调========");
            Map<String, String> params = new HashMap<>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (String name : requestParams.keySet()) {
                params.put(name, request.getParameter(name));
                 System.out.println(name + " = " + request.getParameter(name));
            }

            String tradeNo = params.get("out_trade_no");
            String gmtPayment = params.get("gmt_payment");
            String alipayTradeNo = params.get("trade_no");

            String sign = params.get("sign");
            String content = AlipaySignature.getSignCheckContentV1(params);
			//	支付宝公钥
            String publik = "";
            boolean checkSignature = AlipaySignature.rsa256CheckContent(content, sign,publik , "UTF-8"); // 验证签名
            // 支付宝验签
            if (checkSignature) {
                // 验签通过
                System.out.println("交易名称: " + params.get("subject"));
                System.out.println("交易状态: " + params.get("trade_status"));
                System.out.println("支付宝交易凭证号: " + params.get("trade_no"));
                System.out.println("商户订单号: " + params.get("out_trade_no"));
                System.out.println("交易金额: " + params.get("total_amount"));
                System.out.println("买家在支付宝唯一id: " + params.get("buyer_id"));
                System.out.println("买家付款时间: " + params.get("gmt_payment"));
                System.out.println("买家付款金额: " + params.get("buyer_pay_amount"));

                // 更新订单未已支付
//                ordersMapper.updateState(tradeNo, "已支付", gmtPayment, alipayTradeNo);
            }
        }
        return "success";
    }
```