
# springboot集成RocketMQ



## RocketMQ安装部署

## 1. 下载 5.0.0



下载地址：[https://rocketmq.apache.org/dowloading/releases/](https://rocketmq.apache.org/dowloading/releases/)


![](/images/springboot/rocket/image-20230318113334107.png)


## 2. 设置环境变量：

**变量名：** ROCKETMQ_HOME

**变量值：** G:\rocketmq-all-5.0.0-bin-release\bin MQ解压路径\MQ文件夹名

## 3. 启动

在`rocketmq-all-5.0.0-bin-release\bin`目录下，打开cmd窗口

先启动 nameServer，启动命令：

```Plain Text
start mqnamesrv.cmd
```


然后在启动 Broker，启动命令：

```Plain Text
start mqbroker.cmd -n 127.0.0.1:9876 autoCreateTopicEnable=true
```


## 4. 下载代码运行

[https://github.com/apache/rocketmq-dashboard](https://github.com/apache/rocketmq-dashboard)

启动完成之后，浏览器中输入`127.0.0.1:8080`，成功后即可进行管理端查看。

### pom.xml

```Plain Text

 <!--引入lombok-->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.10</version>
</dependency>
		
		
<dependency>
    <groupId>org.apache.rocketmq</groupId>
    <artifactId>rocketmq-client-java</artifactId>
    <version>5.0.0</version>
</dependency>


<dependency>
    <groupId>org.apache.rocketmq</groupId>
    <artifactId>rocketmq-spring-boot-starter</artifactId>
    <version>2.2.1</version>
</dependency>
```


#### 如果使用了阿里的json，调整下版本

```Plain Text
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
	<version>2.0.19</version>
</dependency>
```


#### application.prooerties

```Plain Text
#rocket消息队列配置
rocketmq.name-server=127.0.0.1:9876
rocketmq.producer.enable=true
rocketmq.producer.group=springBootGroup
```


## 消息实体类

```Plain Text
@Data
public class MqMsg {

    private String topic;
    private String tag;
    private Object content;
}
```


## 生产者

进行SpringBoot和RocketMQ整合时，关键使用的是RocketMQTemplate类来进行消息发送，其中包含有send()、asyncSend()、sendOneWay()、sendMessageInTransaction()等方法，每个方法都至少包含有参数destination和message。

destination：消息发送到哪个topic和tag，在SpringBoot中topic和tag使用一个参数发送，其中用英文的冒号（:）进行连接。

message：消息体，需要使用 MessageBuilder.withPayload方法对消息进行封装。

```Plain Text
public interface RocketMqService {


    /**
     * 同步发送消息
     */
    void send(MqMsg mqMsg);
    /**
     * 异步发送消息，异步返回消息结果
     */
    void asyncSend(MqMsg mqMsg);
    /**
     * 单向发送消息，不关心返回结果，容易消息丢失，适合日志收集、不精确统计等消息发送;
     */
    void syncSendOrderly(MqMsg mqMsg);

}
```


```Plain Text
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class RocketMqServiceImpl implements RocketMqService {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    private static final Logger log = LoggerFactory.getLogger(RocketMqServiceImpl.class);


    @Override
    public void send(MqMsg msg) {
        log.info("send发送消息:{}", msg);
        rocketMQTemplate.send(msg.getTopic() + ":" + msg.getTag(),
                MessageBuilder.withPayload(msg.getContent()).build());
    }

    @Override
    public void asyncSend(MqMsg msg) {
        log.info("asyncSend发送消息:{}", msg);
        rocketMQTemplate.asyncSend(msg.getTopic() + ":" + msg.getTag(), msg.getContent(),
                new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        log.info("事物消息发送成功:{}", sendResult.getTransactionId());
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        log.info("mqMsg={}消息发送失败", msg);
                    }
                });
    }

    @Override
    public void syncSendOrderly(MqMsg msg) {
        log.info("syncSendOrderly发送消息:{}", msg);
        rocketMQTemplate.sendOneWay(msg.getTopic() + ":" + msg.getTag(), msg.getContent());
    }

}
```


## 消费者

1.消息消费者需要实现接口RocketMQListener并重写onMessage消费方法 2.@RocketMQMessageListener注解参数解释

- topic:表示需要监听哪个topic的消息

- consumerGroup:表示消费者组

- selectorExpression:表示需要监听的tag

```Plain Text
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
@RocketMQMessageListener(topic = "topicA", consumerGroup = "consumer1", selectorExpression = "send")
public class SendConsumer implements RocketMQListener<String> {

    private static final Logger log = LoggerFactory.getLogger(SendConsumer.class);

    @Override
    public void onMessage(String s) {
        log.info("消费者成功消费消息:{}", s);
    }

}
```


```Plain Text
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
    import org.apache.rocketmq.spring.core.RocketMQListener;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.stereotype.Service;
    
    @Service
    @RocketMQMessageListener(topic = "topicA", consumerGroup = "consumer2", selectorExpression = "asyncSend")
    public class AsyncSendConsumer implements RocketMQListener<String> {
    
        private static final Logger log = LoggerFactory.getLogger(AsyncSendConsumer.class);
    
        @Override
        public void onMessage(String s) {
            log.info("消费者成功消费消息:{}", s);
        }
    }
```


## controller

```Plain Text
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class RocketMqController {


    private static final Logger logger = LoggerFactory.getLogger(RocketMqController.class);

    @Autowired
    private RocketMqServiceImpl rocketMqService;

    @GetMapping("/send")
    public void send(){
        MqMsg msg = new MqMsg();
        msg.setTopic("topicA");
        msg.setTag("send");
        Map<String, String> map = new HashMap<>();
        map.put("name", "jack");
        map.put("age", "23");
        map.put("tel", "10086");
        msg.setContent(map.toString());
        rocketMqService.send(msg);
    }

    @GetMapping("/asyncSend")
    public void asyncSend(){
        MqMsg msg = new MqMsg();
        msg.setTopic("topicA");
        msg.setTag("asyncSend");
        Map<String, String> userinfo = new HashMap<>();
        userinfo.put("name", "lee");
        userinfo.put("age", "24");
        userinfo.put("tel", "10010");
        msg.setContent(userinfo.toString());
        rocketMqService.asyncSend(msg);
    }


}
```




