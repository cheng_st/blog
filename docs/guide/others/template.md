
# 微信模板通知



### 前提

 1. 用户登录微信公众平台 &emsp; https://mp.weixin.qq.com/
 2. 申请注册一个公众号（服务号）
 3. 完成服务商认证，不能注册个人 （认证费用300）

<br/>

4. 用户注册完成后，申请开通模板消息

![Alt text](images/image-20230919164700918.png)

<br/>

5. 申请理由：

>用户在关注本公众号后，在门店消费之后，需要向用户发送一个消费通知，以及用户在门店充值后也需要发送通知等等。

<br/>

6. 主营行业：

​	运输和仓储

​	仓储

​	其他


<br/>
添加消息模板：

![Alt text](images/1.png)

<br/>
选择类目：

![Alt text](images/2.png)


选择模板:
![Alt text](images/3.png)


![Alt text](images/4.png)




公众号的模板消息添加好了之后，在系统的短信管理里把模板加上。

模板字段需要用英文字母拼接,例如充值通知: thing9,amount3,amount1,amount2

![Alt text](images/5.png)





<br/>
在微信公众号左侧，找到自定义菜单

新增一个自定义菜单，放入下面链接将会打开用户绑定页面

1. appid：客户微信公众号的appid

2. state：客户在beferp网页版的锁号

3. component_appid：比弗第三方平台的appid(固定值)

https://open.weixin.qq.com/connect/oauth2/authorize?appid=************&redirect_uri=https%3A%2F%2Fbeferp.com%2Fbefapi%2Fweb%2FwebAuth&response_type=code&scope=snsapi_base&state=********&component_appid=wxe7986597a6b2a7e4&connect_redirect=1#wechat_redirect


<div style="color:gray">用户打开上面连接，输入手机号码进行绑定后，就可以获取到openid了，后续则通过openid推送模板消息</div>

<br/>

在beferp网页版系统里，点击头像，选择绑定微信公众号。（需要公众号的管理员微信扫码）

![Alt text](images/7.png)


![Alt text](images/8.png)

<br/>
需要使用公众号绑定的管理员的微信扫码确认.

![Alt text](images/9.png)

<br/>
若是无法打开绑定页面，检查redis的值，将这个删掉后再次重试

![Alt text](images/10.png)

<br/>

客户授权成功后，redis将会出现以下内容。

由客户的微信APPID组成，第一个是调用token,调用模板消息时代替原本用户的授权token试用.

第二个是刷新token,第一个token过期后用此token刷新。

![Alt text](images/11.png)


1. 然后点击添加模板。

2. 模板存入在mysql的befapp数据库中，t_weixin_moban表中。


![Alt text](images/12.png)