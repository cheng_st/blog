

# vitepress使用卜蒜子做访问统计

1. 打开.vitepress目录下得config.js，加上以下内容

```
module.exports = {
	head: [
		[
			'script', { type: 'text/javascript', src: 'https://busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js' }
		]
	],
	
	......
}
```


2. 在.vitepress下新建一个components文件夹，然后新建一个vue文件`Bsz.vue`

```
<template>
    <div class="statistics">
        <div class="busuanzi">
            <span id="busuanzi_container_site_pv" style="margin-right: 20px;">
                本站总访问量
                <span id="busuanzi_value_site_pv"></span>次
                <!-- <span class="post-meta-divider">|</span> -->
            </span>
            ❤️
            <span id="busuanzi_container_site_uv" style="margin-left: 20px;">
                本站访客数
                <span id="busuanzi_value_site_uv"></span>人
            </span>
        </div>
    </div>
</template>
<style>
.statistics {
    text-align: center;
    bottom: 120px;
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    color: grey;
    font-size: 15px;
}
</style>
```

3. 打开theme文件夹下得index.js,添加如下内容

```
import { h } from "vue";

import Bsz from "../components/Bsz.vue";
```

```
export default {
	...DefaultTheme,
	Layout() {
		return h(DefaultTheme.Layout, null, {
			"home-features-after": () => h(Bsz),
		});
	},
	enhanceApp({
		app
	}) {},
};
```


4. 打包后如果报错

`If this is expected, you can disable this check via config. Refer: https://vitepress.dev/reference/site-config#ignoredeadlinks`


在config.js中加一句话就行


```javascript
module.exports = {
	head: [
		[
			'script', { type: 'text/javascript', src: 'https://busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js' }
		]
	],
	ignoreDeadLinks: true	//	死链
}
```
