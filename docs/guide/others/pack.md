
# 定时任务发送短信提醒


## 1. 验证接口

先验证发送短信得接口是否有用，调用正在运行的接口

```地址
https://beferp.com/befapi/push/sendTemplate
```


```参数
{
	"phone": "18857131213",
	"signName": "杭州比弗企业管理",
	"templateId": "2004020",
	"string": [
		"桂B426H5"
	]
}
```

```结果
{
	"success": true,
	"msg": "成功",
	"data": "成功",
	"code": 0
}
```



## 2. 编码代码


1. 新增一个SmsServiceImpl，用core表达式循环执行发送短信的任务

```
@Scheduled(cron = "0 0 10 * * ?") //每天上午10点
```

2. 在发送短信之前，我们要先查询出所有的web数据源

```
@Select("select * from web_datasource")
List<JSONObject> selectCompany();
```

3. 然后再循环每个数据源，通过账号及密码切到对应的数据库中

```
for (JSONObject json : jsonObjects) {
	String urls = json.getString("urls");
	String password = json.getString("password");
	String login_name = json.getString("login_name");
	String data_name = json.getString("data_name");
	String ports = json.getString("ports");

	commonUtils.findDataSource(urls, data_name, login_name, password, ports, true);

}
```

4. 切换完成后，调用过程`p_bef_auto_sms_s`

```
@Select("{ CALL  p_bef_auto_sms_s()}")
@Options(statementType = StatementType.CALLABLE)
List<JSONObject> selectSms();
```

5. 最后将过程查出来的数据循环遍历，发送`txlx`为11的生日短信提醒

```
[
    {
        "p1":"浙88888",
        "p2":"",
        "p3":"",
        "open_id":"",
        "txlx":11,
        "msm":"",
        "sign":"杭州比弗企业管理",
        "rem_je":0,
        "lx":"message",
        "cp":"浙88888",
        "sms_id":"2004020",
        "use_je":0,
        "tel":"18857131213",
        "app_id":""
    }
]
```

```
for (JSONObject js : jsonObjects1) {
	JSONObject requestJSon = new JSONObject();

	String sms_id = js.getString("sms_id");
	String sign = js.getString("sign");
	String tel = js.getString("tel");
	String txlx = js.getString("txlx");

	if ("11".equals(txlx)) {   //  生日提醒
		String p1 = js.getString("p1");
		ArrayList<Object> objects = new ArrayList<>();
		objects.add(p1);
		requestJSon.put("string", objects);
	}

	requestJSon.put("phone", tel);
	requestJSon.put("signName", sign);
	requestJSon.put("templateId", sms_id);
	String str = HttpClientUtils.doPost("https://beferp.com/befapi/push/sendTemplate", requestJSon);
	log.info(str);

}
```