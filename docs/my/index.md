<link rel="stylesheet" type="text/css" href="../public/index.css">

<div align="center">
	<img src="/avatar.jpg" style="width:150px;border-radius:50%">

</div>

<div align="center" style="margin-top:20px;font-weight:bold;color:gray">
毕业于19年，一位 “全干” 工程师。
精通Java SpringBoot js vue的缩写...

❤️ 爱好 ： `📷摄行` &nbsp; `🚴骑行`。📮 : `1836288637@qq.com` 

目前正在自学Swift Ui。
</div>


## 项目

<div class="div">
	<div class="div-item">
		<div>vue+elementui管理系统</div>
		<div>springboot+element-ui开发的简单后台管理系统。</div>
		<div></div>
		<div class="item-body">
			<ul>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 用户管理</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 菜单管理</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 日志</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 更多功能。。。</li>
			</ul>
		</div>
		<div class="div-click">
			<a href="https://gitee.com/cheng_st/qf-project" target="_blank">点击查看</a>
		</div>
	</div>
	<div class="div-item">
		<div>有财账单APP</div>
		<div>基于uniapp混合开发的APP,已上架Appstore。</div>
		<div></div>
		<div class="item-body">
			<ul>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 支出、收入记账</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 借、贷记账</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 账单统计</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 更多功能。。。</li>
			</ul>
		</div>
		<div class="div-click">
			<a href="https://apps.apple.com/cn/app/%E6%9C%89%E8%B4%A2%E8%B4%A6%E5%8D%95/id6444892425" target="_blank">点击查看</a>
		</div>
	</div>
	<div class="div-item">
		<div>vue3+ts管理系统</div>
		<div>springboot+element-ui开发的简单后台管理系统。</div>
		<div></div>
		<div class="item-body">
			<ul>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 用户管理</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 菜单管理</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 日志</li>
				<li><i class="icon ion-md-checkmark-circle-outline"></i> 更多功能。。。</li>
			</ul>
		</div>
		<div class="div-click">
			<a href="https://gitee.com/cheng_st/qf-project" target="_blank"></a>
		</div>
	</div>
</div>

## 技术栈


<div style="display:flex;justify-content: space-around;flex-wrap: wrap;">

<img src="https://img.shields.io/badge/vue-green">
<img src="https://img.shields.io/badge/axios-green">
<img src="https://img.shields.io/badge/elementui-green">
<img src="https://img.shields.io/badge/java-orange">
<img src="https://img.shields.io/badge/springboot-orange">
<img src="https://img.shields.io/badge/redis-orange">
<img src="https://img.shields.io/badge/mybatis-orange">
<img src="https://img.shields.io/badge/nginx-orange">
<img src="https://img.shields.io/badge/uniapp-blue">
<img src="https://img.shields.io/badge/微信小程序-blue">
</div>


### 前端

vue axios elementui...

### 后端

java springboot redis mybatis nginx...

### 其他

uniapp 微信小程序 





	
	