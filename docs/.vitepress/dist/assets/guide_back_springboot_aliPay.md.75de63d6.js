import{_ as s,o as n,c as a,S as e}from"./chunks/framework.d7c45c4d.js";const d=JSON.parse('{"title":"springboot集成支付宝扫码支付","description":"","frontmatter":{},"headers":[],"relativePath":"guide/back/springboot/aliPay.md","filePath":"guide/back/springboot/aliPay.md","lastUpdated":1705138198000}'),p={name:"guide/back/springboot/aliPay.md"},l=e(`<h1 id="springboot集成支付宝扫码支付" tabindex="-1">springboot集成支付宝扫码支付 <a class="header-anchor" href="#springboot集成支付宝扫码支付" aria-label="Permalink to &quot;springboot集成支付宝扫码支付&quot;">​</a></h1><p>只写了扫码支付功能</p><p>maven依赖:</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">&lt;!-- 支付宝支付jar包 --&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">	&lt;groupId&gt;com.alipay.sdk&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">	&lt;artifactId&gt;alipay-sdk-java&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">	&lt;version&gt;3.1.0&lt;/version&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/dependency&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">&lt;!-- 支付宝支付jar包 --&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;">	&lt;groupId&gt;com.alipay.sdk&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#24292e;">	&lt;artifactId&gt;alipay-sdk-java&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#24292e;">	&lt;version&gt;3.1.0&lt;/version&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/dependency&gt;</span></span></code></pre></div><h2 id="_1-注册登录" tabindex="-1">1. 注册登录 <a class="header-anchor" href="#_1-注册登录" aria-label="Permalink to &quot;1. 注册登录&quot;">​</a></h2><ol><li>登录支付宝的支付平台，注册开发者账户，使用沙箱环境</li></ol><p><code>https://openhome.alipay.com/develop/sandbox/account</code></p><ol start="2"><li>在沙箱应用里有默认配置，记住默认配置后开始写代码</li></ol><h2 id="_2-java扫码支付代码" tabindex="-1">2. java扫码支付代码 <a class="header-anchor" href="#_2-java扫码支付代码" aria-label="Permalink to &quot;2. java扫码支付代码&quot;">​</a></h2><div class="language-AlipayBean.java vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">AlipayBean.java</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">package com.bef.controller.weChat;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import lombok.Data;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import java.io.Serializable;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * Created by Intellij IDEA.</span></span>
<span class="line"><span style="color:#e1e4e8;"> * User:  Administrator</span></span>
<span class="line"><span style="color:#e1e4e8;"> * Date:  2024-01-13</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">@Data</span></span>
<span class="line"><span style="color:#e1e4e8;">public class AlipayBean implements Serializable {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 商户订单号</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    private String out_trade_no;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 订单名称</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    private String subject;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 付款金额</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    private String total_amount;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 商品描述</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    private String body;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 产品编号，支付方式不同，传的数据不同</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    //如果是PC网页支付，这个是必传参数</span></span>
<span class="line"><span style="color:#e1e4e8;">//    private String product_code = &quot;FAST_INSTANT_TRADE_PAY&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">    //如果是扫码支付，这个是选传参数</span></span>
<span class="line"><span style="color:#e1e4e8;">    private String product_code = &quot;FACE_TO_FACE_PAYMENT&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">package com.bef.controller.weChat;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import lombok.Data;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import java.io.Serializable;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * Created by Intellij IDEA.</span></span>
<span class="line"><span style="color:#24292e;"> * User:  Administrator</span></span>
<span class="line"><span style="color:#24292e;"> * Date:  2024-01-13</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">@Data</span></span>
<span class="line"><span style="color:#24292e;">public class AlipayBean implements Serializable {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 商户订单号</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    private String out_trade_no;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 订单名称</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    private String subject;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 付款金额</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    private String total_amount;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 商品描述</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    private String body;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 产品编号，支付方式不同，传的数据不同</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    //如果是PC网页支付，这个是必传参数</span></span>
<span class="line"><span style="color:#24292e;">//    private String product_code = &quot;FAST_INSTANT_TRADE_PAY&quot;;</span></span>
<span class="line"><span style="color:#24292e;">    //如果是扫码支付，这个是选传参数</span></span>
<span class="line"><span style="color:#24292e;">    private String product_code = &quot;FACE_TO_FACE_PAYMENT&quot;;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><ol start="3"><li>填入对应的沙箱环境的配置信息:</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 手机扫码支付</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @param alipayBean</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @return</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @throws Exception</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    @RequestMapping(&quot;/pay2&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">    @ResponseBody</span></span>
<span class="line"><span style="color:#e1e4e8;">    public Map&lt;String, Object&gt; pay2(AlipayBean alipayBean) throws Exception {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">//        # 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号，在沙箱应用中获取</span></span>
<span class="line"><span style="color:#e1e4e8;">       String appId = &quot;&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">//# 支付宝公钥,在沙箱应用获取，通过应用公钥生成支付宝公钥</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        String publicKey = &quot;&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">//# 商户私钥，您的PKCS8格式RSA2私钥，通过开发助手生成的应用私钥</span></span>
<span class="line"><span style="color:#e1e4e8;">		String privateKey =&quot;&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">//# 服务器异步通知页面路径需http://格式的完整路径，不能加?id=123这类自定义参数</span></span>
<span class="line"><span style="color:#e1e4e8;">        String notifyUrl = &quot;http://zdk75v.natappfree.cc/befapi/alipay/notify&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">//# 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数</span></span>
<span class="line"><span style="color:#e1e4e8;">        String returnUrl = &quot;http://zdk75v.natappfree.cc/befapi/alipay/notify&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">//# 签名方式</span></span>
<span class="line"><span style="color:#e1e4e8;">        String signType = &quot;RSA2&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">//# 字符编码格式</span></span>
<span class="line"><span style="color:#e1e4e8;">        String charset = &quot;utf-8&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">//# 支付宝网关，在沙箱应用中获取</span></span>
<span class="line"><span style="color:#e1e4e8;">        String gatewayUrl = &quot;https://openapi-sandbox.dl.alipaydev.com/gateway.do&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        //接口模拟数据</span></span>
<span class="line"><span style="color:#e1e4e8;">        UUID uuid = UUID.randomUUID();</span></span>
<span class="line"><span style="color:#e1e4e8;">        alipayBean.setOut_trade_no(uuid.toString());</span></span>
<span class="line"><span style="color:#e1e4e8;">        alipayBean.setSubject(&quot;订单名称&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        alipayBean.setTotal_amount(String.valueOf(new Random().nextInt(100)));</span></span>
<span class="line"><span style="color:#e1e4e8;">        alipayBean.setBody(&quot;商品描述&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl, appId, privateKey, &quot;json&quot;, charset, publicKey, signType);</span></span>
<span class="line"><span style="color:#e1e4e8;">        //扫码支付使用AlipayTradePrecreateRequest传参，下面调用的是execute方法</span></span>
<span class="line"><span style="color:#e1e4e8;">        AlipayTradePrecreateRequest precreateRequest = new AlipayTradePrecreateRequest();</span></span>
<span class="line"><span style="color:#e1e4e8;">        precreateRequest.setReturnUrl(returnUrl);</span></span>
<span class="line"><span style="color:#e1e4e8;">        precreateRequest.setNotifyUrl(notifyUrl);</span></span>
<span class="line"><span style="color:#e1e4e8;">        precreateRequest.setBizContent(JSONObject.toJSONString(alipayBean));</span></span>
<span class="line"><span style="color:#e1e4e8;">        log.info(&quot;封装请求支付宝付款参数为:{}&quot;, JSONObject.toJSONString(precreateRequest));</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        AlipayTradePrecreateResponse response = null;</span></span>
<span class="line"><span style="color:#e1e4e8;">        try {</span></span>
<span class="line"><span style="color:#e1e4e8;">            response = alipayClient.execute(precreateRequest);</span></span>
<span class="line"><span style="color:#e1e4e8;">        } catch (AlipayApiException e) {</span></span>
<span class="line"><span style="color:#e1e4e8;">            e.printStackTrace();</span></span>
<span class="line"><span style="color:#e1e4e8;">            throw new Exception(String.format(&quot;下单失败 错误代码:[%s], 错误信息:[%s]&quot;, e.getErrCode(), e.getErrMsg()));</span></span>
<span class="line"><span style="color:#e1e4e8;">        }</span></span>
<span class="line"><span style="color:#e1e4e8;">        log.info(&quot;AlipayTradePrecreateResponse = {}&quot;, response.getBody());</span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;">        if (!response.isSuccess()) {</span></span>
<span class="line"><span style="color:#e1e4e8;">            throw new Exception(String.format(&quot;下单失败 错误代码:[%s], 错误信息:[%s]&quot;, response.getCode(), response.getMsg()));</span></span>
<span class="line"><span style="color:#e1e4e8;">        }</span></span>
<span class="line"><span style="color:#e1e4e8;">        // TODO 下单记录保存入库</span></span>
<span class="line"><span style="color:#e1e4e8;">        // 返回结果，主要是返回 qr_code，前端根据 qr_code 进行重定向或者生成二维码引导用户支付</span></span>
<span class="line"><span style="color:#e1e4e8;">        JSONObject jsonObject = new JSONObject();</span></span>
<span class="line"><span style="color:#e1e4e8;">        //支付宝响应的订单号</span></span>
<span class="line"><span style="color:#e1e4e8;">        String outTradeNo = response.getOutTradeNo();</span></span>
<span class="line"><span style="color:#e1e4e8;">        jsonObject.put(&quot;outTradeNo&quot;,outTradeNo);</span></span>
<span class="line"><span style="color:#e1e4e8;">        //二维码地址，页面使用二维码工具显示出来就可以了</span></span>
<span class="line"><span style="color:#e1e4e8;">        jsonObject.put(&quot;qrCode&quot;,response.getQrCode());</span></span>
<span class="line"><span style="color:#e1e4e8;">        return Result.success(jsonObject);</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 手机扫码支付</span></span>
<span class="line"><span style="color:#24292e;">     * @param alipayBean</span></span>
<span class="line"><span style="color:#24292e;">     * @return</span></span>
<span class="line"><span style="color:#24292e;">     * @throws Exception</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    @RequestMapping(&quot;/pay2&quot;)</span></span>
<span class="line"><span style="color:#24292e;">    @ResponseBody</span></span>
<span class="line"><span style="color:#24292e;">    public Map&lt;String, Object&gt; pay2(AlipayBean alipayBean) throws Exception {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">//        # 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号，在沙箱应用中获取</span></span>
<span class="line"><span style="color:#24292e;">       String appId = &quot;&quot;;</span></span>
<span class="line"><span style="color:#24292e;">//# 支付宝公钥,在沙箱应用获取，通过应用公钥生成支付宝公钥</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        String publicKey = &quot;&quot;;</span></span>
<span class="line"><span style="color:#24292e;">//# 商户私钥，您的PKCS8格式RSA2私钥，通过开发助手生成的应用私钥</span></span>
<span class="line"><span style="color:#24292e;">		String privateKey =&quot;&quot;;</span></span>
<span class="line"><span style="color:#24292e;">//# 服务器异步通知页面路径需http://格式的完整路径，不能加?id=123这类自定义参数</span></span>
<span class="line"><span style="color:#24292e;">        String notifyUrl = &quot;http://zdk75v.natappfree.cc/befapi/alipay/notify&quot;;</span></span>
<span class="line"><span style="color:#24292e;">//# 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数</span></span>
<span class="line"><span style="color:#24292e;">        String returnUrl = &quot;http://zdk75v.natappfree.cc/befapi/alipay/notify&quot;;</span></span>
<span class="line"><span style="color:#24292e;">//# 签名方式</span></span>
<span class="line"><span style="color:#24292e;">        String signType = &quot;RSA2&quot;;</span></span>
<span class="line"><span style="color:#24292e;">//# 字符编码格式</span></span>
<span class="line"><span style="color:#24292e;">        String charset = &quot;utf-8&quot;;</span></span>
<span class="line"><span style="color:#24292e;">//# 支付宝网关，在沙箱应用中获取</span></span>
<span class="line"><span style="color:#24292e;">        String gatewayUrl = &quot;https://openapi-sandbox.dl.alipaydev.com/gateway.do&quot;;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        //接口模拟数据</span></span>
<span class="line"><span style="color:#24292e;">        UUID uuid = UUID.randomUUID();</span></span>
<span class="line"><span style="color:#24292e;">        alipayBean.setOut_trade_no(uuid.toString());</span></span>
<span class="line"><span style="color:#24292e;">        alipayBean.setSubject(&quot;订单名称&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        alipayBean.setTotal_amount(String.valueOf(new Random().nextInt(100)));</span></span>
<span class="line"><span style="color:#24292e;">        alipayBean.setBody(&quot;商品描述&quot;);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl, appId, privateKey, &quot;json&quot;, charset, publicKey, signType);</span></span>
<span class="line"><span style="color:#24292e;">        //扫码支付使用AlipayTradePrecreateRequest传参，下面调用的是execute方法</span></span>
<span class="line"><span style="color:#24292e;">        AlipayTradePrecreateRequest precreateRequest = new AlipayTradePrecreateRequest();</span></span>
<span class="line"><span style="color:#24292e;">        precreateRequest.setReturnUrl(returnUrl);</span></span>
<span class="line"><span style="color:#24292e;">        precreateRequest.setNotifyUrl(notifyUrl);</span></span>
<span class="line"><span style="color:#24292e;">        precreateRequest.setBizContent(JSONObject.toJSONString(alipayBean));</span></span>
<span class="line"><span style="color:#24292e;">        log.info(&quot;封装请求支付宝付款参数为:{}&quot;, JSONObject.toJSONString(precreateRequest));</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        AlipayTradePrecreateResponse response = null;</span></span>
<span class="line"><span style="color:#24292e;">        try {</span></span>
<span class="line"><span style="color:#24292e;">            response = alipayClient.execute(precreateRequest);</span></span>
<span class="line"><span style="color:#24292e;">        } catch (AlipayApiException e) {</span></span>
<span class="line"><span style="color:#24292e;">            e.printStackTrace();</span></span>
<span class="line"><span style="color:#24292e;">            throw new Exception(String.format(&quot;下单失败 错误代码:[%s], 错误信息:[%s]&quot;, e.getErrCode(), e.getErrMsg()));</span></span>
<span class="line"><span style="color:#24292e;">        }</span></span>
<span class="line"><span style="color:#24292e;">        log.info(&quot;AlipayTradePrecreateResponse = {}&quot;, response.getBody());</span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;">        if (!response.isSuccess()) {</span></span>
<span class="line"><span style="color:#24292e;">            throw new Exception(String.format(&quot;下单失败 错误代码:[%s], 错误信息:[%s]&quot;, response.getCode(), response.getMsg()));</span></span>
<span class="line"><span style="color:#24292e;">        }</span></span>
<span class="line"><span style="color:#24292e;">        // TODO 下单记录保存入库</span></span>
<span class="line"><span style="color:#24292e;">        // 返回结果，主要是返回 qr_code，前端根据 qr_code 进行重定向或者生成二维码引导用户支付</span></span>
<span class="line"><span style="color:#24292e;">        JSONObject jsonObject = new JSONObject();</span></span>
<span class="line"><span style="color:#24292e;">        //支付宝响应的订单号</span></span>
<span class="line"><span style="color:#24292e;">        String outTradeNo = response.getOutTradeNo();</span></span>
<span class="line"><span style="color:#24292e;">        jsonObject.put(&quot;outTradeNo&quot;,outTradeNo);</span></span>
<span class="line"><span style="color:#24292e;">        //二维码地址，页面使用二维码工具显示出来就可以了</span></span>
<span class="line"><span style="color:#24292e;">        jsonObject.put(&quot;qrCode&quot;,response.getQrCode());</span></span>
<span class="line"><span style="color:#24292e;">        return Result.success(jsonObject);</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span></code></pre></div><p>将地址用草料二维码打开后，需要使用沙箱支付宝扫码才能支付，普通支付宝扫码会显示二维码无效。</p><p>沙箱支付宝在沙箱工具里下载.</p><ol start="4"><li>响应结果:</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">{</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;success&quot;: true,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;data&quot;: {</span></span>
<span class="line"><span style="color:#e1e4e8;">		&quot;qrCode&quot;: &quot;https://qr.alipay.com/bax04441vxcsp2bwfy3f000a&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">		&quot;outTradeNo&quot;: &quot;85b0dd4b-141b-4ae5-b80a-e5041a32abdd&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">	},</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;msg&quot;: &quot;请求成功&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;code&quot;: 0</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">{</span></span>
<span class="line"><span style="color:#24292e;">	&quot;success&quot;: true,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;data&quot;: {</span></span>
<span class="line"><span style="color:#24292e;">		&quot;qrCode&quot;: &quot;https://qr.alipay.com/bax04441vxcsp2bwfy3f000a&quot;,</span></span>
<span class="line"><span style="color:#24292e;">		&quot;outTradeNo&quot;: &quot;85b0dd4b-141b-4ae5-b80a-e5041a32abdd&quot;</span></span>
<span class="line"><span style="color:#24292e;">	},</span></span>
<span class="line"><span style="color:#24292e;">	&quot;msg&quot;: &quot;请求成功&quot;,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;code&quot;: 0</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="_3-回调接口" tabindex="-1">3. 回调接口 <a class="header-anchor" href="#_3-回调接口" aria-label="Permalink to &quot;3. 回调接口&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 支付宝支付成功回调</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @param request</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @return</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @throws Exception</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    @PostMapping(&quot;/notify&quot;)  // 注意这里必须是POST接口</span></span>
<span class="line"><span style="color:#e1e4e8;">    public String payNotify(HttpServletRequest request) throws Exception {</span></span>
<span class="line"><span style="color:#e1e4e8;">        if (request.getParameter(&quot;trade_status&quot;).equals(&quot;TRADE_SUCCESS&quot;)) {</span></span>
<span class="line"><span style="color:#e1e4e8;">            System.out.println(&quot;=========支付宝异步回调========&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">            Map&lt;String, String&gt; params = new HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#e1e4e8;">            Map&lt;String, String[]&gt; requestParams = request.getParameterMap();</span></span>
<span class="line"><span style="color:#e1e4e8;">            for (String name : requestParams.keySet()) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                params.put(name, request.getParameter(name));</span></span>
<span class="line"><span style="color:#e1e4e8;">                 System.out.println(name + &quot; = &quot; + request.getParameter(name));</span></span>
<span class="line"><span style="color:#e1e4e8;">            }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">            String tradeNo = params.get(&quot;out_trade_no&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">            String gmtPayment = params.get(&quot;gmt_payment&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">            String alipayTradeNo = params.get(&quot;trade_no&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">            String sign = params.get(&quot;sign&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">            String content = AlipaySignature.getSignCheckContentV1(params);</span></span>
<span class="line"><span style="color:#e1e4e8;">			//	支付宝公钥</span></span>
<span class="line"><span style="color:#e1e4e8;">            String publik = &quot;&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">            boolean checkSignature = AlipaySignature.rsa256CheckContent(content, sign,publik , &quot;UTF-8&quot;); // 验证签名</span></span>
<span class="line"><span style="color:#e1e4e8;">            // 支付宝验签</span></span>
<span class="line"><span style="color:#e1e4e8;">            if (checkSignature) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                // 验签通过</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;交易名称: &quot; + params.get(&quot;subject&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;交易状态: &quot; + params.get(&quot;trade_status&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;支付宝交易凭证号: &quot; + params.get(&quot;trade_no&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;商户订单号: &quot; + params.get(&quot;out_trade_no&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;交易金额: &quot; + params.get(&quot;total_amount&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;买家在支付宝唯一id: &quot; + params.get(&quot;buyer_id&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;买家付款时间: &quot; + params.get(&quot;gmt_payment&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;">                System.out.println(&quot;买家付款金额: &quot; + params.get(&quot;buyer_pay_amount&quot;));</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                // 更新订单未已支付</span></span>
<span class="line"><span style="color:#e1e4e8;">//                ordersMapper.updateState(tradeNo, &quot;已支付&quot;, gmtPayment, alipayTradeNo);</span></span>
<span class="line"><span style="color:#e1e4e8;">            }</span></span>
<span class="line"><span style="color:#e1e4e8;">        }</span></span>
<span class="line"><span style="color:#e1e4e8;">        return &quot;success&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 支付宝支付成功回调</span></span>
<span class="line"><span style="color:#24292e;">     * @param request</span></span>
<span class="line"><span style="color:#24292e;">     * @return</span></span>
<span class="line"><span style="color:#24292e;">     * @throws Exception</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    @PostMapping(&quot;/notify&quot;)  // 注意这里必须是POST接口</span></span>
<span class="line"><span style="color:#24292e;">    public String payNotify(HttpServletRequest request) throws Exception {</span></span>
<span class="line"><span style="color:#24292e;">        if (request.getParameter(&quot;trade_status&quot;).equals(&quot;TRADE_SUCCESS&quot;)) {</span></span>
<span class="line"><span style="color:#24292e;">            System.out.println(&quot;=========支付宝异步回调========&quot;);</span></span>
<span class="line"><span style="color:#24292e;">            Map&lt;String, String&gt; params = new HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#24292e;">            Map&lt;String, String[]&gt; requestParams = request.getParameterMap();</span></span>
<span class="line"><span style="color:#24292e;">            for (String name : requestParams.keySet()) {</span></span>
<span class="line"><span style="color:#24292e;">                params.put(name, request.getParameter(name));</span></span>
<span class="line"><span style="color:#24292e;">                 System.out.println(name + &quot; = &quot; + request.getParameter(name));</span></span>
<span class="line"><span style="color:#24292e;">            }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">            String tradeNo = params.get(&quot;out_trade_no&quot;);</span></span>
<span class="line"><span style="color:#24292e;">            String gmtPayment = params.get(&quot;gmt_payment&quot;);</span></span>
<span class="line"><span style="color:#24292e;">            String alipayTradeNo = params.get(&quot;trade_no&quot;);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">            String sign = params.get(&quot;sign&quot;);</span></span>
<span class="line"><span style="color:#24292e;">            String content = AlipaySignature.getSignCheckContentV1(params);</span></span>
<span class="line"><span style="color:#24292e;">			//	支付宝公钥</span></span>
<span class="line"><span style="color:#24292e;">            String publik = &quot;&quot;;</span></span>
<span class="line"><span style="color:#24292e;">            boolean checkSignature = AlipaySignature.rsa256CheckContent(content, sign,publik , &quot;UTF-8&quot;); // 验证签名</span></span>
<span class="line"><span style="color:#24292e;">            // 支付宝验签</span></span>
<span class="line"><span style="color:#24292e;">            if (checkSignature) {</span></span>
<span class="line"><span style="color:#24292e;">                // 验签通过</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;交易名称: &quot; + params.get(&quot;subject&quot;));</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;交易状态: &quot; + params.get(&quot;trade_status&quot;));</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;支付宝交易凭证号: &quot; + params.get(&quot;trade_no&quot;));</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;商户订单号: &quot; + params.get(&quot;out_trade_no&quot;));</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;交易金额: &quot; + params.get(&quot;total_amount&quot;));</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;买家在支付宝唯一id: &quot; + params.get(&quot;buyer_id&quot;));</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;买家付款时间: &quot; + params.get(&quot;gmt_payment&quot;));</span></span>
<span class="line"><span style="color:#24292e;">                System.out.println(&quot;买家付款金额: &quot; + params.get(&quot;buyer_pay_amount&quot;));</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                // 更新订单未已支付</span></span>
<span class="line"><span style="color:#24292e;">//                ordersMapper.updateState(tradeNo, &quot;已支付&quot;, gmtPayment, alipayTradeNo);</span></span>
<span class="line"><span style="color:#24292e;">            }</span></span>
<span class="line"><span style="color:#24292e;">        }</span></span>
<span class="line"><span style="color:#24292e;">        return &quot;success&quot;;</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span></code></pre></div>`,18),t=[l];function o(c,r,i,y,u,q){return n(),a("div",null,t)}const m=s(p,[["render",o]]);export{d as __pageData,m as default};
