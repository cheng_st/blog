import{_ as s,o as n,c as a,S as p}from"./chunks/framework.d7c45c4d.js";const d=JSON.parse('{"title":"上传文件","description":"","frontmatter":{},"headers":[],"relativePath":"guide/back/springboot/file.md","filePath":"guide/back/springboot/file.md","lastUpdated":1702373809000}'),l={name:"guide/back/springboot/file.md"},o=p(`<h1 id="上传文件" tabindex="-1">上传文件 <a class="header-anchor" href="#上传文件" aria-label="Permalink to &quot;上传文件&quot;">​</a></h1><p>前端上传文件，无法使用json，只能用表单提交的方式</p><h2 id="前端代码" tabindex="-1">前端代码 <a class="header-anchor" href="#前端代码" aria-label="Permalink to &quot;前端代码&quot;">​</a></h2><div class="language-html vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">html</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">form</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">id</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;uploadForm&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">class</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;form-check-inline&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">enctype</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;multipart/form-data&quot;</span><span style="color:#E1E4E8;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">	&lt;</span><span style="color:#85E89D;">input</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">id</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;uploadFile&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">style</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;width: 200px&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">class</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;input-group-append&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">type</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;file&quot;</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#B392F0;">name</span><span style="color:#E1E4E8;">=</span><span style="color:#9ECBFF;">&quot;file&quot;</span><span style="color:#E1E4E8;"> /&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;/</span><span style="color:#85E89D;">form</span><span style="color:#E1E4E8;">&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">form</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">id</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;uploadForm&quot;</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">class</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;form-check-inline&quot;</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">enctype</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;multipart/form-data&quot;</span><span style="color:#24292E;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">	&lt;</span><span style="color:#22863A;">input</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">id</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;uploadFile&quot;</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">style</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;width: 200px&quot;</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">class</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;input-group-append&quot;</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">type</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;file&quot;</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6F42C1;">name</span><span style="color:#24292E;">=</span><span style="color:#032F62;">&quot;file&quot;</span><span style="color:#24292E;"> /&gt;</span></span>
<span class="line"><span style="color:#24292E;">&lt;/</span><span style="color:#22863A;">form</span><span style="color:#24292E;">&gt;</span></span></code></pre></div><p>上传文档需要使用formdata格式,后端还需要其他参数，所以需要如下传参方式</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">let json = {</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;参数1&quot;:1</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;">let data = new FormData();</span></span>
<span class="line"><span style="color:#e1e4e8;">data.append(&#39;file&#39;, $(&quot;#uploadFile&quot;)[0].files[0]);	//	文件</span></span>
<span class="line"><span style="color:#e1e4e8;">data.append(&#39;params&#39;, JSON.stringify(json))	//	其他的参数</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">--------然后在ajax的将这个form表单传到后端</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">let json = {</span></span>
<span class="line"><span style="color:#24292e;">	&quot;参数1&quot;:1</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;">let data = new FormData();</span></span>
<span class="line"><span style="color:#24292e;">data.append(&#39;file&#39;, $(&quot;#uploadFile&quot;)[0].files[0]);	//	文件</span></span>
<span class="line"><span style="color:#24292e;">data.append(&#39;params&#39;, JSON.stringify(json))	//	其他的参数</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">--------然后在ajax的将这个form表单传到后端</span></span></code></pre></div><p>示例代码:</p><div class="language-javascript vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">javascript</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#F97583;">let</span><span style="color:#E1E4E8;"> uploadFileName </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">$</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;#uploadFile&#39;</span><span style="color:#E1E4E8;">).</span><span style="color:#B392F0;">val</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (uploadFileName.</span><span style="color:#79B8FF;">length</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">===</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">0</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">	mini.</span><span style="color:#B392F0;">alert</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;请选择需要上传的文档信息&#39;</span><span style="color:#E1E4E8;">);</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;">;</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#F97583;">var</span><span style="color:#E1E4E8;"> fd </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">new</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">FormData</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"><span style="color:#E1E4E8;">fd.</span><span style="color:#B392F0;">append</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;file&#39;</span><span style="color:#E1E4E8;">, </span><span style="color:#B392F0;">$</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;#uploadFile&quot;</span><span style="color:#E1E4E8;">)[</span><span style="color:#79B8FF;">0</span><span style="color:#E1E4E8;">].files[</span><span style="color:#79B8FF;">0</span><span style="color:#E1E4E8;">]);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//	ajax</span></span>
<span class="line"><span style="color:#B392F0;">befAjaxFormData</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;/openApi/uploadDocs.do&#39;</span><span style="color:#E1E4E8;">, selectData, fd, </span><span style="color:#F97583;">function</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">datas</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#B392F0;">showTips</span><span style="color:#E1E4E8;">(datas.msg);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">})</span></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//封装一个方法</span></span>
<span class="line"><span style="color:#F97583;">function</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">befAjaxFormData</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">url</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">params</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">formData</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">callback</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	params.gsxx_id </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">isEmpty</span><span style="color:#E1E4E8;">(params.gsxx_id) </span><span style="color:#F97583;">?</span><span style="color:#E1E4E8;"> sessionStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;gsxx_id&#39;</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">:</span><span style="color:#E1E4E8;"> params.gsxx_id;</span></span>
<span class="line"><span style="color:#E1E4E8;">	params.login_id </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">isEmpty</span><span style="color:#E1E4E8;">(params.login_id) </span><span style="color:#F97583;">?</span><span style="color:#E1E4E8;"> sessionStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;login_id&#39;</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">:</span><span style="color:#E1E4E8;"> params.login_id;</span></span>
<span class="line"><span style="color:#E1E4E8;">	params.login_bm </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">isEmpty</span><span style="color:#E1E4E8;">(params.login_bm) </span><span style="color:#F97583;">?</span><span style="color:#E1E4E8;"> sessionStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;login_bm&#39;</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">:</span><span style="color:#E1E4E8;"> params.login_bm;</span></span>
<span class="line"><span style="color:#E1E4E8;">	params.mac </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">isEmpty</span><span style="color:#E1E4E8;">(params.mac) </span><span style="color:#F97583;">?</span><span style="color:#E1E4E8;"> localStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;mac&#39;</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">:</span><span style="color:#E1E4E8;"> params.mac;</span></span>
<span class="line"><span style="color:#E1E4E8;">	params.bef_token </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;web_token:&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> params.gsxx_id </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;:&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> params.login_id;</span></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	formData.</span><span style="color:#B392F0;">append</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;params&quot;</span><span style="color:#E1E4E8;">, </span><span style="color:#79B8FF;">JSON</span><span style="color:#E1E4E8;">.</span><span style="color:#B392F0;">stringify</span><span style="color:#E1E4E8;">(params))</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">var</span><span style="color:#E1E4E8;"> jzmessageid </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> mini.</span><span style="color:#B392F0;">loading</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;正在加载,请稍等...&quot;</span><span style="color:#E1E4E8;">, </span><span style="color:#9ECBFF;">&quot;加载提示框&quot;</span><span style="color:#E1E4E8;">);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	$.</span><span style="color:#B392F0;">ajax</span><span style="color:#E1E4E8;">({</span></span>
<span class="line"><span style="color:#E1E4E8;">		method: </span><span style="color:#9ECBFF;">&#39;POST&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		url: uploadUrl </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> url,</span></span>
<span class="line"><span style="color:#E1E4E8;">		cache: </span><span style="color:#79B8FF;">false</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		data: formData,</span></span>
<span class="line"><span style="color:#E1E4E8;">		processData: </span><span style="color:#79B8FF;">false</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		contentType: </span><span style="color:#79B8FF;">false</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#B392F0;">success</span><span style="color:#E1E4E8;">: </span><span style="color:#F97583;">function</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">datas</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">			console.</span><span style="color:#B392F0;">log</span><span style="color:#E1E4E8;">(datas);</span></span>
<span class="line"><span style="color:#E1E4E8;">			mini.</span><span style="color:#B392F0;">hideMessageBox</span><span style="color:#E1E4E8;">(jzmessageid);</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (datas.code </span><span style="color:#F97583;">==</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">0</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">				</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (callback </span><span style="color:#F97583;">!=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;undefined&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">&amp;&amp;</span><span style="color:#E1E4E8;"> callback </span><span style="color:#F97583;">!=</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">null</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">					</span><span style="color:#B392F0;">callback</span><span style="color:#E1E4E8;">(datas);</span></span>
<span class="line"><span style="color:#E1E4E8;">				}</span></span>
<span class="line"><span style="color:#E1E4E8;">			} </span><span style="color:#F97583;">else</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">				mini.</span><span style="color:#B392F0;">alert</span><span style="color:#E1E4E8;">(datas.msg, </span><span style="color:#9ECBFF;">&quot;错误信息&quot;</span><span style="color:#E1E4E8;">, </span><span style="color:#F97583;">function</span><span style="color:#E1E4E8;">() {</span></span>
<span class="line"><span style="color:#E1E4E8;">					</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (datas.code </span><span style="color:#F97583;">==</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">-</span><span style="color:#79B8FF;">11</span><span style="color:#E1E4E8;">) { </span><span style="color:#6A737D;">//token 验证失效</span></span>
<span class="line"><span style="color:#E1E4E8;">						top[</span><span style="color:#9ECBFF;">&quot;win&quot;</span><span style="color:#E1E4E8;">].</span><span style="color:#B392F0;">logout</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"><span style="color:#E1E4E8;">					}</span></span>
<span class="line"><span style="color:#E1E4E8;">				});</span></span>
<span class="line"><span style="color:#E1E4E8;">			}</span></span>
<span class="line"><span style="color:#E1E4E8;">		},</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#B392F0;">error</span><span style="color:#E1E4E8;">: </span><span style="color:#F97583;">function</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">e</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">			console.</span><span style="color:#B392F0;">log</span><span style="color:#E1E4E8;">(e);</span></span>
<span class="line"><span style="color:#E1E4E8;">		}</span></span>
<span class="line"><span style="color:#E1E4E8;">	})</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#D73A49;">let</span><span style="color:#24292E;"> uploadFileName </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">$</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;#uploadFile&#39;</span><span style="color:#24292E;">).</span><span style="color:#6F42C1;">val</span><span style="color:#24292E;">();</span></span>
<span class="line"><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (uploadFileName.</span><span style="color:#005CC5;">length</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">===</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">0</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">	mini.</span><span style="color:#6F42C1;">alert</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;请选择需要上传的文档信息&#39;</span><span style="color:#24292E;">);</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">return</span><span style="color:#24292E;">;</span></span>
<span class="line"><span style="color:#24292E;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#D73A49;">var</span><span style="color:#24292E;"> fd </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">new</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">FormData</span><span style="color:#24292E;">();</span></span>
<span class="line"><span style="color:#24292E;">fd.</span><span style="color:#6F42C1;">append</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;file&#39;</span><span style="color:#24292E;">, </span><span style="color:#6F42C1;">$</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;#uploadFile&quot;</span><span style="color:#24292E;">)[</span><span style="color:#005CC5;">0</span><span style="color:#24292E;">].files[</span><span style="color:#005CC5;">0</span><span style="color:#24292E;">]);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//	ajax</span></span>
<span class="line"><span style="color:#6F42C1;">befAjaxFormData</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;/openApi/uploadDocs.do&#39;</span><span style="color:#24292E;">, selectData, fd, </span><span style="color:#D73A49;">function</span><span style="color:#24292E;">(</span><span style="color:#E36209;">datas</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6F42C1;">showTips</span><span style="color:#24292E;">(datas.msg);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">})</span></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//封装一个方法</span></span>
<span class="line"><span style="color:#D73A49;">function</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">befAjaxFormData</span><span style="color:#24292E;">(</span><span style="color:#E36209;">url</span><span style="color:#24292E;">, </span><span style="color:#E36209;">params</span><span style="color:#24292E;">, </span><span style="color:#E36209;">formData</span><span style="color:#24292E;">, </span><span style="color:#E36209;">callback</span><span style="color:#24292E;">) {</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	params.gsxx_id </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">isEmpty</span><span style="color:#24292E;">(params.gsxx_id) </span><span style="color:#D73A49;">?</span><span style="color:#24292E;"> sessionStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;gsxx_id&#39;</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">:</span><span style="color:#24292E;"> params.gsxx_id;</span></span>
<span class="line"><span style="color:#24292E;">	params.login_id </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">isEmpty</span><span style="color:#24292E;">(params.login_id) </span><span style="color:#D73A49;">?</span><span style="color:#24292E;"> sessionStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;login_id&#39;</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">:</span><span style="color:#24292E;"> params.login_id;</span></span>
<span class="line"><span style="color:#24292E;">	params.login_bm </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">isEmpty</span><span style="color:#24292E;">(params.login_bm) </span><span style="color:#D73A49;">?</span><span style="color:#24292E;"> sessionStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;login_bm&#39;</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">:</span><span style="color:#24292E;"> params.login_bm;</span></span>
<span class="line"><span style="color:#24292E;">	params.mac </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">isEmpty</span><span style="color:#24292E;">(params.mac) </span><span style="color:#D73A49;">?</span><span style="color:#24292E;"> localStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;mac&#39;</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">:</span><span style="color:#24292E;"> params.mac;</span></span>
<span class="line"><span style="color:#24292E;">	params.bef_token </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;web_token:&quot;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> params.gsxx_id </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;:&quot;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> params.login_id;</span></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	formData.</span><span style="color:#6F42C1;">append</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;params&quot;</span><span style="color:#24292E;">, </span><span style="color:#005CC5;">JSON</span><span style="color:#24292E;">.</span><span style="color:#6F42C1;">stringify</span><span style="color:#24292E;">(params))</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">var</span><span style="color:#24292E;"> jzmessageid </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> mini.</span><span style="color:#6F42C1;">loading</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;正在加载,请稍等...&quot;</span><span style="color:#24292E;">, </span><span style="color:#032F62;">&quot;加载提示框&quot;</span><span style="color:#24292E;">);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	$.</span><span style="color:#6F42C1;">ajax</span><span style="color:#24292E;">({</span></span>
<span class="line"><span style="color:#24292E;">		method: </span><span style="color:#032F62;">&#39;POST&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		url: uploadUrl </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> url,</span></span>
<span class="line"><span style="color:#24292E;">		cache: </span><span style="color:#005CC5;">false</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		data: formData,</span></span>
<span class="line"><span style="color:#24292E;">		processData: </span><span style="color:#005CC5;">false</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		contentType: </span><span style="color:#005CC5;">false</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6F42C1;">success</span><span style="color:#24292E;">: </span><span style="color:#D73A49;">function</span><span style="color:#24292E;">(</span><span style="color:#E36209;">datas</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">			console.</span><span style="color:#6F42C1;">log</span><span style="color:#24292E;">(datas);</span></span>
<span class="line"><span style="color:#24292E;">			mini.</span><span style="color:#6F42C1;">hideMessageBox</span><span style="color:#24292E;">(jzmessageid);</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (datas.code </span><span style="color:#D73A49;">==</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">0</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">				</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (callback </span><span style="color:#D73A49;">!=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;undefined&quot;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">&amp;&amp;</span><span style="color:#24292E;"> callback </span><span style="color:#D73A49;">!=</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">null</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">					</span><span style="color:#6F42C1;">callback</span><span style="color:#24292E;">(datas);</span></span>
<span class="line"><span style="color:#24292E;">				}</span></span>
<span class="line"><span style="color:#24292E;">			} </span><span style="color:#D73A49;">else</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">				mini.</span><span style="color:#6F42C1;">alert</span><span style="color:#24292E;">(datas.msg, </span><span style="color:#032F62;">&quot;错误信息&quot;</span><span style="color:#24292E;">, </span><span style="color:#D73A49;">function</span><span style="color:#24292E;">() {</span></span>
<span class="line"><span style="color:#24292E;">					</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (datas.code </span><span style="color:#D73A49;">==</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">-</span><span style="color:#005CC5;">11</span><span style="color:#24292E;">) { </span><span style="color:#6A737D;">//token 验证失效</span></span>
<span class="line"><span style="color:#24292E;">						top[</span><span style="color:#032F62;">&quot;win&quot;</span><span style="color:#24292E;">].</span><span style="color:#6F42C1;">logout</span><span style="color:#24292E;">();</span></span>
<span class="line"><span style="color:#24292E;">					}</span></span>
<span class="line"><span style="color:#24292E;">				});</span></span>
<span class="line"><span style="color:#24292E;">			}</span></span>
<span class="line"><span style="color:#24292E;">		},</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6F42C1;">error</span><span style="color:#24292E;">: </span><span style="color:#D73A49;">function</span><span style="color:#24292E;">(</span><span style="color:#E36209;">e</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">			console.</span><span style="color:#6F42C1;">log</span><span style="color:#24292E;">(e);</span></span>
<span class="line"><span style="color:#24292E;">		}</span></span>
<span class="line"><span style="color:#24292E;">	})</span></span>
<span class="line"><span style="color:#24292E;">}</span></span></code></pre></div><h2 id="后端代码" tabindex="-1">后端代码 <a class="header-anchor" href="#后端代码" aria-label="Permalink to &quot;后端代码&quot;">​</a></h2><p>在后端的项目里，写一个controller</p><p>定义一个方法，参数如下，且需要和前端传过来的formdata的名称保持一致:</p><p>file:file</p><p>params:params</p><p>示例代码：</p><div class="language-java vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">java</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#E1E4E8;">@</span><span style="color:#F97583;">ApiOperation</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;上传文档&quot;</span><span style="color:#E1E4E8;">)</span></span>
<span class="line"><span style="color:#E1E4E8;">@</span><span style="color:#F97583;">RequestMapping</span><span style="color:#E1E4E8;">(</span><span style="color:#79B8FF;">value</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;uploadDocs.do&quot;</span><span style="color:#E1E4E8;">, </span><span style="color:#79B8FF;">method</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> RequestMethod.POST)</span></span>
<span class="line"><span style="color:#E1E4E8;">@</span><span style="color:#F97583;">OthersAnnotation</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;docs&quot;</span><span style="color:#E1E4E8;">)	</span><span style="color:#6A737D;">//	需要切换数据源所以加个注解</span></span>
<span class="line"><span style="color:#F97583;">public</span><span style="color:#E1E4E8;"> Map </span><span style="color:#B392F0;">uploadDocs</span><span style="color:#E1E4E8;">(@</span><span style="color:#F97583;">RequestPart</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;file&quot;</span><span style="color:#E1E4E8;">) MultipartFile file, String params) {</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	JSONObject jsonObject </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> JSONObject.</span><span style="color:#B392F0;">parseObject</span><span style="color:#E1E4E8;">(params);</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (file.</span><span style="color:#B392F0;">isEmpty</span><span style="color:#E1E4E8;">()) {</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> Result.</span><span style="color:#B392F0;">fail</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;文件不能为空...&quot;</span><span style="color:#E1E4E8;">);</span></span>
<span class="line"><span style="color:#E1E4E8;">	}</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#6A737D;">/*String fileType = file.getContentType();</span></span>
<span class="line"><span style="color:#6A737D;">	if (fileType != null &amp;&amp; !fileType.contains(&quot;pdf&quot;)) {</span></span>
<span class="line"><span style="color:#6A737D;">		// 处理PDF文件</span></span>
<span class="line"><span style="color:#6A737D;">		return Result.fail(&quot;只能上传pdf文件&quot;);</span></span>
<span class="line"><span style="color:#6A737D;">	}*/</span></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#6A737D;">//  路径 D:\\\\docs\\\\1108</span></span>
<span class="line"><span style="color:#E1E4E8;">	String imgFilePath </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;D:&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> File.separator </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;docs&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> File.separator </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> jsonObject.</span><span style="color:#B392F0;">getString</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;gsxx_id&quot;</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> File.separator;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">try</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#6A737D;">// 文件全称</span></span>
<span class="line"><span style="color:#E1E4E8;">		String fileName </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> file.</span><span style="color:#B392F0;">getOriginalFilename</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#6A737D;">//获取后缀名</span></span>
<span class="line"><span style="color:#E1E4E8;">		String sname </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> fileName.</span><span style="color:#B392F0;">substring</span><span style="color:#E1E4E8;">(fileName.</span><span style="color:#B392F0;">lastIndexOf</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;.&quot;</span><span style="color:#E1E4E8;">));</span></span>
<span class="line"><span style="color:#E1E4E8;">		UUID uuid </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> UUID.</span><span style="color:#B392F0;">randomUUID</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">		String newFileName </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> uuid </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> sname;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">		File paths </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">new</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">File</span><span style="color:#E1E4E8;">(imgFilePath </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> newFileName);</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (</span><span style="color:#F97583;">!</span><span style="color:#E1E4E8;">paths.</span><span style="color:#B392F0;">exists</span><span style="color:#E1E4E8;">()) {</span></span>
<span class="line"><span style="color:#E1E4E8;">			paths.</span><span style="color:#B392F0;">mkdirs</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"><span style="color:#E1E4E8;">		}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">		file.</span><span style="color:#B392F0;">transferTo</span><span style="color:#E1E4E8;">(paths);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">		Map&lt;</span><span style="color:#F97583;">String</span><span style="color:#E1E4E8;">, </span><span style="color:#F97583;">String</span><span style="color:#E1E4E8;">&gt; hashmap </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">new</span><span style="color:#E1E4E8;"> HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#6A737D;">//https://api.beferp.com/docs/1108\\8443294f-6b90-41f2-97a7-e12a7b02a557.pdf</span></span>
<span class="line"><span style="color:#E1E4E8;">		jsonObject.</span><span style="color:#B392F0;">put</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;osspath&quot;</span><span style="color:#E1E4E8;">, </span><span style="color:#9ECBFF;">&quot;https://api.beferp.com/docs/&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> jsonObject.</span><span style="color:#B392F0;">getString</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;gsxx_id&quot;</span><span style="color:#E1E4E8;">)</span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> File.separator </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> newFileName);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">		hashmap.</span><span style="color:#B392F0;">put</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;gsxx_id&quot;</span><span style="color:#E1E4E8;">, jsonObject.</span><span style="color:#B392F0;">getString</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;gsxx_id&quot;</span><span style="color:#E1E4E8;">));</span></span>
<span class="line"><span style="color:#E1E4E8;">		hashmap.</span><span style="color:#B392F0;">put</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;login_id&quot;</span><span style="color:#E1E4E8;">, jsonObject.</span><span style="color:#B392F0;">getString</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;login_id&quot;</span><span style="color:#E1E4E8;">));</span></span>
<span class="line"><span style="color:#E1E4E8;">		hashmap.</span><span style="color:#B392F0;">put</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;error_code&quot;</span><span style="color:#E1E4E8;">, </span><span style="color:#9ECBFF;">&quot;&quot;</span><span style="color:#E1E4E8;">);</span></span>
<span class="line"><span style="color:#E1E4E8;">		hashmap.</span><span style="color:#B392F0;">put</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;jsonData&quot;</span><span style="color:#E1E4E8;">, jsonObject.</span><span style="color:#B392F0;">toJSONString</span><span style="color:#E1E4E8;">());</span></span>
<span class="line"><span style="color:#E1E4E8;">		List list </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> mobileCommonMapper.</span><span style="color:#B392F0;">P_bef_file_p_i</span><span style="color:#E1E4E8;">(hashmap);</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> Result.</span><span style="color:#B392F0;">success</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;上传成功&quot;</span><span style="color:#E1E4E8;">);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">	} </span><span style="color:#F97583;">catch</span><span style="color:#E1E4E8;"> (Exception </span><span style="color:#FFAB70;">e</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">		e.</span><span style="color:#B392F0;">printStackTrace</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> Result.</span><span style="color:#B392F0;">fail</span><span style="color:#E1E4E8;">(e.</span><span style="color:#B392F0;">getMessage</span><span style="color:#E1E4E8;">());</span></span>
<span class="line"><span style="color:#E1E4E8;">	}</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292E;">@</span><span style="color:#D73A49;">ApiOperation</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;上传文档&quot;</span><span style="color:#24292E;">)</span></span>
<span class="line"><span style="color:#24292E;">@</span><span style="color:#D73A49;">RequestMapping</span><span style="color:#24292E;">(</span><span style="color:#005CC5;">value</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;uploadDocs.do&quot;</span><span style="color:#24292E;">, </span><span style="color:#005CC5;">method</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> RequestMethod.POST)</span></span>
<span class="line"><span style="color:#24292E;">@</span><span style="color:#D73A49;">OthersAnnotation</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;docs&quot;</span><span style="color:#24292E;">)	</span><span style="color:#6A737D;">//	需要切换数据源所以加个注解</span></span>
<span class="line"><span style="color:#D73A49;">public</span><span style="color:#24292E;"> Map </span><span style="color:#6F42C1;">uploadDocs</span><span style="color:#24292E;">(@</span><span style="color:#D73A49;">RequestPart</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;file&quot;</span><span style="color:#24292E;">) MultipartFile file, String params) {</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	JSONObject jsonObject </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> JSONObject.</span><span style="color:#6F42C1;">parseObject</span><span style="color:#24292E;">(params);</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (file.</span><span style="color:#6F42C1;">isEmpty</span><span style="color:#24292E;">()) {</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> Result.</span><span style="color:#6F42C1;">fail</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;文件不能为空...&quot;</span><span style="color:#24292E;">);</span></span>
<span class="line"><span style="color:#24292E;">	}</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6A737D;">/*String fileType = file.getContentType();</span></span>
<span class="line"><span style="color:#6A737D;">	if (fileType != null &amp;&amp; !fileType.contains(&quot;pdf&quot;)) {</span></span>
<span class="line"><span style="color:#6A737D;">		// 处理PDF文件</span></span>
<span class="line"><span style="color:#6A737D;">		return Result.fail(&quot;只能上传pdf文件&quot;);</span></span>
<span class="line"><span style="color:#6A737D;">	}*/</span></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6A737D;">//  路径 D:\\\\docs\\\\1108</span></span>
<span class="line"><span style="color:#24292E;">	String imgFilePath </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;D:&quot;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> File.separator </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;docs&quot;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> File.separator </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> jsonObject.</span><span style="color:#6F42C1;">getString</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;gsxx_id&quot;</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> File.separator;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">try</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6A737D;">// 文件全称</span></span>
<span class="line"><span style="color:#24292E;">		String fileName </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> file.</span><span style="color:#6F42C1;">getOriginalFilename</span><span style="color:#24292E;">();</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6A737D;">//获取后缀名</span></span>
<span class="line"><span style="color:#24292E;">		String sname </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> fileName.</span><span style="color:#6F42C1;">substring</span><span style="color:#24292E;">(fileName.</span><span style="color:#6F42C1;">lastIndexOf</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;.&quot;</span><span style="color:#24292E;">));</span></span>
<span class="line"><span style="color:#24292E;">		UUID uuid </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> UUID.</span><span style="color:#6F42C1;">randomUUID</span><span style="color:#24292E;">();</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">		String newFileName </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> uuid </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> sname;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">		File paths </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">new</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">File</span><span style="color:#24292E;">(imgFilePath </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> newFileName);</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (</span><span style="color:#D73A49;">!</span><span style="color:#24292E;">paths.</span><span style="color:#6F42C1;">exists</span><span style="color:#24292E;">()) {</span></span>
<span class="line"><span style="color:#24292E;">			paths.</span><span style="color:#6F42C1;">mkdirs</span><span style="color:#24292E;">();</span></span>
<span class="line"><span style="color:#24292E;">		}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">		file.</span><span style="color:#6F42C1;">transferTo</span><span style="color:#24292E;">(paths);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">		Map&lt;</span><span style="color:#D73A49;">String</span><span style="color:#24292E;">, </span><span style="color:#D73A49;">String</span><span style="color:#24292E;">&gt; hashmap </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">new</span><span style="color:#24292E;"> HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#24292E;">		</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6A737D;">//https://api.beferp.com/docs/1108\\8443294f-6b90-41f2-97a7-e12a7b02a557.pdf</span></span>
<span class="line"><span style="color:#24292E;">		jsonObject.</span><span style="color:#6F42C1;">put</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;osspath&quot;</span><span style="color:#24292E;">, </span><span style="color:#032F62;">&quot;https://api.beferp.com/docs/&quot;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> jsonObject.</span><span style="color:#6F42C1;">getString</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;gsxx_id&quot;</span><span style="color:#24292E;">)</span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> File.separator </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> newFileName);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">		hashmap.</span><span style="color:#6F42C1;">put</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;gsxx_id&quot;</span><span style="color:#24292E;">, jsonObject.</span><span style="color:#6F42C1;">getString</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;gsxx_id&quot;</span><span style="color:#24292E;">));</span></span>
<span class="line"><span style="color:#24292E;">		hashmap.</span><span style="color:#6F42C1;">put</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;login_id&quot;</span><span style="color:#24292E;">, jsonObject.</span><span style="color:#6F42C1;">getString</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;login_id&quot;</span><span style="color:#24292E;">));</span></span>
<span class="line"><span style="color:#24292E;">		hashmap.</span><span style="color:#6F42C1;">put</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;error_code&quot;</span><span style="color:#24292E;">, </span><span style="color:#032F62;">&quot;&quot;</span><span style="color:#24292E;">);</span></span>
<span class="line"><span style="color:#24292E;">		hashmap.</span><span style="color:#6F42C1;">put</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;jsonData&quot;</span><span style="color:#24292E;">, jsonObject.</span><span style="color:#6F42C1;">toJSONString</span><span style="color:#24292E;">());</span></span>
<span class="line"><span style="color:#24292E;">		List list </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> mobileCommonMapper.</span><span style="color:#6F42C1;">P_bef_file_p_i</span><span style="color:#24292E;">(hashmap);</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> Result.</span><span style="color:#6F42C1;">success</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;上传成功&quot;</span><span style="color:#24292E;">);</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">	} </span><span style="color:#D73A49;">catch</span><span style="color:#24292E;"> (Exception </span><span style="color:#E36209;">e</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">		e.</span><span style="color:#6F42C1;">printStackTrace</span><span style="color:#24292E;">();</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> Result.</span><span style="color:#6F42C1;">fail</span><span style="color:#24292E;">(e.</span><span style="color:#6F42C1;">getMessage</span><span style="color:#24292E;">());</span></span>
<span class="line"><span style="color:#24292E;">	}</span></span>
<span class="line"><span style="color:#24292E;">}</span></span></code></pre></div><p>@OthersAnnotation注解：</p><p>定义一个注解，在ApiAspet切面里使用</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">@Target(ElementType.METHOD)</span></span>
<span class="line"><span style="color:#e1e4e8;">@Retention(RetentionPolicy.RUNTIME)</span></span>
<span class="line"><span style="color:#e1e4e8;">@Documented</span></span>
<span class="line"><span style="color:#e1e4e8;">public @interface OthersAnnotation {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    String value() default &quot;&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">@Target(ElementType.METHOD)</span></span>
<span class="line"><span style="color:#24292e;">@Retention(RetentionPolicy.RUNTIME)</span></span>
<span class="line"><span style="color:#24292e;">@Documented</span></span>
<span class="line"><span style="color:#24292e;">public @interface OthersAnnotation {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    String value() default &quot;&quot;;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><p>因为需要切换数据源，所以在进入接口之前需要通过gsxx_id来判断切换至哪个数据源</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">@Pointcut(&quot;@annotation(com.bef.annotation.OthersAnnotation)&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">public void otherAspect() {</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">@Before(&quot;otherAspect()&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">public void otherBefore(JoinPoint joinPoint) {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	MethodSignature signature = (MethodSignature) joinPoint.getSignature();</span></span>
<span class="line"><span style="color:#e1e4e8;">	Method method = signature.getMethod();</span></span>
<span class="line"><span style="color:#e1e4e8;">	OthersAnnotation annotation = method.getAnnotation(OthersAnnotation.class);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	Object[] args = joinPoint.getArgs();</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	//  上传文档</span></span>
<span class="line"><span style="color:#e1e4e8;">	if (Objects.equals(annotation.value(), &quot;docs&quot;)) {</span></span>
<span class="line"><span style="color:#e1e4e8;">		HashMap hashMap = JSON.parseObject(args[1].toString(), HashMap.class);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">		// 从 http 请求头中取出 token</span></span>
<span class="line"><span style="color:#e1e4e8;">		String tokenKey = hashMap.get(&quot;bef_token&quot;).toString();</span></span>
<span class="line"><span style="color:#e1e4e8;">		// 执行认证</span></span>
<span class="line"><span style="color:#e1e4e8;">		if (StringUtils.isEmpty(tokenKey)) {</span></span>
<span class="line"><span style="color:#e1e4e8;">			throw new MyException(-11, &quot;无token，请重新登录&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">		}</span></span>
<span class="line"><span style="color:#e1e4e8;">		try {</span></span>
<span class="line"><span style="color:#e1e4e8;">			tokenKey = URLDecoder.decode(tokenKey, &quot;UTF-8&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">		} catch (UnsupportedEncodingException e) {</span></span>
<span class="line"><span style="color:#e1e4e8;">			e.printStackTrace();</span></span>
<span class="line"><span style="color:#e1e4e8;">		}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">		Token token = (Token) redisUtil.get(tokenKey);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">		commonUtils.findLockByUrl(token.getDatasourceKey(), Integer.parseInt(token.getGsxx_id()), true);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	}</span></span>
<span class="line"><span style="color:#e1e4e8;">	//	查询及删除</span></span>
<span class="line"><span style="color:#e1e4e8;">	if (Objects.equals(annotation.value(), &quot;idocs&quot;)) {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">		for (Object obj : args) {</span></span>
<span class="line"><span style="color:#e1e4e8;">			HashMap hashMap = JSON.parseObject(obj.toString(), HashMap.class);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">			String tokenKey = hashMap.get(&quot;bef_token&quot;).toString();</span></span>
<span class="line"><span style="color:#e1e4e8;">			// 执行认证</span></span>
<span class="line"><span style="color:#e1e4e8;">			if (StringUtils.isEmpty(tokenKey)) {</span></span>
<span class="line"><span style="color:#e1e4e8;">				throw new MyException(-11, &quot;无token，请重新登录&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">			}</span></span>
<span class="line"><span style="color:#e1e4e8;">			try {</span></span>
<span class="line"><span style="color:#e1e4e8;">				tokenKey = URLDecoder.decode(tokenKey, &quot;UTF-8&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">			} catch (UnsupportedEncodingException e) {</span></span>
<span class="line"><span style="color:#e1e4e8;">				e.printStackTrace();</span></span>
<span class="line"><span style="color:#e1e4e8;">			}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">			Token token = (Token) redisUtil.get(tokenKey);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">			commonUtils.findLockByUrl(token.getDatasourceKey(), Integer.parseInt(token.getGsxx_id()), true);</span></span>
<span class="line"><span style="color:#e1e4e8;">		}</span></span>
<span class="line"><span style="color:#e1e4e8;">	}</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">@Pointcut(&quot;@annotation(com.bef.annotation.OthersAnnotation)&quot;)</span></span>
<span class="line"><span style="color:#24292e;">public void otherAspect() {</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">@Before(&quot;otherAspect()&quot;)</span></span>
<span class="line"><span style="color:#24292e;">public void otherBefore(JoinPoint joinPoint) {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	MethodSignature signature = (MethodSignature) joinPoint.getSignature();</span></span>
<span class="line"><span style="color:#24292e;">	Method method = signature.getMethod();</span></span>
<span class="line"><span style="color:#24292e;">	OthersAnnotation annotation = method.getAnnotation(OthersAnnotation.class);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	Object[] args = joinPoint.getArgs();</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	//  上传文档</span></span>
<span class="line"><span style="color:#24292e;">	if (Objects.equals(annotation.value(), &quot;docs&quot;)) {</span></span>
<span class="line"><span style="color:#24292e;">		HashMap hashMap = JSON.parseObject(args[1].toString(), HashMap.class);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">		// 从 http 请求头中取出 token</span></span>
<span class="line"><span style="color:#24292e;">		String tokenKey = hashMap.get(&quot;bef_token&quot;).toString();</span></span>
<span class="line"><span style="color:#24292e;">		// 执行认证</span></span>
<span class="line"><span style="color:#24292e;">		if (StringUtils.isEmpty(tokenKey)) {</span></span>
<span class="line"><span style="color:#24292e;">			throw new MyException(-11, &quot;无token，请重新登录&quot;);</span></span>
<span class="line"><span style="color:#24292e;">		}</span></span>
<span class="line"><span style="color:#24292e;">		try {</span></span>
<span class="line"><span style="color:#24292e;">			tokenKey = URLDecoder.decode(tokenKey, &quot;UTF-8&quot;);</span></span>
<span class="line"><span style="color:#24292e;">		} catch (UnsupportedEncodingException e) {</span></span>
<span class="line"><span style="color:#24292e;">			e.printStackTrace();</span></span>
<span class="line"><span style="color:#24292e;">		}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">		Token token = (Token) redisUtil.get(tokenKey);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">		commonUtils.findLockByUrl(token.getDatasourceKey(), Integer.parseInt(token.getGsxx_id()), true);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	}</span></span>
<span class="line"><span style="color:#24292e;">	//	查询及删除</span></span>
<span class="line"><span style="color:#24292e;">	if (Objects.equals(annotation.value(), &quot;idocs&quot;)) {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">		for (Object obj : args) {</span></span>
<span class="line"><span style="color:#24292e;">			HashMap hashMap = JSON.parseObject(obj.toString(), HashMap.class);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">			String tokenKey = hashMap.get(&quot;bef_token&quot;).toString();</span></span>
<span class="line"><span style="color:#24292e;">			// 执行认证</span></span>
<span class="line"><span style="color:#24292e;">			if (StringUtils.isEmpty(tokenKey)) {</span></span>
<span class="line"><span style="color:#24292e;">				throw new MyException(-11, &quot;无token，请重新登录&quot;);</span></span>
<span class="line"><span style="color:#24292e;">			}</span></span>
<span class="line"><span style="color:#24292e;">			try {</span></span>
<span class="line"><span style="color:#24292e;">				tokenKey = URLDecoder.decode(tokenKey, &quot;UTF-8&quot;);</span></span>
<span class="line"><span style="color:#24292e;">			} catch (UnsupportedEncodingException e) {</span></span>
<span class="line"><span style="color:#24292e;">				e.printStackTrace();</span></span>
<span class="line"><span style="color:#24292e;">			}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">			Token token = (Token) redisUtil.get(tokenKey);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">			commonUtils.findLockByUrl(token.getDatasourceKey(), Integer.parseInt(token.getGsxx_id()), true);</span></span>
<span class="line"><span style="color:#24292e;">		}</span></span>
<span class="line"><span style="color:#24292e;">	}</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><p>后端发送formdata格式</p><div class="language-java vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">java</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#E1E4E8;">@</span><span style="color:#F97583;">RequestMapping</span><span style="color:#E1E4E8;">(</span><span style="color:#79B8FF;">value</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;uploadDocs.do&quot;</span><span style="color:#E1E4E8;">, </span><span style="color:#79B8FF;">method</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> RequestMethod.POST, </span><span style="color:#79B8FF;">consumes</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> MediaType.MULTIPART_FORM_DATA_VALUE)</span></span>
<span class="line"><span style="color:#F97583;">public</span><span style="color:#E1E4E8;"> Map </span><span style="color:#B392F0;">uploadDocs</span><span style="color:#E1E4E8;">(@</span><span style="color:#F97583;">RequestPart</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;file&quot;</span><span style="color:#E1E4E8;">) MultipartFile file, String params) {</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">    JSONObject jsonObject </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> JSONObject.</span><span style="color:#B392F0;">parseObject</span><span style="color:#E1E4E8;">(params);</span></span>
<span class="line"><span style="color:#E1E4E8;">    </span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (file.</span><span style="color:#B392F0;">isEmpty</span><span style="color:#E1E4E8;">()) {</span></span>
<span class="line"><span style="color:#E1E4E8;">        </span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> Result.</span><span style="color:#B392F0;">fail</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;文件不能为空...&quot;</span><span style="color:#E1E4E8;">);</span></span>
<span class="line"><span style="color:#E1E4E8;">    }</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">    </span><span style="color:#F97583;">try</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">        Resource resource </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> file.</span><span style="color:#B392F0;">getResource</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">        MultiValueMap multiValueMap </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">new</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">LinkedMultiValueMap</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"><span style="color:#E1E4E8;">        multiValueMap.</span><span style="color:#B392F0;">add</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;file&quot;</span><span style="color:#E1E4E8;">, resource);</span></span>
<span class="line"><span style="color:#E1E4E8;">        multiValueMap.</span><span style="color:#B392F0;">add</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;params&quot;</span><span style="color:#E1E4E8;">, params);</span></span>
<span class="line"><span style="color:#E1E4E8;">        String string </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> HttpClientUtils.</span><span style="color:#B392F0;">sendFormDataRequest</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&quot;https://api.beferp.com/befapi/openApi/uploadDocs.do&quot;</span><span style="color:#E1E4E8;">, multiValueMap);</span></span>
<span class="line"><span style="color:#E1E4E8;">        JSONObject json </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> JSONObject.</span><span style="color:#B392F0;">parseObject</span><span style="color:#E1E4E8;">(string);</span></span>
<span class="line"><span style="color:#E1E4E8;">        </span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> json;</span></span>
<span class="line"><span style="color:#E1E4E8;">    } </span><span style="color:#F97583;">catch</span><span style="color:#E1E4E8;"> (Exception </span><span style="color:#FFAB70;">e</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">        e.</span><span style="color:#B392F0;">printStackTrace</span><span style="color:#E1E4E8;">();</span></span>
<span class="line"><span style="color:#E1E4E8;">    }</span></span>
<span class="line"><span style="color:#E1E4E8;">    </span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">null</span><span style="color:#E1E4E8;">;</span></span>
<span class="line"><span style="color:#E1E4E8;">    </span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292E;">@</span><span style="color:#D73A49;">RequestMapping</span><span style="color:#24292E;">(</span><span style="color:#005CC5;">value</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;uploadDocs.do&quot;</span><span style="color:#24292E;">, </span><span style="color:#005CC5;">method</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> RequestMethod.POST, </span><span style="color:#005CC5;">consumes</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> MediaType.MULTIPART_FORM_DATA_VALUE)</span></span>
<span class="line"><span style="color:#D73A49;">public</span><span style="color:#24292E;"> Map </span><span style="color:#6F42C1;">uploadDocs</span><span style="color:#24292E;">(@</span><span style="color:#D73A49;">RequestPart</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;file&quot;</span><span style="color:#24292E;">) MultipartFile file, String params) {</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">    JSONObject jsonObject </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> JSONObject.</span><span style="color:#6F42C1;">parseObject</span><span style="color:#24292E;">(params);</span></span>
<span class="line"><span style="color:#24292E;">    </span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (file.</span><span style="color:#6F42C1;">isEmpty</span><span style="color:#24292E;">()) {</span></span>
<span class="line"><span style="color:#24292E;">        </span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> Result.</span><span style="color:#6F42C1;">fail</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;文件不能为空...&quot;</span><span style="color:#24292E;">);</span></span>
<span class="line"><span style="color:#24292E;">    }</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">    </span><span style="color:#D73A49;">try</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">        Resource resource </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> file.</span><span style="color:#6F42C1;">getResource</span><span style="color:#24292E;">();</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">        MultiValueMap multiValueMap </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">new</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">LinkedMultiValueMap</span><span style="color:#24292E;">();</span></span>
<span class="line"><span style="color:#24292E;">        multiValueMap.</span><span style="color:#6F42C1;">add</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;file&quot;</span><span style="color:#24292E;">, resource);</span></span>
<span class="line"><span style="color:#24292E;">        multiValueMap.</span><span style="color:#6F42C1;">add</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;params&quot;</span><span style="color:#24292E;">, params);</span></span>
<span class="line"><span style="color:#24292E;">        String string </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> HttpClientUtils.</span><span style="color:#6F42C1;">sendFormDataRequest</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&quot;https://api.beferp.com/befapi/openApi/uploadDocs.do&quot;</span><span style="color:#24292E;">, multiValueMap);</span></span>
<span class="line"><span style="color:#24292E;">        JSONObject json </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> JSONObject.</span><span style="color:#6F42C1;">parseObject</span><span style="color:#24292E;">(string);</span></span>
<span class="line"><span style="color:#24292E;">        </span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> json;</span></span>
<span class="line"><span style="color:#24292E;">    } </span><span style="color:#D73A49;">catch</span><span style="color:#24292E;"> (Exception </span><span style="color:#E36209;">e</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">        e.</span><span style="color:#6F42C1;">printStackTrace</span><span style="color:#24292E;">();</span></span>
<span class="line"><span style="color:#24292E;">    }</span></span>
<span class="line"><span style="color:#24292E;">    </span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">null</span><span style="color:#24292E;">;</span></span>
<span class="line"><span style="color:#24292E;">    </span></span>
<span class="line"><span style="color:#24292E;">}</span></span></code></pre></div><p>HttpClientUtils</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * 发送formData格式的数据</span></span>
<span class="line"><span style="color:#e1e4e8;"> *</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @param url</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @param params</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @return</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">public static String sendFormDataRequest(String url, MultiValueMap&lt;String, String&gt; params) {</span></span>
<span class="line"><span style="color:#e1e4e8;">    RestTemplate client = new RestTemplate();</span></span>
<span class="line"><span style="color:#e1e4e8;">    HttpHeaders headers = new HttpHeaders();</span></span>
<span class="line"><span style="color:#e1e4e8;">    HttpMethod method = HttpMethod.POST;</span></span>
<span class="line"><span style="color:#e1e4e8;">    // 以表单的方式提交</span></span>
<span class="line"><span style="color:#e1e4e8;">    headers.setContentType(MediaType.MULTIPART_FORM_DATA);</span></span>
<span class="line"><span style="color:#e1e4e8;">    //将请求头部和参数合成一个请求</span></span>
<span class="line"><span style="color:#e1e4e8;">    HttpEntity&lt;MultiValueMap&lt;String, String&gt;&gt; requestEntity = new HttpEntity&lt;&gt;(params, headers);</span></span>
<span class="line"><span style="color:#e1e4e8;">    //执行HTTP请求，将返回的结构使用String 类格式化</span></span>
<span class="line"><span style="color:#e1e4e8;">    ResponseEntity&lt;String&gt; response = client.exchange(url, method, requestEntity, String.class);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    return response.getBody();</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * 发送formData格式的数据</span></span>
<span class="line"><span style="color:#24292e;"> *</span></span>
<span class="line"><span style="color:#24292e;"> * @param url</span></span>
<span class="line"><span style="color:#24292e;"> * @param params</span></span>
<span class="line"><span style="color:#24292e;"> * @return</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">public static String sendFormDataRequest(String url, MultiValueMap&lt;String, String&gt; params) {</span></span>
<span class="line"><span style="color:#24292e;">    RestTemplate client = new RestTemplate();</span></span>
<span class="line"><span style="color:#24292e;">    HttpHeaders headers = new HttpHeaders();</span></span>
<span class="line"><span style="color:#24292e;">    HttpMethod method = HttpMethod.POST;</span></span>
<span class="line"><span style="color:#24292e;">    // 以表单的方式提交</span></span>
<span class="line"><span style="color:#24292e;">    headers.setContentType(MediaType.MULTIPART_FORM_DATA);</span></span>
<span class="line"><span style="color:#24292e;">    //将请求头部和参数合成一个请求</span></span>
<span class="line"><span style="color:#24292e;">    HttpEntity&lt;MultiValueMap&lt;String, String&gt;&gt; requestEntity = new HttpEntity&lt;&gt;(params, headers);</span></span>
<span class="line"><span style="color:#24292e;">    //执行HTTP请求，将返回的结构使用String 类格式化</span></span>
<span class="line"><span style="color:#24292e;">    ResponseEntity&lt;String&gt; response = client.exchange(url, method, requestEntity, String.class);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    return response.getBody();</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><p>application.yml</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">spring:</span></span>
<span class="line"><span style="color:#e1e4e8;">  servlet:</span></span>
<span class="line"><span style="color:#e1e4e8;">    multipart:</span></span>
<span class="line"><span style="color:#e1e4e8;">      # 默认支持文件上传</span></span>
<span class="line"><span style="color:#e1e4e8;">      enabled: true</span></span>
<span class="line"><span style="color:#e1e4e8;">      # 最大支持文件大小</span></span>
<span class="line"><span style="color:#e1e4e8;">      max-file-size: 50MB</span></span>
<span class="line"><span style="color:#e1e4e8;">      # 最大支持请求大小</span></span>
<span class="line"><span style="color:#e1e4e8;">      max-request-size: 100MB</span></span>
<span class="line"><span style="color:#e1e4e8;">      # 文件支持写入磁盘</span></span>
<span class="line"><span style="color:#e1e4e8;">      file-size-threshold: 0</span></span>
<span class="line"><span style="color:#e1e4e8;">      # 上传文件的临时目录</span></span>
<span class="line"><span style="color:#e1e4e8;">      location: /test</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">spring:</span></span>
<span class="line"><span style="color:#24292e;">  servlet:</span></span>
<span class="line"><span style="color:#24292e;">    multipart:</span></span>
<span class="line"><span style="color:#24292e;">      # 默认支持文件上传</span></span>
<span class="line"><span style="color:#24292e;">      enabled: true</span></span>
<span class="line"><span style="color:#24292e;">      # 最大支持文件大小</span></span>
<span class="line"><span style="color:#24292e;">      max-file-size: 50MB</span></span>
<span class="line"><span style="color:#24292e;">      # 最大支持请求大小</span></span>
<span class="line"><span style="color:#24292e;">      max-request-size: 100MB</span></span>
<span class="line"><span style="color:#24292e;">      # 文件支持写入磁盘</span></span>
<span class="line"><span style="color:#24292e;">      file-size-threshold: 0</span></span>
<span class="line"><span style="color:#24292e;">      # 上传文件的临时目录</span></span>
<span class="line"><span style="color:#24292e;">      location: /test</span></span></code></pre></div>`,26),e=[o];function t(c,r,y,E,i,u){return n(),a("div",null,e)}const g=s(l,[["render",t]]);export{d as __pageData,g as default};
