import{_ as s,o as n,c as a,S as e}from"./chunks/framework.d7c45c4d.js";const l="/blog/assets/vite.99156ce2.jpg",m=JSON.parse('{"title":"vite+ts+vue3+axios+elementui-plus","description":"","frontmatter":{},"headers":[],"relativePath":"guide/web/ts/index.md","filePath":"guide/web/ts/index.md","lastUpdated":1705138198000}'),p={name:"guide/web/ts/index.md"},t=e(`<h1 id="vite-ts-vue3-axios-elementui-plus" tabindex="-1">vite+ts+vue3+axios+elementui-plus <a class="header-anchor" href="#vite-ts-vue3-axios-elementui-plus" aria-label="Permalink to &quot;vite+ts+vue3+axios+elementui-plus&quot;">​</a></h1><h2 id="_1-使用vite初步搭建" tabindex="-1">1. 使用vite初步搭建 <a class="header-anchor" href="#_1-使用vite初步搭建" aria-label="Permalink to &quot;1. 使用vite初步搭建&quot;">​</a></h2><p>根据官网指示，vite搭建系统共5步:</p><p>官网地址：<code>https://vitejs.cn/vite3-cn/guide/#community-templates</code></p><ol><li><p>在磁盘新建空白文件夹</p></li><li><p>cd进入文件夹，打开cmd输入指令:</p></li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">npm create vite@latest</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">npm create vite@latest</span></span></code></pre></div><ol start="3"><li><p>根据控制台提示，输入项目名map，选择vue、ts模板</p></li><li><p>进入项目根目录</p></li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">cd map</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">cd map</span></span></code></pre></div><ol start="5"><li>下载依赖启动</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">npm install</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">npm run dev</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">npm install</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">npm run dev</span></span></code></pre></div><p><img src="`+l+`" alt="Alt text"></p><h2 id="_2-引入route4" tabindex="-1">2. 引入route4 <a class="header-anchor" href="#_2-引入route4" aria-label="Permalink to &quot;2. 引入route4&quot;">​</a></h2><ol><li>安装router4</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">npm install vue-router@4</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">npm install vue-router@4</span></span></code></pre></div><ol start="2"><li>在src文件下新建一个router文件夹，然后在这下面新建一个index.ts文件</li></ol><div class="language-index.ts vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">index.ts</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">// createWebHashHistory 是hash模式就是访问链接带有#</span></span>
<span class="line"><span style="color:#e1e4e8;">// createWebHistory  是history模式</span></span>
<span class="line"><span style="color:#e1e4e8;">import { createRouter, createWebHashHistory, createWebHistory } from &#39;vue-router&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;">// 引入文件，动态路由</span></span>
<span class="line"><span style="color:#e1e4e8;">const Login = () =&gt; import(&quot;../views/Login.vue&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">const Home = () =&gt; import(&quot;../views/Home.vue&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">const About = () =&gt; import(&quot;../views/About.vue&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">const NotFound = () =&gt; import(&quot;../views/NotFound.vue&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;">// 这里要注意一点，如下面这种写的话会报错，ts</span></span>
<span class="line"><span style="color:#e1e4e8;">// {</span></span>
<span class="line"><span style="color:#e1e4e8;">//    path: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">//    name: &quot;Home&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">//    component: () =&gt; import(&quot;../views/Home.vue&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">//  },</span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;">const routes = [</span></span>
<span class="line"><span style="color:#e1e4e8;">  {</span></span>
<span class="line"><span style="color:#e1e4e8;">    path: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    name: &quot;Login&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    component: Login,</span></span>
<span class="line"><span style="color:#e1e4e8;">  },</span></span>
<span class="line"><span style="color:#e1e4e8;">  {</span></span>
<span class="line"><span style="color:#e1e4e8;">    path: &quot;/Home&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    name: &quot;Home&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    component: Home,</span></span>
<span class="line"><span style="color:#e1e4e8;">  },</span></span>
<span class="line"><span style="color:#e1e4e8;">  {</span></span>
<span class="line"><span style="color:#e1e4e8;">    path: &quot;/About&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    name: &quot;About&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    component: About,</span></span>
<span class="line"><span style="color:#e1e4e8;">  },</span></span>
<span class="line"><span style="color:#e1e4e8;">  {</span></span>
<span class="line"><span style="color:#e1e4e8;">    path: &quot;/:pathMatch(.*)*&quot;, // 代替vue2的通配符path: &quot;*&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    name: &quot;NotFound&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    component: NotFound,</span></span>
<span class="line"><span style="color:#e1e4e8;">  },</span></span>
<span class="line"><span style="color:#e1e4e8;">];</span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;">const router = createRouter({</span></span>
<span class="line"><span style="color:#e1e4e8;">    history: createWebHistory(), //history模式 这个模式就是去掉#号</span></span>
<span class="line"><span style="color:#e1e4e8;">	//history:createWebHashHistory() //hash模式</span></span>
<span class="line"><span style="color:#e1e4e8;">    routes</span></span>
<span class="line"><span style="color:#e1e4e8;">})</span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;">export default router</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">// createWebHashHistory 是hash模式就是访问链接带有#</span></span>
<span class="line"><span style="color:#24292e;">// createWebHistory  是history模式</span></span>
<span class="line"><span style="color:#24292e;">import { createRouter, createWebHashHistory, createWebHistory } from &#39;vue-router&#39;</span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;">// 引入文件，动态路由</span></span>
<span class="line"><span style="color:#24292e;">const Login = () =&gt; import(&quot;../views/Login.vue&quot;);</span></span>
<span class="line"><span style="color:#24292e;">const Home = () =&gt; import(&quot;../views/Home.vue&quot;);</span></span>
<span class="line"><span style="color:#24292e;">const About = () =&gt; import(&quot;../views/About.vue&quot;);</span></span>
<span class="line"><span style="color:#24292e;">const NotFound = () =&gt; import(&quot;../views/NotFound.vue&quot;);</span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;">// 这里要注意一点，如下面这种写的话会报错，ts</span></span>
<span class="line"><span style="color:#24292e;">// {</span></span>
<span class="line"><span style="color:#24292e;">//    path: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#24292e;">//    name: &quot;Home&quot;,</span></span>
<span class="line"><span style="color:#24292e;">//    component: () =&gt; import(&quot;../views/Home.vue&quot;)</span></span>
<span class="line"><span style="color:#24292e;">//  },</span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;">const routes = [</span></span>
<span class="line"><span style="color:#24292e;">  {</span></span>
<span class="line"><span style="color:#24292e;">    path: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    name: &quot;Login&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    component: Login,</span></span>
<span class="line"><span style="color:#24292e;">  },</span></span>
<span class="line"><span style="color:#24292e;">  {</span></span>
<span class="line"><span style="color:#24292e;">    path: &quot;/Home&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    name: &quot;Home&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    component: Home,</span></span>
<span class="line"><span style="color:#24292e;">  },</span></span>
<span class="line"><span style="color:#24292e;">  {</span></span>
<span class="line"><span style="color:#24292e;">    path: &quot;/About&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    name: &quot;About&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    component: About,</span></span>
<span class="line"><span style="color:#24292e;">  },</span></span>
<span class="line"><span style="color:#24292e;">  {</span></span>
<span class="line"><span style="color:#24292e;">    path: &quot;/:pathMatch(.*)*&quot;, // 代替vue2的通配符path: &quot;*&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    name: &quot;NotFound&quot;,</span></span>
<span class="line"><span style="color:#24292e;">    component: NotFound,</span></span>
<span class="line"><span style="color:#24292e;">  },</span></span>
<span class="line"><span style="color:#24292e;">];</span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;">const router = createRouter({</span></span>
<span class="line"><span style="color:#24292e;">    history: createWebHistory(), //history模式 这个模式就是去掉#号</span></span>
<span class="line"><span style="color:#24292e;">	//history:createWebHashHistory() //hash模式</span></span>
<span class="line"><span style="color:#24292e;">    routes</span></span>
<span class="line"><span style="color:#24292e;">})</span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;">export default router</span></span></code></pre></div><ol start="3"><li>在main.ts注入</li></ol><div class="language-main.ts vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">main.ts</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import router from &#39;./router&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">createApp(App).use(router).mount(&#39;#app&#39;)</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import router from &#39;./router&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">createApp(App).use(router).mount(&#39;#app&#39;)</span></span></code></pre></div><ol start="4"><li>使用</li></ol><div class="language-App.vue vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">App.vue</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">&lt;script setup lang=&quot;ts&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/script&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">  &lt;div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;router-view&gt;&lt;/router-view&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">  &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;style &gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/style&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">&lt;script setup lang=&quot;ts&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/script&gt;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#24292e;">  &lt;div&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;router-view&gt;&lt;/router-view&gt;</span></span>
<span class="line"><span style="color:#24292e;">  &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;style &gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/style&gt;</span></span></code></pre></div><h2 id="_3-使用elementui-plus" tabindex="-1">3. 使用elementui-plus <a class="header-anchor" href="#_3-使用elementui-plus" aria-label="Permalink to &quot;3. 使用elementui-plus&quot;">​</a></h2><ol><li>安装</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">npm install element-plus --save</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">npm install element-plus --save</span></span></code></pre></div><ol start="2"><li>引用</li></ol><div class="language-main.ts vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">main.ts</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">// main.ts</span></span>
<span class="line"><span style="color:#e1e4e8;">import { createApp } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">import ElementPlus from &#39;element-plus&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">import &#39;element-plus/dist/index.css&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">import App from &#39;./App.vue&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">const app = createApp(App)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">app.use(ElementPlus)</span></span>
<span class="line"><span style="color:#e1e4e8;">app.mount(&#39;#app&#39;)</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">// main.ts</span></span>
<span class="line"><span style="color:#24292e;">import { createApp } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#24292e;">import ElementPlus from &#39;element-plus&#39;</span></span>
<span class="line"><span style="color:#24292e;">import &#39;element-plus/dist/index.css&#39;</span></span>
<span class="line"><span style="color:#24292e;">import App from &#39;./App.vue&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">const app = createApp(App)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">app.use(ElementPlus)</span></span>
<span class="line"><span style="color:#24292e;">app.mount(&#39;#app&#39;)</span></span></code></pre></div><ol start="3"><li>页面使用</li></ol><div class="language-Login.vue vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Login.vue</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;div class=&quot;cont&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">        &lt;div class=&quot;div-login&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;div class=&quot;div-login-title&quot;&gt;后台管理系统&lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">                &lt;el-input v-model=&quot;account&quot; class=&quot;w-50 m-2 input&quot; placeholder=&quot;请输入账号&quot; prefix-icon=&quot;User&quot; /&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">                &lt;el-input v-model=&quot;password&quot; class=&quot;w-50 m-2 input&quot; placeholder=&quot;请输入密码&quot; prefix-icon=&quot;Unlock&quot; /&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">                &lt;el-button type=&quot;primary&quot; @click=&quot;login()&quot;&gt;登录&lt;/el-button&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">        &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;style scoped&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">.cont {</span></span>
<span class="line"><span style="color:#e1e4e8;">    height: 100vh;</span></span>
<span class="line"><span style="color:#e1e4e8;">    width: 100vw;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">.div-login {</span></span>
<span class="line"><span style="color:#e1e4e8;">    position: absolute;</span></span>
<span class="line"><span style="color:#e1e4e8;">    top: 100px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    left: 30%;</span></span>
<span class="line"><span style="color:#e1e4e8;">    background-color: #fff;</span></span>
<span class="line"><span style="color:#e1e4e8;">    border-radius: 10px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    width: 600px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    height: 400px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    border: 1px solid #ddd;</span></span>
<span class="line"><span style="color:#e1e4e8;">    box-shadow: 5px 5px 5px #ddd;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">.div-login-title {</span></span>
<span class="line"><span style="color:#e1e4e8;">    font-size: 20px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    font-weight: bold;</span></span>
<span class="line"><span style="color:#e1e4e8;">    text-align: center;</span></span>
<span class="line"><span style="color:#e1e4e8;">    margin: 30px auto;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">.input {</span></span>
<span class="line"><span style="color:#e1e4e8;">    height: 50px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    width: 80%;</span></span>
<span class="line"><span style="color:#e1e4e8;">    margin: 20px auto;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">.el-button {</span></span>
<span class="line"><span style="color:#e1e4e8;">    margin-top: 30px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    width: 80%;</span></span>
<span class="line"><span style="color:#e1e4e8;">    height: 50px;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/style&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;script lang=&quot;ts&quot; setup &gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">import { ref } from &#39;vue&#39;;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">var account = ref&lt;String&gt;(&quot;1997&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">var password = ref&lt;String&gt;(&quot;123&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import { useRouter } from &#39;vue-router&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import request from &#39;../api/request-api&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">const router = useRouter();</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">function login() {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    let params = {</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;account&quot;: account.value,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;password&quot;: password.value</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    // 使用 asyncFunction  </span></span>
<span class="line"><span style="color:#e1e4e8;">    request.login(params).then(result =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">        console.log(result);  // 输出：操作成功完成  </span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        localStorage.setItem(&quot;tokenValue&quot;,result.others.tokenValue)</span></span>
<span class="line"><span style="color:#e1e4e8;">        localStorage.setItem(&quot;tokenName&quot;,result.others.tokenName)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        </span></span>
<span class="line"><span style="color:#e1e4e8;">        if (result.code == 0) {</span></span>
<span class="line"><span style="color:#e1e4e8;">            router.push({</span></span>
<span class="line"><span style="color:#e1e4e8;">                path: &#39;/Home&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">            })</span></span>
<span class="line"><span style="color:#e1e4e8;">        }</span></span>
<span class="line"><span style="color:#e1e4e8;">    });</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/script&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;div class=&quot;cont&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">        &lt;div class=&quot;div-login&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;div class=&quot;div-login-title&quot;&gt;后台管理系统&lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;div&gt;</span></span>
<span class="line"><span style="color:#24292e;">                &lt;el-input v-model=&quot;account&quot; class=&quot;w-50 m-2 input&quot; placeholder=&quot;请输入账号&quot; prefix-icon=&quot;User&quot; /&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;div&gt;</span></span>
<span class="line"><span style="color:#24292e;">                &lt;el-input v-model=&quot;password&quot; class=&quot;w-50 m-2 input&quot; placeholder=&quot;请输入密码&quot; prefix-icon=&quot;Unlock&quot; /&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">            &lt;div&gt;</span></span>
<span class="line"><span style="color:#24292e;">                &lt;el-button type=&quot;primary&quot; @click=&quot;login()&quot;&gt;登录&lt;/el-button&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">        &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;style scoped&gt;</span></span>
<span class="line"><span style="color:#24292e;">.cont {</span></span>
<span class="line"><span style="color:#24292e;">    height: 100vh;</span></span>
<span class="line"><span style="color:#24292e;">    width: 100vw;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">.div-login {</span></span>
<span class="line"><span style="color:#24292e;">    position: absolute;</span></span>
<span class="line"><span style="color:#24292e;">    top: 100px;</span></span>
<span class="line"><span style="color:#24292e;">    left: 30%;</span></span>
<span class="line"><span style="color:#24292e;">    background-color: #fff;</span></span>
<span class="line"><span style="color:#24292e;">    border-radius: 10px;</span></span>
<span class="line"><span style="color:#24292e;">    width: 600px;</span></span>
<span class="line"><span style="color:#24292e;">    height: 400px;</span></span>
<span class="line"><span style="color:#24292e;">    border: 1px solid #ddd;</span></span>
<span class="line"><span style="color:#24292e;">    box-shadow: 5px 5px 5px #ddd;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">.div-login-title {</span></span>
<span class="line"><span style="color:#24292e;">    font-size: 20px;</span></span>
<span class="line"><span style="color:#24292e;">    font-weight: bold;</span></span>
<span class="line"><span style="color:#24292e;">    text-align: center;</span></span>
<span class="line"><span style="color:#24292e;">    margin: 30px auto;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">.input {</span></span>
<span class="line"><span style="color:#24292e;">    height: 50px;</span></span>
<span class="line"><span style="color:#24292e;">    width: 80%;</span></span>
<span class="line"><span style="color:#24292e;">    margin: 20px auto;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">.el-button {</span></span>
<span class="line"><span style="color:#24292e;">    margin-top: 30px;</span></span>
<span class="line"><span style="color:#24292e;">    width: 80%;</span></span>
<span class="line"><span style="color:#24292e;">    height: 50px;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;">&lt;/style&gt;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;script lang=&quot;ts&quot; setup &gt;</span></span>
<span class="line"><span style="color:#24292e;">import { ref } from &#39;vue&#39;;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">var account = ref&lt;String&gt;(&quot;1997&quot;)</span></span>
<span class="line"><span style="color:#24292e;">var password = ref&lt;String&gt;(&quot;123&quot;)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import { useRouter } from &#39;vue-router&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import request from &#39;../api/request-api&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">const router = useRouter();</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">function login() {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    let params = {</span></span>
<span class="line"><span style="color:#24292e;">        &quot;account&quot;: account.value,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;password&quot;: password.value</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    // 使用 asyncFunction  </span></span>
<span class="line"><span style="color:#24292e;">    request.login(params).then(result =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">        console.log(result);  // 输出：操作成功完成  </span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        localStorage.setItem(&quot;tokenValue&quot;,result.others.tokenValue)</span></span>
<span class="line"><span style="color:#24292e;">        localStorage.setItem(&quot;tokenName&quot;,result.others.tokenName)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        </span></span>
<span class="line"><span style="color:#24292e;">        if (result.code == 0) {</span></span>
<span class="line"><span style="color:#24292e;">            router.push({</span></span>
<span class="line"><span style="color:#24292e;">                path: &#39;/Home&#39;</span></span>
<span class="line"><span style="color:#24292e;">            })</span></span>
<span class="line"><span style="color:#24292e;">        }</span></span>
<span class="line"><span style="color:#24292e;">    });</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;/script&gt;</span></span></code></pre></div><h2 id="_4-引入axios获取数据" tabindex="-1">4. 引入Axios获取数据 <a class="header-anchor" href="#_4-引入axios获取数据" aria-label="Permalink to &quot;4. 引入Axios获取数据&quot;">​</a></h2><p>封装一个全局请求:</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import axios, { AxiosInstance, AxiosRequestConfig } from &#39;axios&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">// import { Local } from &#39;@/utils/storage&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">import { ElMessage } from &#39;element-plus&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">// import { useUserStore } from &#39;@/store/user&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">class Api {</span></span>
<span class="line"><span style="color:#e1e4e8;">    instance: AxiosInstance</span></span>
<span class="line"><span style="color:#e1e4e8;">    config: AxiosRequestConfig</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    constructor(option: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        this.config = option</span></span>
<span class="line"><span style="color:#e1e4e8;">        // 配置全局参数</span></span>
<span class="line"><span style="color:#e1e4e8;">        this.instance = axios.create(this.config)</span></span>
<span class="line"><span style="color:#e1e4e8;">        this.interceptors()</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    interceptors() {</span></span>
<span class="line"><span style="color:#e1e4e8;">        this.instance.interceptors.request.use(</span></span>
<span class="line"><span style="color:#e1e4e8;">            (config) =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">                removePending(config)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                addPending(config)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                const tokenName = localStorage.getItem(&quot;tokenName&quot;) || &#39;&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                const tokenValue = localStorage.getItem(&quot;tokenValue&quot;) || &#39;&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                if (tokenValue) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                    config.headers[tokenName] = tokenValue;</span></span>
<span class="line"><span style="color:#e1e4e8;">                }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                // if (token) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                //     //@ts-ignore</span></span>
<span class="line"><span style="color:#e1e4e8;">                //     config.headers.satoken = &quot;0675e84d-4f44-41c1-9a1a-82cbc64f08b7&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">                //     config.headers.Authorization = &quot;0675e84d-4f44-41c1-9a1a-82cbc64f08b7&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">                // }</span></span>
<span class="line"><span style="color:#e1e4e8;">                return config</span></span>
<span class="line"><span style="color:#e1e4e8;">            },</span></span>
<span class="line"><span style="color:#e1e4e8;">            (error) =&gt; Promise.reject(error)</span></span>
<span class="line"><span style="color:#e1e4e8;">        )</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        this.instance.interceptors.response.use(</span></span>
<span class="line"><span style="color:#e1e4e8;">            (response) =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">                removePending(response.config)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                const res = response.data</span></span>
<span class="line"><span style="color:#e1e4e8;">                if (res.code !== 0) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                    ElMessage.error(res.msg)</span></span>
<span class="line"><span style="color:#e1e4e8;">                }</span></span>
<span class="line"><span style="color:#e1e4e8;">                return res</span></span>
<span class="line"><span style="color:#e1e4e8;">            },</span></span>
<span class="line"><span style="color:#e1e4e8;">            (error) =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">                error.config &amp;&amp; removePending(error.config)</span></span>
<span class="line"><span style="color:#e1e4e8;">                httpErrorStatusHandle(error)</span></span>
<span class="line"><span style="color:#e1e4e8;">                return Promise.reject(error)</span></span>
<span class="line"><span style="color:#e1e4e8;">            }</span></span>
<span class="line"><span style="color:#e1e4e8;">        )</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;">    async request&lt;T = any&gt;(config: AxiosRequestConfig): Promise&lt;TResponseData&lt;T&gt;&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return this.instance.request&lt;TResponseData&lt;T&gt;, TResponseData&lt;T&gt;&gt;(config)</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    // async request&lt;T = any&gt;(config: AxiosRequestConfig): Promise&lt;T,any&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">    //     return this.instance.request&lt;T&gt;(config)</span></span>
<span class="line"><span style="color:#e1e4e8;">    // }</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">const api = new Api({</span></span>
<span class="line"><span style="color:#e1e4e8;">    baseURL: &#39;http://localhost:8088/api&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    timeout: 10 * 1000,</span></span>
<span class="line"><span style="color:#e1e4e8;">})</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">export default api</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * 处理异常</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @param {*} error</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">function httpErrorStatusHandle(error: any) {</span></span>
<span class="line"><span style="color:#e1e4e8;">    // const userStore = useUserStore()</span></span>
<span class="line"><span style="color:#e1e4e8;">    // 处理被取消的请求</span></span>
<span class="line"><span style="color:#e1e4e8;">    if (axios.isCancel(error)) return console.error(&#39;请求的重复请求：&#39; + error.message)</span></span>
<span class="line"><span style="color:#e1e4e8;">    let message = &#39;&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">    if (error &amp;&amp; error.response) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        switch (error.response.status) {</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 302:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;接口重定向了！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 400:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;参数不正确！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 401:</span></span>
<span class="line"><span style="color:#e1e4e8;">                // userStore.clearLoginInfo()</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;您未登录，或者登录已经超时，请先登录！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 403:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;您没有权限操作！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 404:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = \`请求地址出错: \${error.response.config.url}\`</span></span>
<span class="line"><span style="color:#e1e4e8;">                break // 在正确域名下</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 408:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;请求超时！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 409:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;系统已存在相同数据！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 500:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;服务器内部错误！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 501:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;服务未实现！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 502:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;网关错误！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 503:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;服务不可用！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 504:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;服务暂时无法访问，请稍后再试！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            case 505:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;HTTP版本不受支持！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">            default:</span></span>
<span class="line"><span style="color:#e1e4e8;">                message = &#39;异常问题，请联系管理员！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">                break</span></span>
<span class="line"><span style="color:#e1e4e8;">        }</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;">    if (error.message.includes(&#39;timeout&#39;)) message = &#39;网络请求超时！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">    if (error.message.includes(&#39;Network&#39;))</span></span>
<span class="line"><span style="color:#e1e4e8;">        message = window.navigator.onLine ? &#39;服务端异常！&#39; : &#39;您断网了！&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    ElMessage({</span></span>
<span class="line"><span style="color:#e1e4e8;">        type: &#39;error&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        message,</span></span>
<span class="line"><span style="color:#e1e4e8;">    })</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">const pendingMap = new Map()</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * 储存每个请求的唯一cancel回调, 以此为标识</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @param {*} config</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">function addPending(config: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#e1e4e8;">    const pendingKey = getPendingKey(config)</span></span>
<span class="line"><span style="color:#e1e4e8;">    config.cancelToken =</span></span>
<span class="line"><span style="color:#e1e4e8;">        config.cancelToken ||</span></span>
<span class="line"><span style="color:#e1e4e8;">        new axios.CancelToken((cancel) =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">            if (!pendingMap.has(pendingKey)) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                pendingMap.set(pendingKey, cancel)</span></span>
<span class="line"><span style="color:#e1e4e8;">            }</span></span>
<span class="line"><span style="color:#e1e4e8;">        })</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * 删除重复的请求</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @param {*} config</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">function removePending(config: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#e1e4e8;">    const pendingKey = getPendingKey(config)</span></span>
<span class="line"><span style="color:#e1e4e8;">    if (pendingMap.has(pendingKey)) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        const cancelToken = pendingMap.get(pendingKey)</span></span>
<span class="line"><span style="color:#e1e4e8;">        // 如你不明白此处为什么需要传递pendingKey可以看文章下方的补丁解释</span></span>
<span class="line"><span style="color:#e1e4e8;">        cancelToken(pendingKey)</span></span>
<span class="line"><span style="color:#e1e4e8;">        pendingMap.delete(pendingKey)</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * 生成唯一的每个请求的唯一key</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @param {*} config</span></span>
<span class="line"><span style="color:#e1e4e8;"> * @returns</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">function getPendingKey(config: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#e1e4e8;">    let { url, method, params, data } = config</span></span>
<span class="line"><span style="color:#e1e4e8;">    if (typeof data === &#39;string&#39;) data = JSON.parse(data) // response里面返回的config.data是个字符串对象</span></span>
<span class="line"><span style="color:#e1e4e8;">    return [url, method, JSON.stringify(params), JSON.stringify(data)].join(&#39;&amp;&#39;)</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">// 后端统一返回数据模型</span></span>
<span class="line"><span style="color:#e1e4e8;">export type TResponseData&lt;T&gt; = {</span></span>
<span class="line"><span style="color:#e1e4e8;">    code: 0 | number // 0 =&gt; ok</span></span>
<span class="line"><span style="color:#e1e4e8;">    msg: string</span></span>
<span class="line"><span style="color:#e1e4e8;">    data: T,</span></span>
<span class="line"><span style="color:#e1e4e8;">    others: any</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">// 后端分页数据模型</span></span>
<span class="line"><span style="color:#e1e4e8;">export type PageData&lt;T = any&gt; = {</span></span>
<span class="line"><span style="color:#e1e4e8;">    total: 0 | number</span></span>
<span class="line"><span style="color:#e1e4e8;">    records: Array&lt;T&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import axios, { AxiosInstance, AxiosRequestConfig } from &#39;axios&#39;</span></span>
<span class="line"><span style="color:#24292e;">// import { Local } from &#39;@/utils/storage&#39;</span></span>
<span class="line"><span style="color:#24292e;">import { ElMessage } from &#39;element-plus&#39;</span></span>
<span class="line"><span style="color:#24292e;">// import { useUserStore } from &#39;@/store/user&#39;</span></span>
<span class="line"><span style="color:#24292e;">class Api {</span></span>
<span class="line"><span style="color:#24292e;">    instance: AxiosInstance</span></span>
<span class="line"><span style="color:#24292e;">    config: AxiosRequestConfig</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    constructor(option: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#24292e;">        this.config = option</span></span>
<span class="line"><span style="color:#24292e;">        // 配置全局参数</span></span>
<span class="line"><span style="color:#24292e;">        this.instance = axios.create(this.config)</span></span>
<span class="line"><span style="color:#24292e;">        this.interceptors()</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    interceptors() {</span></span>
<span class="line"><span style="color:#24292e;">        this.instance.interceptors.request.use(</span></span>
<span class="line"><span style="color:#24292e;">            (config) =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">                removePending(config)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                addPending(config)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                const tokenName = localStorage.getItem(&quot;tokenName&quot;) || &#39;&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                const tokenValue = localStorage.getItem(&quot;tokenValue&quot;) || &#39;&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                if (tokenValue) {</span></span>
<span class="line"><span style="color:#24292e;">                    config.headers[tokenName] = tokenValue;</span></span>
<span class="line"><span style="color:#24292e;">                }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                // if (token) {</span></span>
<span class="line"><span style="color:#24292e;">                //     //@ts-ignore</span></span>
<span class="line"><span style="color:#24292e;">                //     config.headers.satoken = &quot;0675e84d-4f44-41c1-9a1a-82cbc64f08b7&quot;</span></span>
<span class="line"><span style="color:#24292e;">                //     config.headers.Authorization = &quot;0675e84d-4f44-41c1-9a1a-82cbc64f08b7&quot;</span></span>
<span class="line"><span style="color:#24292e;">                // }</span></span>
<span class="line"><span style="color:#24292e;">                return config</span></span>
<span class="line"><span style="color:#24292e;">            },</span></span>
<span class="line"><span style="color:#24292e;">            (error) =&gt; Promise.reject(error)</span></span>
<span class="line"><span style="color:#24292e;">        )</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        this.instance.interceptors.response.use(</span></span>
<span class="line"><span style="color:#24292e;">            (response) =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">                removePending(response.config)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                const res = response.data</span></span>
<span class="line"><span style="color:#24292e;">                if (res.code !== 0) {</span></span>
<span class="line"><span style="color:#24292e;">                    ElMessage.error(res.msg)</span></span>
<span class="line"><span style="color:#24292e;">                }</span></span>
<span class="line"><span style="color:#24292e;">                return res</span></span>
<span class="line"><span style="color:#24292e;">            },</span></span>
<span class="line"><span style="color:#24292e;">            (error) =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">                error.config &amp;&amp; removePending(error.config)</span></span>
<span class="line"><span style="color:#24292e;">                httpErrorStatusHandle(error)</span></span>
<span class="line"><span style="color:#24292e;">                return Promise.reject(error)</span></span>
<span class="line"><span style="color:#24292e;">            }</span></span>
<span class="line"><span style="color:#24292e;">        )</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;">    async request&lt;T = any&gt;(config: AxiosRequestConfig): Promise&lt;TResponseData&lt;T&gt;&gt; {</span></span>
<span class="line"><span style="color:#24292e;">        return this.instance.request&lt;TResponseData&lt;T&gt;, TResponseData&lt;T&gt;&gt;(config)</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    // async request&lt;T = any&gt;(config: AxiosRequestConfig): Promise&lt;T,any&gt; {</span></span>
<span class="line"><span style="color:#24292e;">    //     return this.instance.request&lt;T&gt;(config)</span></span>
<span class="line"><span style="color:#24292e;">    // }</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">const api = new Api({</span></span>
<span class="line"><span style="color:#24292e;">    baseURL: &#39;http://localhost:8088/api&#39;,</span></span>
<span class="line"><span style="color:#24292e;">    timeout: 10 * 1000,</span></span>
<span class="line"><span style="color:#24292e;">})</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">export default api</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * 处理异常</span></span>
<span class="line"><span style="color:#24292e;"> * @param {*} error</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">function httpErrorStatusHandle(error: any) {</span></span>
<span class="line"><span style="color:#24292e;">    // const userStore = useUserStore()</span></span>
<span class="line"><span style="color:#24292e;">    // 处理被取消的请求</span></span>
<span class="line"><span style="color:#24292e;">    if (axios.isCancel(error)) return console.error(&#39;请求的重复请求：&#39; + error.message)</span></span>
<span class="line"><span style="color:#24292e;">    let message = &#39;&#39;</span></span>
<span class="line"><span style="color:#24292e;">    if (error &amp;&amp; error.response) {</span></span>
<span class="line"><span style="color:#24292e;">        switch (error.response.status) {</span></span>
<span class="line"><span style="color:#24292e;">            case 302:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;接口重定向了！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 400:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;参数不正确！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 401:</span></span>
<span class="line"><span style="color:#24292e;">                // userStore.clearLoginInfo()</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;您未登录，或者登录已经超时，请先登录！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 403:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;您没有权限操作！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 404:</span></span>
<span class="line"><span style="color:#24292e;">                message = \`请求地址出错: \${error.response.config.url}\`</span></span>
<span class="line"><span style="color:#24292e;">                break // 在正确域名下</span></span>
<span class="line"><span style="color:#24292e;">            case 408:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;请求超时！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 409:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;系统已存在相同数据！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 500:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;服务器内部错误！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 501:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;服务未实现！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 502:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;网关错误！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 503:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;服务不可用！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 504:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;服务暂时无法访问，请稍后再试！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            case 505:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;HTTP版本不受支持！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">            default:</span></span>
<span class="line"><span style="color:#24292e;">                message = &#39;异常问题，请联系管理员！&#39;</span></span>
<span class="line"><span style="color:#24292e;">                break</span></span>
<span class="line"><span style="color:#24292e;">        }</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;">    if (error.message.includes(&#39;timeout&#39;)) message = &#39;网络请求超时！&#39;</span></span>
<span class="line"><span style="color:#24292e;">    if (error.message.includes(&#39;Network&#39;))</span></span>
<span class="line"><span style="color:#24292e;">        message = window.navigator.onLine ? &#39;服务端异常！&#39; : &#39;您断网了！&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    ElMessage({</span></span>
<span class="line"><span style="color:#24292e;">        type: &#39;error&#39;,</span></span>
<span class="line"><span style="color:#24292e;">        message,</span></span>
<span class="line"><span style="color:#24292e;">    })</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">const pendingMap = new Map()</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * 储存每个请求的唯一cancel回调, 以此为标识</span></span>
<span class="line"><span style="color:#24292e;"> * @param {*} config</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">function addPending(config: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#24292e;">    const pendingKey = getPendingKey(config)</span></span>
<span class="line"><span style="color:#24292e;">    config.cancelToken =</span></span>
<span class="line"><span style="color:#24292e;">        config.cancelToken ||</span></span>
<span class="line"><span style="color:#24292e;">        new axios.CancelToken((cancel) =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">            if (!pendingMap.has(pendingKey)) {</span></span>
<span class="line"><span style="color:#24292e;">                pendingMap.set(pendingKey, cancel)</span></span>
<span class="line"><span style="color:#24292e;">            }</span></span>
<span class="line"><span style="color:#24292e;">        })</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * 删除重复的请求</span></span>
<span class="line"><span style="color:#24292e;"> * @param {*} config</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">function removePending(config: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#24292e;">    const pendingKey = getPendingKey(config)</span></span>
<span class="line"><span style="color:#24292e;">    if (pendingMap.has(pendingKey)) {</span></span>
<span class="line"><span style="color:#24292e;">        const cancelToken = pendingMap.get(pendingKey)</span></span>
<span class="line"><span style="color:#24292e;">        // 如你不明白此处为什么需要传递pendingKey可以看文章下方的补丁解释</span></span>
<span class="line"><span style="color:#24292e;">        cancelToken(pendingKey)</span></span>
<span class="line"><span style="color:#24292e;">        pendingMap.delete(pendingKey)</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * 生成唯一的每个请求的唯一key</span></span>
<span class="line"><span style="color:#24292e;"> * @param {*} config</span></span>
<span class="line"><span style="color:#24292e;"> * @returns</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">function getPendingKey(config: AxiosRequestConfig) {</span></span>
<span class="line"><span style="color:#24292e;">    let { url, method, params, data } = config</span></span>
<span class="line"><span style="color:#24292e;">    if (typeof data === &#39;string&#39;) data = JSON.parse(data) // response里面返回的config.data是个字符串对象</span></span>
<span class="line"><span style="color:#24292e;">    return [url, method, JSON.stringify(params), JSON.stringify(data)].join(&#39;&amp;&#39;)</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">// 后端统一返回数据模型</span></span>
<span class="line"><span style="color:#24292e;">export type TResponseData&lt;T&gt; = {</span></span>
<span class="line"><span style="color:#24292e;">    code: 0 | number // 0 =&gt; ok</span></span>
<span class="line"><span style="color:#24292e;">    msg: string</span></span>
<span class="line"><span style="color:#24292e;">    data: T,</span></span>
<span class="line"><span style="color:#24292e;">    others: any</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">// 后端分页数据模型</span></span>
<span class="line"><span style="color:#24292e;">export type PageData&lt;T = any&gt; = {</span></span>
<span class="line"><span style="color:#24292e;">    total: 0 | number</span></span>
<span class="line"><span style="color:#24292e;">    records: Array&lt;T&gt;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="_5-使用示例" tabindex="-1">5. 使用示例 <a class="header-anchor" href="#_5-使用示例" aria-label="Permalink to &quot;5. 使用示例&quot;">​</a></h2><p>在新建一个统一管理api</p><p>然后在每个页面引入后直接使用就行：</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;script lang=&quot;ts&quot; setup&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import { reactive, onMounted } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">// var dataList: Array&lt;String&gt; = [];</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">var dataList = reactive([]) as any[];//现在setup中定义</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import request from &#39;../api/request-api&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">// 创建完成(可以读取dom)</span></span>
<span class="line"><span style="color:#e1e4e8;">onMounted(() =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">    console.log(&quot;创建完成&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    queryData()</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">const queryData = async () =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    let params = {</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;roleId&quot;: 1,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;account&quot;: &quot;1997&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    request.listManagerMenu(params).then(result =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">        console.log(&quot;🚀 ~ file: Menu.vue:76 ~ request.listManagerMenu ~ result:&quot;, result)</span></span>
<span class="line"><span style="color:#e1e4e8;">        result.data.map((item: any) =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">            console.log(item);</span></span>
<span class="line"><span style="color:#e1e4e8;">            dataList.push(item)</span></span>
<span class="line"><span style="color:#e1e4e8;">        })</span></span>
<span class="line"><span style="color:#e1e4e8;">    })</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/script&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;script lang=&quot;ts&quot; setup&gt;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import { reactive, onMounted } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">// var dataList: Array&lt;String&gt; = [];</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">var dataList = reactive([]) as any[];//现在setup中定义</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import request from &#39;../api/request-api&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">// 创建完成(可以读取dom)</span></span>
<span class="line"><span style="color:#24292e;">onMounted(() =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">    console.log(&quot;创建完成&quot;);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    queryData()</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">const queryData = async () =&gt; {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    let params = {</span></span>
<span class="line"><span style="color:#24292e;">        &quot;roleId&quot;: 1,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;account&quot;: &quot;1997&quot;</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    request.listManagerMenu(params).then(result =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">        console.log(&quot;🚀 ~ file: Menu.vue:76 ~ request.listManagerMenu ~ result:&quot;, result)</span></span>
<span class="line"><span style="color:#24292e;">        result.data.map((item: any) =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">            console.log(item);</span></span>
<span class="line"><span style="color:#24292e;">            dataList.push(item)</span></span>
<span class="line"><span style="color:#24292e;">        })</span></span>
<span class="line"><span style="color:#24292e;">    })</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;/script&gt;</span></span></code></pre></div><div class="language-request-api.ts vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">request-api.ts</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import http from &#39;../common/HttpApi&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">// import { PageData } from &#39;@/types/global/request&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">// import { UserInfo } from &#39;@/types/user&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">class RequestApi {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * </span></span>
<span class="line"><span style="color:#e1e4e8;">     * @param params 登录管理系统</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @returns </span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    login(params: object) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return http.request({</span></span>
<span class="line"><span style="color:#e1e4e8;">            url: &#39;/manager/login-manager&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">            data: params</span></span>
<span class="line"><span style="color:#e1e4e8;">        })</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 查询账号的菜单权限</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @param params </span></span>
<span class="line"><span style="color:#e1e4e8;">     * @returns </span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    listManagerMenu(params: object) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return http.request({</span></span>
<span class="line"><span style="color:#e1e4e8;">            url: &#39;/menu/list-manager-menu&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">            data: params</span></span>
<span class="line"><span style="color:#e1e4e8;">        })</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 获取分页表格数据</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @param params 查询参数</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @returns</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    // getPageData(params: object) {</span></span>
<span class="line"><span style="color:#e1e4e8;">    //     return http.request&lt;PageData&lt;UserInfo&gt;&gt;({</span></span>
<span class="line"><span style="color:#e1e4e8;">    //         url: &#39;/sys/userInfo/page&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    //         method: &#39;GET&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">    //         params,</span></span>
<span class="line"><span style="color:#e1e4e8;">    //     })</span></span>
<span class="line"><span style="color:#e1e4e8;">    // }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    listManager(params: object) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return http.request({</span></span>
<span class="line"><span style="color:#e1e4e8;">            url: &#39;/manager/list&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        })</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 根据id删除</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @param id id</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @returns</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    removeById(id: string) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return http.request({</span></span>
<span class="line"><span style="color:#e1e4e8;">            url: &#39;/sys/userInfo/removeById/&#39; + id,</span></span>
<span class="line"><span style="color:#e1e4e8;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        })</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 保存接口</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @param params 保存参数</span></span>
<span class="line"><span style="color:#e1e4e8;">     * @returns</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    saveOrUpdate(params: object) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return http.request({</span></span>
<span class="line"><span style="color:#e1e4e8;">            url: &#39;/sys/userInfo/saveOrUpdate&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#e1e4e8;">            data: params,</span></span>
<span class="line"><span style="color:#e1e4e8;">        })</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">const requestApi = new RequestApi()</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">export default requestApi</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import http from &#39;../common/HttpApi&#39;</span></span>
<span class="line"><span style="color:#24292e;">// import { PageData } from &#39;@/types/global/request&#39;</span></span>
<span class="line"><span style="color:#24292e;">// import { UserInfo } from &#39;@/types/user&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">class RequestApi {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * </span></span>
<span class="line"><span style="color:#24292e;">     * @param params 登录管理系统</span></span>
<span class="line"><span style="color:#24292e;">     * @returns </span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    login(params: object) {</span></span>
<span class="line"><span style="color:#24292e;">        return http.request({</span></span>
<span class="line"><span style="color:#24292e;">            url: &#39;/manager/login-manager&#39;,</span></span>
<span class="line"><span style="color:#24292e;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#24292e;">            data: params</span></span>
<span class="line"><span style="color:#24292e;">        })</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 查询账号的菜单权限</span></span>
<span class="line"><span style="color:#24292e;">     * @param params </span></span>
<span class="line"><span style="color:#24292e;">     * @returns </span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    listManagerMenu(params: object) {</span></span>
<span class="line"><span style="color:#24292e;">        return http.request({</span></span>
<span class="line"><span style="color:#24292e;">            url: &#39;/menu/list-manager-menu&#39;,</span></span>
<span class="line"><span style="color:#24292e;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#24292e;">            data: params</span></span>
<span class="line"><span style="color:#24292e;">        })</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 获取分页表格数据</span></span>
<span class="line"><span style="color:#24292e;">     * @param params 查询参数</span></span>
<span class="line"><span style="color:#24292e;">     * @returns</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    // getPageData(params: object) {</span></span>
<span class="line"><span style="color:#24292e;">    //     return http.request&lt;PageData&lt;UserInfo&gt;&gt;({</span></span>
<span class="line"><span style="color:#24292e;">    //         url: &#39;/sys/userInfo/page&#39;,</span></span>
<span class="line"><span style="color:#24292e;">    //         method: &#39;GET&#39;,</span></span>
<span class="line"><span style="color:#24292e;">    //         params,</span></span>
<span class="line"><span style="color:#24292e;">    //     })</span></span>
<span class="line"><span style="color:#24292e;">    // }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    listManager(params: object) {</span></span>
<span class="line"><span style="color:#24292e;">        return http.request({</span></span>
<span class="line"><span style="color:#24292e;">            url: &#39;/manager/list&#39;,</span></span>
<span class="line"><span style="color:#24292e;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#24292e;">        })</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 根据id删除</span></span>
<span class="line"><span style="color:#24292e;">     * @param id id</span></span>
<span class="line"><span style="color:#24292e;">     * @returns</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    removeById(id: string) {</span></span>
<span class="line"><span style="color:#24292e;">        return http.request({</span></span>
<span class="line"><span style="color:#24292e;">            url: &#39;/sys/userInfo/removeById/&#39; + id,</span></span>
<span class="line"><span style="color:#24292e;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#24292e;">        })</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 保存接口</span></span>
<span class="line"><span style="color:#24292e;">     * @param params 保存参数</span></span>
<span class="line"><span style="color:#24292e;">     * @returns</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    saveOrUpdate(params: object) {</span></span>
<span class="line"><span style="color:#24292e;">        return http.request({</span></span>
<span class="line"><span style="color:#24292e;">            url: &#39;/sys/userInfo/saveOrUpdate&#39;,</span></span>
<span class="line"><span style="color:#24292e;">            method: &#39;POST&#39;,</span></span>
<span class="line"><span style="color:#24292e;">            data: params,</span></span>
<span class="line"><span style="color:#24292e;">        })</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">const requestApi = new RequestApi()</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">export default requestApi</span></span></code></pre></div><h2 id="_7-vue3生命周期" tabindex="-1">7. vue3生命周期 <a class="header-anchor" href="#_7-vue3生命周期" aria-label="Permalink to &quot;7. vue3生命周期&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import { reactive, onBeforeMount ,onMounted } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">// 创建之前(无法读取dom)</span></span>
<span class="line"><span style="color:#e1e4e8;">onBeforeMount(() =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(&quot;创建之前&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;">// 创建完成(可以读取dom)</span></span>
<span class="line"><span style="color:#e1e4e8;">onMounted(() =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(&quot;创建完成&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;">// 更新之前(读取更新之前的dom)</span></span>
<span class="line"><span style="color:#e1e4e8;">onBeforeUpdate(() =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(&quot;更新之前&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;">// 更新完成(读取更新之后的dom)</span></span>
<span class="line"><span style="color:#e1e4e8;">onUpdated(() =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(&quot;更新完成&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;">// 卸载之前</span></span>
<span class="line"><span style="color:#e1e4e8;">onBeforeUnmount(() =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(&quot;卸载之前&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;">// 卸载完成</span></span>
<span class="line"><span style="color:#e1e4e8;">onUnmounted(() =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(&quot;卸载完成&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;">// 收集依赖</span></span>
<span class="line"><span style="color:#e1e4e8;">onRenderTracked((e) =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(e);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span>
<span class="line"><span style="color:#e1e4e8;"> </span></span>
<span class="line"><span style="color:#e1e4e8;">// 更新依赖</span></span>
<span class="line"><span style="color:#e1e4e8;">onRenderTriggered((e) =&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">  console.log(e);</span></span>
<span class="line"><span style="color:#e1e4e8;">});</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import { reactive, onBeforeMount ,onMounted } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">// 创建之前(无法读取dom)</span></span>
<span class="line"><span style="color:#24292e;">onBeforeMount(() =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(&quot;创建之前&quot;);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;">// 创建完成(可以读取dom)</span></span>
<span class="line"><span style="color:#24292e;">onMounted(() =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(&quot;创建完成&quot;);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;">// 更新之前(读取更新之前的dom)</span></span>
<span class="line"><span style="color:#24292e;">onBeforeUpdate(() =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(&quot;更新之前&quot;);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;">// 更新完成(读取更新之后的dom)</span></span>
<span class="line"><span style="color:#24292e;">onUpdated(() =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(&quot;更新完成&quot;);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;">// 卸载之前</span></span>
<span class="line"><span style="color:#24292e;">onBeforeUnmount(() =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(&quot;卸载之前&quot;);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;">// 卸载完成</span></span>
<span class="line"><span style="color:#24292e;">onUnmounted(() =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(&quot;卸载完成&quot;);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;">// 收集依赖</span></span>
<span class="line"><span style="color:#24292e;">onRenderTracked((e) =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(e);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span>
<span class="line"><span style="color:#24292e;"> </span></span>
<span class="line"><span style="color:#24292e;">// 更新依赖</span></span>
<span class="line"><span style="color:#24292e;">onRenderTriggered((e) =&gt; {</span></span>
<span class="line"><span style="color:#24292e;">  console.log(e);</span></span>
<span class="line"><span style="color:#24292e;">});</span></span></code></pre></div><h2 id="_8-菜单显示" tabindex="-1">8. 菜单显示 <a class="header-anchor" href="#_8-菜单显示" aria-label="Permalink to &quot;8. 菜单显示&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">&lt;el-row class=&quot;tac&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">	&lt;el-col :span=&quot;24&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">		&lt;!-- &lt;h5 class=&quot;mb-2&quot;&gt;Default colors&lt;/h5&gt; --&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">		&lt;el-menu default-active=&quot;0&quot; class=&quot;el-menu-vertical-demo&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">			&lt;div v-for=&quot;(menu, index)  in dataList&quot; :key=&quot;index&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">				&lt;!--只有一级菜单--&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">				&lt;el-menu-item v-if=&quot;!menu.children&quot; :index=&quot;index + &#39;&#39;&quot; @click=&quot;clickItem(menu.menu_url)&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">					&lt;el-icon&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">						&lt;component :is=&quot;menu.icon&quot;&gt;&lt;/component&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">					&lt;/el-icon&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">					{{ menu.menu_name }}</span></span>
<span class="line"><span style="color:#e1e4e8;">				&lt;/el-menu-item&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">				&lt;!-- 多级菜单 --&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">				&lt;el-sub-menu v-else :index=&quot;index + &#39;&#39;&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">					&lt;template #title&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">						&lt;el-icon&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">							&lt;component :is=&quot;menu.icon&quot;&gt;&lt;/component&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">						&lt;/el-icon&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">						&lt;span&gt;{{ menu.menu_name }}&lt;/span&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">					&lt;/template&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">					&lt;el-menu-item v-for=&quot;(subMenu, subIndex) in menu.children&quot; :index=&quot;subMenu.menu_id + &#39;&#39;&quot; :key=&quot;subIndex&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">						@click=&quot;clickItem(subMenu.menu_url)&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">						&lt;el-icon&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">							&lt;component :is=&quot;subMenu.icon&quot;&gt;&lt;/component&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">						&lt;/el-icon&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">						{{ subMenu.menu_name }}</span></span>
<span class="line"><span style="color:#e1e4e8;">					&lt;/el-menu-item&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">				&lt;/el-sub-menu&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">			&lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">		&lt;/el-menu&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">	&lt;/el-col&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/el-row&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">&lt;el-row class=&quot;tac&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">	&lt;el-col :span=&quot;24&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">		&lt;!-- &lt;h5 class=&quot;mb-2&quot;&gt;Default colors&lt;/h5&gt; --&gt;</span></span>
<span class="line"><span style="color:#24292e;">		&lt;el-menu default-active=&quot;0&quot; class=&quot;el-menu-vertical-demo&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">			&lt;div v-for=&quot;(menu, index)  in dataList&quot; :key=&quot;index&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">				&lt;!--只有一级菜单--&gt;</span></span>
<span class="line"><span style="color:#24292e;">				&lt;el-menu-item v-if=&quot;!menu.children&quot; :index=&quot;index + &#39;&#39;&quot; @click=&quot;clickItem(menu.menu_url)&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">					&lt;el-icon&gt;</span></span>
<span class="line"><span style="color:#24292e;">						&lt;component :is=&quot;menu.icon&quot;&gt;&lt;/component&gt;</span></span>
<span class="line"><span style="color:#24292e;">					&lt;/el-icon&gt;</span></span>
<span class="line"><span style="color:#24292e;">					{{ menu.menu_name }}</span></span>
<span class="line"><span style="color:#24292e;">				&lt;/el-menu-item&gt;</span></span>
<span class="line"><span style="color:#24292e;">				&lt;!-- 多级菜单 --&gt;</span></span>
<span class="line"><span style="color:#24292e;">				&lt;el-sub-menu v-else :index=&quot;index + &#39;&#39;&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">					&lt;template #title&gt;</span></span>
<span class="line"><span style="color:#24292e;">						&lt;el-icon&gt;</span></span>
<span class="line"><span style="color:#24292e;">							&lt;component :is=&quot;menu.icon&quot;&gt;&lt;/component&gt;</span></span>
<span class="line"><span style="color:#24292e;">						&lt;/el-icon&gt;</span></span>
<span class="line"><span style="color:#24292e;">						&lt;span&gt;{{ menu.menu_name }}&lt;/span&gt;</span></span>
<span class="line"><span style="color:#24292e;">					&lt;/template&gt;</span></span>
<span class="line"><span style="color:#24292e;">					&lt;el-menu-item v-for=&quot;(subMenu, subIndex) in menu.children&quot; :index=&quot;subMenu.menu_id + &#39;&#39;&quot; :key=&quot;subIndex&quot;</span></span>
<span class="line"><span style="color:#24292e;">						@click=&quot;clickItem(subMenu.menu_url)&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">						&lt;el-icon&gt;</span></span>
<span class="line"><span style="color:#24292e;">							&lt;component :is=&quot;subMenu.icon&quot;&gt;&lt;/component&gt;</span></span>
<span class="line"><span style="color:#24292e;">						&lt;/el-icon&gt;</span></span>
<span class="line"><span style="color:#24292e;">						{{ subMenu.menu_name }}</span></span>
<span class="line"><span style="color:#24292e;">					&lt;/el-menu-item&gt;</span></span>
<span class="line"><span style="color:#24292e;">				&lt;/el-sub-menu&gt;</span></span>
<span class="line"><span style="color:#24292e;">			&lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">		&lt;/el-menu&gt;</span></span>
<span class="line"><span style="color:#24292e;">	&lt;/el-col&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/el-row&gt;</span></span></code></pre></div><p>后台处理好数据格式：</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">{</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;code&quot;: 0,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;msg&quot;: &quot;成功&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;data&quot;: [</span></span>
<span class="line"><span style="color:#e1e4e8;">		{</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;id&quot;: 336,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_id&quot;: &quot;43&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;parent_id&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_url&quot;: &quot;home&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_name&quot;: &quot;首页&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;icon&quot;: &quot;el-icon-s-home&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;order&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">		},</span></span>
<span class="line"><span style="color:#e1e4e8;">		{</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;id&quot;: 337,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;parent_id&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_url&quot;: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_name&quot;: &quot;系统管理&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;icon&quot;: &quot;el-icon-s-tools&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;children&quot;: [</span></span>
<span class="line"><span style="color:#e1e4e8;">				{</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;id&quot;: 338,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_id&quot;: &quot;3&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;parent_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_url&quot;: &quot;managerList&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_name&quot;: &quot;账户管理&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;icon&quot;: &quot;el-icon-user-solid&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;order&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">				},</span></span>
<span class="line"><span style="color:#e1e4e8;">				{</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;id&quot;: 339,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_id&quot;: &quot;2&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;parent_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_url&quot;: &quot;rolesList&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_name&quot;: &quot;角色管理&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;icon&quot;: &quot;el-icon-s-custom&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;order&quot;: &quot;2&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">				},</span></span>
<span class="line"><span style="color:#e1e4e8;">				{</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;id&quot;: 340,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_id&quot;: &quot;4&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;parent_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_url&quot;: &quot;menuList&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_name&quot;: &quot;菜单管理&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;icon&quot;: &quot;el-icon-menu&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;order&quot;: &quot;3&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">				}</span></span>
<span class="line"><span style="color:#e1e4e8;">			],</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;order&quot;: &quot;2&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">		},</span></span>
<span class="line"><span style="color:#e1e4e8;">		{</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;id&quot;: 341,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_id&quot;: &quot;44&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;parent_id&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_url&quot;: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;menu_name&quot;: &quot;日志&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;icon&quot;: &quot;el-icon-notebook-1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;children&quot;: [</span></span>
<span class="line"><span style="color:#e1e4e8;">				{</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;id&quot;: 342,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_id&quot;: &quot;47&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;parent_id&quot;: &quot;44&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_url&quot;: &quot;loginLogList&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;menu_name&quot;: &quot;登录日志&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;icon&quot;: &quot;el-icon-tickets&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;order&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">				}</span></span>
<span class="line"><span style="color:#e1e4e8;">			],</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;order&quot;: &quot;3&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">		}</span></span>
<span class="line"><span style="color:#e1e4e8;">	],</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;others&quot;: null</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">{</span></span>
<span class="line"><span style="color:#24292e;">	&quot;code&quot;: 0,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;msg&quot;: &quot;成功&quot;,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;data&quot;: [</span></span>
<span class="line"><span style="color:#24292e;">		{</span></span>
<span class="line"><span style="color:#24292e;">			&quot;id&quot;: 336,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_id&quot;: &quot;43&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;parent_id&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_url&quot;: &quot;home&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_name&quot;: &quot;首页&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;icon&quot;: &quot;el-icon-s-home&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;order&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#24292e;">		},</span></span>
<span class="line"><span style="color:#24292e;">		{</span></span>
<span class="line"><span style="color:#24292e;">			&quot;id&quot;: 337,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;parent_id&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_url&quot;: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_name&quot;: &quot;系统管理&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;icon&quot;: &quot;el-icon-s-tools&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;children&quot;: [</span></span>
<span class="line"><span style="color:#24292e;">				{</span></span>
<span class="line"><span style="color:#24292e;">					&quot;id&quot;: 338,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_id&quot;: &quot;3&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;parent_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_url&quot;: &quot;managerList&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_name&quot;: &quot;账户管理&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;icon&quot;: &quot;el-icon-user-solid&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;order&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#24292e;">				},</span></span>
<span class="line"><span style="color:#24292e;">				{</span></span>
<span class="line"><span style="color:#24292e;">					&quot;id&quot;: 339,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_id&quot;: &quot;2&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;parent_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_url&quot;: &quot;rolesList&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_name&quot;: &quot;角色管理&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;icon&quot;: &quot;el-icon-s-custom&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;order&quot;: &quot;2&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#24292e;">				},</span></span>
<span class="line"><span style="color:#24292e;">				{</span></span>
<span class="line"><span style="color:#24292e;">					&quot;id&quot;: 340,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_id&quot;: &quot;4&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;parent_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_url&quot;: &quot;menuList&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_name&quot;: &quot;菜单管理&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;icon&quot;: &quot;el-icon-menu&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;order&quot;: &quot;3&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#24292e;">				}</span></span>
<span class="line"><span style="color:#24292e;">			],</span></span>
<span class="line"><span style="color:#24292e;">			&quot;order&quot;: &quot;2&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#24292e;">		},</span></span>
<span class="line"><span style="color:#24292e;">		{</span></span>
<span class="line"><span style="color:#24292e;">			&quot;id&quot;: 341,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_id&quot;: &quot;44&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;parent_id&quot;: &quot;0&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_url&quot;: &quot;/&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;menu_name&quot;: &quot;日志&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;icon&quot;: &quot;el-icon-notebook-1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;children&quot;: [</span></span>
<span class="line"><span style="color:#24292e;">				{</span></span>
<span class="line"><span style="color:#24292e;">					&quot;id&quot;: 342,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;role_id&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_id&quot;: &quot;47&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;parent_id&quot;: &quot;44&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_url&quot;: &quot;loginLogList&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;menu_name&quot;: &quot;登录日志&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;icon&quot;: &quot;el-icon-tickets&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;children&quot;: null,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;order&quot;: &quot;1&quot;,</span></span>
<span class="line"><span style="color:#24292e;">					&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#24292e;">				}</span></span>
<span class="line"><span style="color:#24292e;">			],</span></span>
<span class="line"><span style="color:#24292e;">			&quot;order&quot;: &quot;3&quot;,</span></span>
<span class="line"><span style="color:#24292e;">			&quot;delete_flag&quot;: &quot;0&quot;</span></span>
<span class="line"><span style="color:#24292e;">		}</span></span>
<span class="line"><span style="color:#24292e;">	],</span></span>
<span class="line"><span style="color:#24292e;">	&quot;others&quot;: null</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div>`,41),o=[t];function c(r,i,u,y,q,g){return n(),a("div",null,o)}const h=s(p,[["render",c]]);export{m as __pageData,h as default};
