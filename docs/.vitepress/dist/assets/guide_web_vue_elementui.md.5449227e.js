import{_ as s,o as n,c as a,S as l}from"./chunks/framework.d7c45c4d.js";const d=JSON.parse('{"title":"hbuilderx新建项目，选择vue2","description":"","frontmatter":{},"headers":[],"relativePath":"guide/web/vue/elementui.md","filePath":"guide/web/vue/elementui.md","lastUpdated":1703061529000}'),p={name:"guide/web/vue/elementui.md"},o=l(`<h1 id="hbuilderx新建项目-选择vue2" tabindex="-1">hbuilderx新建项目，选择vue2 <a class="header-anchor" href="#hbuilderx新建项目-选择vue2" aria-label="Permalink to &quot;hbuilderx新建项目，选择vue2&quot;">​</a></h1><h2 id="_1-使用router" tabindex="-1">1. 使用router <a class="header-anchor" href="#_1-使用router" aria-label="Permalink to &quot;1. 使用router&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">npm i vue-router@3.5.2 -S</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">npm i vue-router@3.5.2 -S</span></span></code></pre></div><p>src下创建router目录，在里面新建index.js</p><div class="language-JavaScript vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">JavaScript</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">/* 1.导入Vue和VueRouter的包 */</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#F97583;">import</span><span style="color:#E1E4E8;"> Vue </span><span style="color:#F97583;">from</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;vue&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#F97583;">import</span><span style="color:#E1E4E8;"> VueRouter </span><span style="color:#F97583;">from</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;vue-router&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">/* 导入需要的组件 */</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#F97583;">import</span><span style="color:#E1E4E8;"> Main </span><span style="color:#F97583;">from</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;@/pages/Main.vue&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#F97583;">import</span><span style="color:#E1E4E8;"> Login </span><span style="color:#F97583;">from</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;@/pages/Login.vue&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span></span>
<span class="line"><span style="color:#E1E4E8;">   </span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">/* 2.调用Vue.use()函数，把VueRouter安装为Vue的插件 */</span></span>
<span class="line"><span style="color:#E1E4E8;">   Vue.</span><span style="color:#B392F0;">use</span><span style="color:#E1E4E8;">(VueRouter)</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">/* 3.创建路由实例对象 */</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#F97583;">const</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">router</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">new</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">VueRouter</span><span style="color:#E1E4E8;">({</span></span>
<span class="line"><span style="color:#E1E4E8;">   	routes: [</span></span>
<span class="line"><span style="color:#E1E4E8;">   		</span><span style="color:#6A737D;">// 路由规则</span></span>
<span class="line"><span style="color:#E1E4E8;">   		{</span></span>
<span class="line"><span style="color:#E1E4E8;">   			path: </span><span style="color:#9ECBFF;">&#39;/login&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">   			component: Login</span></span>
<span class="line"><span style="color:#E1E4E8;">   		},</span></span>
<span class="line"><span style="color:#E1E4E8;">   		{</span></span>
<span class="line"><span style="color:#E1E4E8;">   			path: </span><span style="color:#9ECBFF;">&#39;/main&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">   			component: Main</span></span>
<span class="line"><span style="color:#E1E4E8;">   		}</span></span>
<span class="line"><span style="color:#E1E4E8;">   	]</span></span>
<span class="line"><span style="color:#E1E4E8;">   })</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">/* 为router实例对象，声明全局前置导航守卫 */</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">/* 只要发生了路由的跳转，都会触发回调函数 */</span></span>
<span class="line"><span style="color:#E1E4E8;">   router.</span><span style="color:#B392F0;">beforeEach</span><span style="color:#E1E4E8;">(</span><span style="color:#F97583;">function</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">to</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">from</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">next</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">   	</span><span style="color:#6A737D;">// to表示将要访问的路由的信息对象</span></span>
<span class="line"><span style="color:#E1E4E8;">   	</span><span style="color:#6A737D;">// from表示将要离开的路由信息对象</span></span>
<span class="line"><span style="color:#E1E4E8;">   	</span><span style="color:#6A737D;">// next()函数表示放行的意思</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">/* 分析：</span></span>
<span class="line"><span style="color:#6A737D;">   1.拿到用户将要访问的hash地址</span></span>
<span class="line"><span style="color:#6A737D;">   2.判断hash地址是否等于/main，</span></span>
<span class="line"><span style="color:#6A737D;">   2.1如果等于/main，证明需要登录之后才能访问成功</span></span>
<span class="line"><span style="color:#6A737D;">   2.2如果不等于/main，则不需要登录，直接放行next（）</span></span>
<span class="line"><span style="color:#6A737D;">   3.如果访问的地址是/main，则需要读取localStorage中的token值</span></span>
<span class="line"><span style="color:#6A737D;">   3.1如果有token，则放行</span></span>
<span class="line"><span style="color:#6A737D;">   3.2如果没有token则强制跳转到/login登录页 */</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (to.path </span><span style="color:#F97583;">===</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;/main&#39;</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">   	</span><span style="color:#6A737D;">/* 要访问后台主页，需要判断是否有token */</span></span>
<span class="line"><span style="color:#E1E4E8;">   	</span><span style="color:#F97583;">const</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">token</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> localStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;token&#39;</span><span style="color:#E1E4E8;">)</span></span>
<span class="line"><span style="color:#E1E4E8;">   	</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (token) {</span></span>
<span class="line"><span style="color:#E1E4E8;">   		</span><span style="color:#B392F0;">next</span><span style="color:#E1E4E8;">()</span></span>
<span class="line"><span style="color:#E1E4E8;">   	} </span><span style="color:#F97583;">else</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">   		</span><span style="color:#6A737D;">// 没有登录跳转login页面</span></span>
<span class="line"><span style="color:#E1E4E8;">   		</span><span style="color:#6A737D;">// next(&#39;/login&#39;)</span></span>
<span class="line"><span style="color:#E1E4E8;">   		</span><span style="color:#B392F0;">next</span><span style="color:#E1E4E8;">()</span></span>
<span class="line"><span style="color:#E1E4E8;">   	}</span></span>
<span class="line"><span style="color:#E1E4E8;">   } </span><span style="color:#F97583;">else</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">   	</span><span style="color:#B392F0;">next</span><span style="color:#E1E4E8;">()</span></span>
<span class="line"><span style="color:#E1E4E8;">   }</span></span>
<span class="line"><span style="color:#E1E4E8;">   })</span></span>
<span class="line"><span style="color:#E1E4E8;">   </span></span>
<span class="line"><span style="color:#E1E4E8;">      </span><span style="color:#6A737D;">/* 4.向外共享router */</span></span>
<span class="line"><span style="color:#E1E4E8;">      </span><span style="color:#F97583;">export</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">default</span><span style="color:#E1E4E8;"> router</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292E;">   </span><span style="color:#6A737D;">/* 1.导入Vue和VueRouter的包 */</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#D73A49;">import</span><span style="color:#24292E;"> Vue </span><span style="color:#D73A49;">from</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;vue&#39;</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#D73A49;">import</span><span style="color:#24292E;"> VueRouter </span><span style="color:#D73A49;">from</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;vue-router&#39;</span></span>
<span class="line"><span style="color:#24292E;">   </span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#6A737D;">/* 导入需要的组件 */</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#D73A49;">import</span><span style="color:#24292E;"> Main </span><span style="color:#D73A49;">from</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;@/pages/Main.vue&#39;</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#D73A49;">import</span><span style="color:#24292E;"> Login </span><span style="color:#D73A49;">from</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;@/pages/Login.vue&#39;</span></span>
<span class="line"><span style="color:#24292E;">   </span></span>
<span class="line"><span style="color:#24292E;">   </span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#6A737D;">/* 2.调用Vue.use()函数，把VueRouter安装为Vue的插件 */</span></span>
<span class="line"><span style="color:#24292E;">   Vue.</span><span style="color:#6F42C1;">use</span><span style="color:#24292E;">(VueRouter)</span></span>
<span class="line"><span style="color:#24292E;">   </span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#6A737D;">/* 3.创建路由实例对象 */</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#D73A49;">const</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">router</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">new</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">VueRouter</span><span style="color:#24292E;">({</span></span>
<span class="line"><span style="color:#24292E;">   	routes: [</span></span>
<span class="line"><span style="color:#24292E;">   		</span><span style="color:#6A737D;">// 路由规则</span></span>
<span class="line"><span style="color:#24292E;">   		{</span></span>
<span class="line"><span style="color:#24292E;">   			path: </span><span style="color:#032F62;">&#39;/login&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">   			component: Login</span></span>
<span class="line"><span style="color:#24292E;">   		},</span></span>
<span class="line"><span style="color:#24292E;">   		{</span></span>
<span class="line"><span style="color:#24292E;">   			path: </span><span style="color:#032F62;">&#39;/main&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">   			component: Main</span></span>
<span class="line"><span style="color:#24292E;">   		}</span></span>
<span class="line"><span style="color:#24292E;">   	]</span></span>
<span class="line"><span style="color:#24292E;">   })</span></span>
<span class="line"><span style="color:#24292E;">   </span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#6A737D;">/* 为router实例对象，声明全局前置导航守卫 */</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#6A737D;">/* 只要发生了路由的跳转，都会触发回调函数 */</span></span>
<span class="line"><span style="color:#24292E;">   router.</span><span style="color:#6F42C1;">beforeEach</span><span style="color:#24292E;">(</span><span style="color:#D73A49;">function</span><span style="color:#24292E;">(</span><span style="color:#E36209;">to</span><span style="color:#24292E;">, </span><span style="color:#E36209;">from</span><span style="color:#24292E;">, </span><span style="color:#E36209;">next</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">   	</span><span style="color:#6A737D;">// to表示将要访问的路由的信息对象</span></span>
<span class="line"><span style="color:#24292E;">   	</span><span style="color:#6A737D;">// from表示将要离开的路由信息对象</span></span>
<span class="line"><span style="color:#24292E;">   	</span><span style="color:#6A737D;">// next()函数表示放行的意思</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#6A737D;">/* 分析：</span></span>
<span class="line"><span style="color:#6A737D;">   1.拿到用户将要访问的hash地址</span></span>
<span class="line"><span style="color:#6A737D;">   2.判断hash地址是否等于/main，</span></span>
<span class="line"><span style="color:#6A737D;">   2.1如果等于/main，证明需要登录之后才能访问成功</span></span>
<span class="line"><span style="color:#6A737D;">   2.2如果不等于/main，则不需要登录，直接放行next（）</span></span>
<span class="line"><span style="color:#6A737D;">   3.如果访问的地址是/main，则需要读取localStorage中的token值</span></span>
<span class="line"><span style="color:#6A737D;">   3.1如果有token，则放行</span></span>
<span class="line"><span style="color:#6A737D;">   3.2如果没有token则强制跳转到/login登录页 */</span></span>
<span class="line"><span style="color:#24292E;">   </span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (to.path </span><span style="color:#D73A49;">===</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;/main&#39;</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">   	</span><span style="color:#6A737D;">/* 要访问后台主页，需要判断是否有token */</span></span>
<span class="line"><span style="color:#24292E;">   	</span><span style="color:#D73A49;">const</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">token</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> localStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;token&#39;</span><span style="color:#24292E;">)</span></span>
<span class="line"><span style="color:#24292E;">   	</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (token) {</span></span>
<span class="line"><span style="color:#24292E;">   		</span><span style="color:#6F42C1;">next</span><span style="color:#24292E;">()</span></span>
<span class="line"><span style="color:#24292E;">   	} </span><span style="color:#D73A49;">else</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">   		</span><span style="color:#6A737D;">// 没有登录跳转login页面</span></span>
<span class="line"><span style="color:#24292E;">   		</span><span style="color:#6A737D;">// next(&#39;/login&#39;)</span></span>
<span class="line"><span style="color:#24292E;">   		</span><span style="color:#6F42C1;">next</span><span style="color:#24292E;">()</span></span>
<span class="line"><span style="color:#24292E;">   	}</span></span>
<span class="line"><span style="color:#24292E;">   } </span><span style="color:#D73A49;">else</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">   	</span><span style="color:#6F42C1;">next</span><span style="color:#24292E;">()</span></span>
<span class="line"><span style="color:#24292E;">   }</span></span>
<span class="line"><span style="color:#24292E;">   })</span></span>
<span class="line"><span style="color:#24292E;">   </span></span>
<span class="line"><span style="color:#24292E;">      </span><span style="color:#6A737D;">/* 4.向外共享router */</span></span>
<span class="line"><span style="color:#24292E;">      </span><span style="color:#D73A49;">export</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">default</span><span style="color:#24292E;"> router</span></span></code></pre></div><p>src/main.js 入口文件中，导入并挂载路由模块.</p><div class="language-main.js vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">main.js</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import router from &#39;@/router&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">new Vue({</span></span>
<span class="line"><span style="color:#e1e4e8;">     render: h =&gt; h(App),</span></span>
<span class="line"><span style="color:#e1e4e8;">     router,</span></span>
<span class="line"><span style="color:#e1e4e8;">   }).$mount(&#39;#app&#39;)</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import router from &#39;@/router&#39;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">new Vue({</span></span>
<span class="line"><span style="color:#24292e;">     render: h =&gt; h(App),</span></span>
<span class="line"><span style="color:#24292e;">     router,</span></span>
<span class="line"><span style="color:#24292e;">   }).$mount(&#39;#app&#39;)</span></span></code></pre></div><h2 id="_2-使用elementui" tabindex="-1">2. 使用elementui <a class="header-anchor" href="#_2-使用elementui" aria-label="Permalink to &quot;2. 使用elementui&quot;">​</a></h2><p>引入elementui main.js中导入</p><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">npm i element-ui -S</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">npm i element-ui -S</span></span></code></pre></div><div class="language-main.js vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">main.js</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import ElementUI from &#39;element-ui&#39;; </span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import &#39;element-ui/lib/theme-chalk/index.css&#39;;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">Vue.use(ElementUI);</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import ElementUI from &#39;element-ui&#39;; </span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import &#39;element-ui/lib/theme-chalk/index.css&#39;;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">Vue.use(ElementUI);</span></span></code></pre></div><h2 id="_3-使用axios" tabindex="-1">3. 使用axios <a class="header-anchor" href="#_3-使用axios" aria-label="Permalink to &quot;3. 使用axios&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">npm install axios</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">npm install axios</span></span></code></pre></div><div class="language-main.js vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">main.js</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import axios from &#39;axios&#39; // 导入 axios</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import {</span></span>
<span class="line"><span style="color:#e1e4e8;">	getRequest,</span></span>
<span class="line"><span style="color:#e1e4e8;">	postRequest</span></span>
<span class="line"><span style="color:#e1e4e8;">} from &#39;./common/requestHttp.js&#39;</span></span>
<span class="line"><span style="color:#e1e4e8;">Vue.prototype.$getRequest = getRequest;</span></span>
<span class="line"><span style="color:#e1e4e8;">Vue.prototype.$postRequest = postRequest;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">Vue.use(axios)</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import axios from &#39;axios&#39; // 导入 axios</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import {</span></span>
<span class="line"><span style="color:#24292e;">	getRequest,</span></span>
<span class="line"><span style="color:#24292e;">	postRequest</span></span>
<span class="line"><span style="color:#24292e;">} from &#39;./common/requestHttp.js&#39;</span></span>
<span class="line"><span style="color:#24292e;">Vue.prototype.$getRequest = getRequest;</span></span>
<span class="line"><span style="color:#24292e;">Vue.prototype.$postRequest = postRequest;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">Vue.use(axios)</span></span></code></pre></div><h2 id="_4-封装requesthttp" tabindex="-1">4. 封装requestHttp <a class="header-anchor" href="#_4-封装requesthttp" aria-label="Permalink to &quot;4. 封装requestHttp&quot;">​</a></h2><p>新建请求文件 /src/common/requestHttp.js</p><div class="language-js vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">js</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#F97583;">import</span><span style="color:#E1E4E8;"> axios </span><span style="color:#F97583;">from</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;axios&#39;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#F97583;">import</span><span style="color:#E1E4E8;"> ElementUI </span><span style="color:#F97583;">from</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;element-ui&#39;</span><span style="color:#E1E4E8;">;</span></span>
<span class="line"><span style="color:#F97583;">import</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	Loading</span></span>
<span class="line"><span style="color:#E1E4E8;">} </span><span style="color:#F97583;">from</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;element-ui&#39;</span><span style="color:#E1E4E8;">;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#F97583;">let</span><span style="color:#E1E4E8;"> loading;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">axios.defaults.headers.common[</span><span style="color:#9ECBFF;">&#39;Authorization&#39;</span><span style="color:#E1E4E8;">] </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> sessionStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;sessionId&#39;</span><span style="color:#E1E4E8;">)</span></span>
<span class="line"><span style="color:#6A737D;">// import qs from &#39;qs&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">axios.defaults.withCredentials </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">true</span><span style="color:#E1E4E8;">; </span><span style="color:#6A737D;">//	允许跨域</span></span>
<span class="line"><span style="color:#E1E4E8;">axios.interceptors.request.</span><span style="color:#B392F0;">use</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">config</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=&gt;</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">let</span><span style="color:#E1E4E8;"> append </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> document.</span><span style="color:#B392F0;">getElementsByName</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;body&#39;</span><span style="color:#E1E4E8;">)</span></span>
<span class="line"><span style="color:#E1E4E8;">	append.innerHTML </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;&lt;img style=&quot;position:fixed;</span><span style="color:#79B8FF;">\\n</span><span style="color:#9ECBFF;">&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#9ECBFF;">&#39; left:47%;</span><span style="color:#79B8FF;">\\n</span><span style="color:#9ECBFF;">&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#9ECBFF;">&#39; top:40%;</span><span style="color:#79B8FF;">\\n</span><span style="color:#9ECBFF;">&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#9ECBFF;">&#39; transform: translateY(-50%),translateX(-50%);&quot;&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#9ECBFF;">&#39; src=&quot;../../static/img/a.jpg&quot;/&gt;&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> config</span></span>
<span class="line"><span style="color:#E1E4E8;">}, </span><span style="color:#FFAB70;">err</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=&gt;</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">Promise</span><span style="color:#E1E4E8;">.</span><span style="color:#B392F0;">resolve</span><span style="color:#E1E4E8;">(err)</span></span>
<span class="line"><span style="color:#E1E4E8;">})</span></span>
<span class="line"></span>
<span class="line"><span style="color:#F97583;">let</span><span style="color:#E1E4E8;"> base </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&quot;http://localhost:8099/api/&quot;</span><span style="color:#E1E4E8;"> </span><span style="color:#6A737D;">// 接口域名</span></span>
<span class="line"></span>
<span class="line"><span style="color:#F97583;">export</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">const</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">request</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> (</span><span style="color:#FFAB70;">url</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">params</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">method</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">Func</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">isJson</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">=&gt;</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#6A737D;">// console.log(&quot;请求的接口---&gt;&quot; + base + url);</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#6A737D;">// console.log(&quot;请求的方法---&gt;&quot; + method);</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#6A737D;">// console.log(&quot;请求格式---&gt;&quot; + isJson);</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#6A737D;">// console.log(&quot;请求的参数---&gt;&quot; + JSON.stringify(params));</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#B392F0;">startLoding</span><span style="color:#E1E4E8;">()</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">var</span><span style="color:#E1E4E8;"> _this </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">this</span><span style="color:#E1E4E8;">;</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#B392F0;">axios</span><span style="color:#E1E4E8;">({</span></span>
<span class="line"><span style="color:#E1E4E8;">		method: method,</span></span>
<span class="line"><span style="color:#E1E4E8;">		url: </span><span style="color:#9ECBFF;">\`\${</span><span style="color:#E1E4E8;">base</span><span style="color:#9ECBFF;">}\${</span><span style="color:#E1E4E8;">url</span><span style="color:#9ECBFF;">}\`</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		data: method </span><span style="color:#F97583;">===</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;post&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">?</span><span style="color:#E1E4E8;"> params </span><span style="color:#F97583;">:</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		transformRequest: [</span><span style="color:#F97583;">function</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">data</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (isJson </span><span style="color:#F97583;">===</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">1</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">				</span><span style="color:#6A737D;">// console.log(&quot;判断是否json格式或者是表单提交形式&quot;);</span></span>
<span class="line"><span style="color:#E1E4E8;">				</span><span style="color:#6A737D;">// debugger       // 判断是否json格式或者是表单提交形式</span></span>
<span class="line"><span style="color:#E1E4E8;">				</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">JSON</span><span style="color:#E1E4E8;">.</span><span style="color:#B392F0;">stringify</span><span style="color:#E1E4E8;">(data)</span></span>
<span class="line"><span style="color:#E1E4E8;">			}</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#F97583;">let</span><span style="color:#E1E4E8;"> ret </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#F97583;">for</span><span style="color:#E1E4E8;"> (</span><span style="color:#F97583;">let</span><span style="color:#E1E4E8;"> it </span><span style="color:#F97583;">in</span><span style="color:#E1E4E8;"> data) {</span></span>
<span class="line"><span style="color:#E1E4E8;">				ret </span><span style="color:#F97583;">+=</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">encodeURIComponent</span><span style="color:#E1E4E8;">(it) </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;=&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">encodeURIComponent</span><span style="color:#E1E4E8;">(data[it]) </span><span style="color:#F97583;">+</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;&amp;&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">			}</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> ret </span><span style="color:#6A737D;">// 便于直接取到内部data</span></span>
<span class="line"><span style="color:#E1E4E8;">		}],</span></span>
<span class="line"><span style="color:#E1E4E8;">		headers: {</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#6A737D;">// 认证和请求方式</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#9ECBFF;">&#39;Content-Type&#39;</span><span style="color:#E1E4E8;">: isJson </span><span style="color:#F97583;">===</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">1</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">?</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;application/json&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">:</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;application/x-www-form-urlencoded&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#9ECBFF;">&#39;token&#39;</span><span style="color:#E1E4E8;">: sessionStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;token&#39;</span><span style="color:#E1E4E8;">),</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#9ECBFF;">&#39;Authorization&#39;</span><span style="color:#E1E4E8;">: sessionStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;sessionId&#39;</span><span style="color:#E1E4E8;">),</span></span>
<span class="line"><span style="color:#E1E4E8;">		},</span></span>
<span class="line"><span style="color:#E1E4E8;">		params: method </span><span style="color:#F97583;">===</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;get&#39;</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">?</span><span style="color:#E1E4E8;"> params </span><span style="color:#F97583;">:</span><span style="color:#E1E4E8;"> </span><span style="color:#9ECBFF;">&#39;&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">	}).</span><span style="color:#B392F0;">then</span><span style="color:#E1E4E8;">(</span><span style="color:#FFAB70;">data</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=&gt;</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#B392F0;">endLoading</span><span style="color:#E1E4E8;">()</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#6A737D;">// console.log(&quot;返回结果：----》&quot; + JSON.stringify(data.data));</span></span>
<span class="line"><span style="color:#E1E4E8;">		</span><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> (data.data.code </span><span style="color:#F97583;">==</span><span style="color:#E1E4E8;"> </span><span style="color:#79B8FF;">0</span><span style="color:#E1E4E8;">) {</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#B392F0;">Func</span><span style="color:#E1E4E8;">(data.data)</span></span>
<span class="line"><span style="color:#E1E4E8;">		} </span><span style="color:#F97583;">else</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#6A737D;">//	错误提示</span></span>
<span class="line"><span style="color:#E1E4E8;">			ElementUI.</span><span style="color:#B392F0;">Message</span><span style="color:#E1E4E8;">({</span></span>
<span class="line"><span style="color:#E1E4E8;">				message: data.data.msg,</span></span>
<span class="line"><span style="color:#E1E4E8;">				type: </span><span style="color:#9ECBFF;">&#39;error&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">			});</span></span>
<span class="line"><span style="color:#E1E4E8;">		}</span></span>
<span class="line"><span style="color:#E1E4E8;">	})</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span>
<span class="line"><span style="color:#6A737D;">// uploadFileRequest  图片上传</span></span>
<span class="line"><span style="color:#F97583;">export</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">const</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">uploadFileRequest</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> (</span><span style="color:#FFAB70;">url</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">params</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">=&gt;</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#F97583;">return</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">axios</span><span style="color:#E1E4E8;">({</span></span>
<span class="line"><span style="color:#E1E4E8;">		method: </span><span style="color:#9ECBFF;">&#39;post&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		url: </span><span style="color:#9ECBFF;">\`\${</span><span style="color:#E1E4E8;">base</span><span style="color:#9ECBFF;">}\${</span><span style="color:#E1E4E8;">url</span><span style="color:#9ECBFF;">}\`</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		data: params,</span></span>
<span class="line"><span style="color:#E1E4E8;">		headers: {</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#9ECBFF;">&#39;Content-Type&#39;</span><span style="color:#E1E4E8;">: </span><span style="color:#9ECBFF;">&#39;multipart/form-data&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#9ECBFF;">&#39;token&#39;</span><span style="color:#E1E4E8;">: localStorage.</span><span style="color:#B392F0;">getItem</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;token&#39;</span><span style="color:#E1E4E8;">)</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#6A737D;">// &#39;authorization&#39;:&#39;admin&#39;,</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#6A737D;">// &#39;token&#39;:&#39;740a1d6be9c14292a13811cabb99950b&#39;</span></span>
<span class="line"><span style="color:#E1E4E8;">		}</span></span>
<span class="line"><span style="color:#E1E4E8;">	})</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">// get </span></span>
<span class="line"></span>
<span class="line"><span style="color:#F97583;">export</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">const</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">getRequest</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> (</span><span style="color:#FFAB70;">url</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">params</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">Func</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">isJson</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">=&gt;</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#B392F0;">request</span><span style="color:#E1E4E8;">(url, params, </span><span style="color:#9ECBFF;">&#39;get&#39;</span><span style="color:#E1E4E8;">, Func, isJson)</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">// post</span></span>
<span class="line"><span style="color:#F97583;">export</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">const</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">postRequest</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> (</span><span style="color:#FFAB70;">url</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">params</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">Func</span><span style="color:#E1E4E8;">, </span><span style="color:#FFAB70;">isJson</span><span style="color:#E1E4E8;">) </span><span style="color:#F97583;">=&gt;</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	</span><span style="color:#B392F0;">request</span><span style="color:#E1E4E8;">(url, params, </span><span style="color:#9ECBFF;">&#39;post&#39;</span><span style="color:#E1E4E8;">, Func, isJson)</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span>
<span class="line"><span style="color:#F97583;">function</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">startLoding</span><span style="color:#E1E4E8;">() {</span></span>
<span class="line"><span style="color:#E1E4E8;">	loading </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> Loading.</span><span style="color:#B392F0;">service</span><span style="color:#E1E4E8;">({</span></span>
<span class="line"><span style="color:#E1E4E8;">		lock: </span><span style="color:#79B8FF;">true</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		text: </span><span style="color:#9ECBFF;">&#39;加载中...&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">		spinner: </span><span style="color:#9ECBFF;">&#39;el-icon-loading&#39;</span><span style="color:#E1E4E8;">,</span></span>
<span class="line"><span style="color:#E1E4E8;">	});</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#F97583;">function</span><span style="color:#E1E4E8;"> </span><span style="color:#B392F0;">endLoading</span><span style="color:#E1E4E8;">(){</span></span>
<span class="line"><span style="color:#E1E4E8;">	loading.</span><span style="color:#B392F0;">close</span><span style="color:#E1E4E8;">()</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#D73A49;">import</span><span style="color:#24292E;"> axios </span><span style="color:#D73A49;">from</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;axios&#39;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#D73A49;">import</span><span style="color:#24292E;"> ElementUI </span><span style="color:#D73A49;">from</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;element-ui&#39;</span><span style="color:#24292E;">;</span></span>
<span class="line"><span style="color:#D73A49;">import</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	Loading</span></span>
<span class="line"><span style="color:#24292E;">} </span><span style="color:#D73A49;">from</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;element-ui&#39;</span><span style="color:#24292E;">;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#D73A49;">let</span><span style="color:#24292E;"> loading;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">axios.defaults.headers.common[</span><span style="color:#032F62;">&#39;Authorization&#39;</span><span style="color:#24292E;">] </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> sessionStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;sessionId&#39;</span><span style="color:#24292E;">)</span></span>
<span class="line"><span style="color:#6A737D;">// import qs from &#39;qs&#39;</span></span>
<span class="line"><span style="color:#24292E;">axios.defaults.withCredentials </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">true</span><span style="color:#24292E;">; </span><span style="color:#6A737D;">//	允许跨域</span></span>
<span class="line"><span style="color:#24292E;">axios.interceptors.request.</span><span style="color:#6F42C1;">use</span><span style="color:#24292E;">(</span><span style="color:#E36209;">config</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=&gt;</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">let</span><span style="color:#24292E;"> append </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> document.</span><span style="color:#6F42C1;">getElementsByName</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;body&#39;</span><span style="color:#24292E;">)</span></span>
<span class="line"><span style="color:#24292E;">	append.innerHTML </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;&lt;img style=&quot;position:fixed;</span><span style="color:#005CC5;">\\n</span><span style="color:#032F62;">&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#032F62;">&#39; left:47%;</span><span style="color:#005CC5;">\\n</span><span style="color:#032F62;">&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#032F62;">&#39; top:40%;</span><span style="color:#005CC5;">\\n</span><span style="color:#032F62;">&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#032F62;">&#39; transform: translateY(-50%),translateX(-50%);&quot;&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#032F62;">&#39; src=&quot;../../static/img/a.jpg&quot;/&gt;&#39;</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> config</span></span>
<span class="line"><span style="color:#24292E;">}, </span><span style="color:#E36209;">err</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=&gt;</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">Promise</span><span style="color:#24292E;">.</span><span style="color:#6F42C1;">resolve</span><span style="color:#24292E;">(err)</span></span>
<span class="line"><span style="color:#24292E;">})</span></span>
<span class="line"></span>
<span class="line"><span style="color:#D73A49;">let</span><span style="color:#24292E;"> base </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&quot;http://localhost:8099/api/&quot;</span><span style="color:#24292E;"> </span><span style="color:#6A737D;">// 接口域名</span></span>
<span class="line"></span>
<span class="line"><span style="color:#D73A49;">export</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">const</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">request</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> (</span><span style="color:#E36209;">url</span><span style="color:#24292E;">, </span><span style="color:#E36209;">params</span><span style="color:#24292E;">, </span><span style="color:#E36209;">method</span><span style="color:#24292E;">, </span><span style="color:#E36209;">Func</span><span style="color:#24292E;">, </span><span style="color:#E36209;">isJson</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">=&gt;</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6A737D;">// console.log(&quot;请求的接口---&gt;&quot; + base + url);</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6A737D;">// console.log(&quot;请求的方法---&gt;&quot; + method);</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6A737D;">// console.log(&quot;请求格式---&gt;&quot; + isJson);</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6A737D;">// console.log(&quot;请求的参数---&gt;&quot; + JSON.stringify(params));</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6F42C1;">startLoding</span><span style="color:#24292E;">()</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">var</span><span style="color:#24292E;"> _this </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">this</span><span style="color:#24292E;">;</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6F42C1;">axios</span><span style="color:#24292E;">({</span></span>
<span class="line"><span style="color:#24292E;">		method: method,</span></span>
<span class="line"><span style="color:#24292E;">		url: </span><span style="color:#032F62;">\`\${</span><span style="color:#24292E;">base</span><span style="color:#032F62;">}\${</span><span style="color:#24292E;">url</span><span style="color:#032F62;">}\`</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		data: method </span><span style="color:#D73A49;">===</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;post&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">?</span><span style="color:#24292E;"> params </span><span style="color:#D73A49;">:</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		transformRequest: [</span><span style="color:#D73A49;">function</span><span style="color:#24292E;">(</span><span style="color:#E36209;">data</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (isJson </span><span style="color:#D73A49;">===</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">1</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">				</span><span style="color:#6A737D;">// console.log(&quot;判断是否json格式或者是表单提交形式&quot;);</span></span>
<span class="line"><span style="color:#24292E;">				</span><span style="color:#6A737D;">// debugger       // 判断是否json格式或者是表单提交形式</span></span>
<span class="line"><span style="color:#24292E;">				</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">JSON</span><span style="color:#24292E;">.</span><span style="color:#6F42C1;">stringify</span><span style="color:#24292E;">(data)</span></span>
<span class="line"><span style="color:#24292E;">			}</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#D73A49;">let</span><span style="color:#24292E;"> ret </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;&#39;</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#D73A49;">for</span><span style="color:#24292E;"> (</span><span style="color:#D73A49;">let</span><span style="color:#24292E;"> it </span><span style="color:#D73A49;">in</span><span style="color:#24292E;"> data) {</span></span>
<span class="line"><span style="color:#24292E;">				ret </span><span style="color:#D73A49;">+=</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">encodeURIComponent</span><span style="color:#24292E;">(it) </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;=&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">encodeURIComponent</span><span style="color:#24292E;">(data[it]) </span><span style="color:#D73A49;">+</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;&amp;&#39;</span></span>
<span class="line"><span style="color:#24292E;">			}</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> ret </span><span style="color:#6A737D;">// 便于直接取到内部data</span></span>
<span class="line"><span style="color:#24292E;">		}],</span></span>
<span class="line"><span style="color:#24292E;">		headers: {</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#6A737D;">// 认证和请求方式</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#032F62;">&#39;Content-Type&#39;</span><span style="color:#24292E;">: isJson </span><span style="color:#D73A49;">===</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">1</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">?</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;application/json&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">:</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;application/x-www-form-urlencoded&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#032F62;">&#39;token&#39;</span><span style="color:#24292E;">: sessionStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;token&#39;</span><span style="color:#24292E;">),</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#032F62;">&#39;Authorization&#39;</span><span style="color:#24292E;">: sessionStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;sessionId&#39;</span><span style="color:#24292E;">),</span></span>
<span class="line"><span style="color:#24292E;">		},</span></span>
<span class="line"><span style="color:#24292E;">		params: method </span><span style="color:#D73A49;">===</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;get&#39;</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">?</span><span style="color:#24292E;"> params </span><span style="color:#D73A49;">:</span><span style="color:#24292E;"> </span><span style="color:#032F62;">&#39;&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">	}).</span><span style="color:#6F42C1;">then</span><span style="color:#24292E;">(</span><span style="color:#E36209;">data</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=&gt;</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6F42C1;">endLoading</span><span style="color:#24292E;">()</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#6A737D;">// console.log(&quot;返回结果：----》&quot; + JSON.stringify(data.data));</span></span>
<span class="line"><span style="color:#24292E;">		</span><span style="color:#D73A49;">if</span><span style="color:#24292E;"> (data.data.code </span><span style="color:#D73A49;">==</span><span style="color:#24292E;"> </span><span style="color:#005CC5;">0</span><span style="color:#24292E;">) {</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#6F42C1;">Func</span><span style="color:#24292E;">(data.data)</span></span>
<span class="line"><span style="color:#24292E;">		} </span><span style="color:#D73A49;">else</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#6A737D;">//	错误提示</span></span>
<span class="line"><span style="color:#24292E;">			ElementUI.</span><span style="color:#6F42C1;">Message</span><span style="color:#24292E;">({</span></span>
<span class="line"><span style="color:#24292E;">				message: data.data.msg,</span></span>
<span class="line"><span style="color:#24292E;">				type: </span><span style="color:#032F62;">&#39;error&#39;</span></span>
<span class="line"><span style="color:#24292E;">			});</span></span>
<span class="line"><span style="color:#24292E;">		}</span></span>
<span class="line"><span style="color:#24292E;">	})</span></span>
<span class="line"><span style="color:#24292E;">}</span></span>
<span class="line"><span style="color:#6A737D;">// uploadFileRequest  图片上传</span></span>
<span class="line"><span style="color:#D73A49;">export</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">const</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">uploadFileRequest</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> (</span><span style="color:#E36209;">url</span><span style="color:#24292E;">, </span><span style="color:#E36209;">params</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">=&gt;</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#D73A49;">return</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">axios</span><span style="color:#24292E;">({</span></span>
<span class="line"><span style="color:#24292E;">		method: </span><span style="color:#032F62;">&#39;post&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		url: </span><span style="color:#032F62;">\`\${</span><span style="color:#24292E;">base</span><span style="color:#032F62;">}\${</span><span style="color:#24292E;">url</span><span style="color:#032F62;">}\`</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		data: params,</span></span>
<span class="line"><span style="color:#24292E;">		headers: {</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#032F62;">&#39;Content-Type&#39;</span><span style="color:#24292E;">: </span><span style="color:#032F62;">&#39;multipart/form-data&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#032F62;">&#39;token&#39;</span><span style="color:#24292E;">: localStorage.</span><span style="color:#6F42C1;">getItem</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;token&#39;</span><span style="color:#24292E;">)</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#6A737D;">// &#39;authorization&#39;:&#39;admin&#39;,</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#6A737D;">// &#39;token&#39;:&#39;740a1d6be9c14292a13811cabb99950b&#39;</span></span>
<span class="line"><span style="color:#24292E;">		}</span></span>
<span class="line"><span style="color:#24292E;">	})</span></span>
<span class="line"><span style="color:#24292E;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">// get </span></span>
<span class="line"></span>
<span class="line"><span style="color:#D73A49;">export</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">const</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">getRequest</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> (</span><span style="color:#E36209;">url</span><span style="color:#24292E;">, </span><span style="color:#E36209;">params</span><span style="color:#24292E;">, </span><span style="color:#E36209;">Func</span><span style="color:#24292E;">, </span><span style="color:#E36209;">isJson</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">=&gt;</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6F42C1;">request</span><span style="color:#24292E;">(url, params, </span><span style="color:#032F62;">&#39;get&#39;</span><span style="color:#24292E;">, Func, isJson)</span></span>
<span class="line"><span style="color:#24292E;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">// post</span></span>
<span class="line"><span style="color:#D73A49;">export</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">const</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">postRequest</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> (</span><span style="color:#E36209;">url</span><span style="color:#24292E;">, </span><span style="color:#E36209;">params</span><span style="color:#24292E;">, </span><span style="color:#E36209;">Func</span><span style="color:#24292E;">, </span><span style="color:#E36209;">isJson</span><span style="color:#24292E;">) </span><span style="color:#D73A49;">=&gt;</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	</span><span style="color:#6F42C1;">request</span><span style="color:#24292E;">(url, params, </span><span style="color:#032F62;">&#39;post&#39;</span><span style="color:#24292E;">, Func, isJson)</span></span>
<span class="line"><span style="color:#24292E;">}</span></span>
<span class="line"><span style="color:#D73A49;">function</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">startLoding</span><span style="color:#24292E;">() {</span></span>
<span class="line"><span style="color:#24292E;">	loading </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> Loading.</span><span style="color:#6F42C1;">service</span><span style="color:#24292E;">({</span></span>
<span class="line"><span style="color:#24292E;">		lock: </span><span style="color:#005CC5;">true</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		text: </span><span style="color:#032F62;">&#39;加载中...&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">		spinner: </span><span style="color:#032F62;">&#39;el-icon-loading&#39;</span><span style="color:#24292E;">,</span></span>
<span class="line"><span style="color:#24292E;">	});</span></span>
<span class="line"><span style="color:#24292E;">}</span></span>
<span class="line"></span>
<span class="line"><span style="color:#D73A49;">function</span><span style="color:#24292E;"> </span><span style="color:#6F42C1;">endLoading</span><span style="color:#24292E;">(){</span></span>
<span class="line"><span style="color:#24292E;">	loading.</span><span style="color:#6F42C1;">close</span><span style="color:#24292E;">()</span></span>
<span class="line"><span style="color:#24292E;">}</span></span></code></pre></div><p>​</p>`,18),e=[o];function t(c,r,E,y,i,F){return n(),a("div",null,e)}const m=s(p,[["render",t]]);export{d as __pageData,m as default};
