import{_ as s,o as n,c as e,S as a}from"./chunks/framework.d7c45c4d.js";const g=JSON.parse('{"title":"定时任务发送短信提醒","description":"","frontmatter":{},"headers":[],"relativePath":"guide/others/pack.md","filePath":"guide/others/pack.md","lastUpdated":1705138198000}'),t={name:"guide/others/pack.md"},p=a(`<h1 id="定时任务发送短信提醒" tabindex="-1">定时任务发送短信提醒 <a class="header-anchor" href="#定时任务发送短信提醒" aria-label="Permalink to &quot;定时任务发送短信提醒&quot;">​</a></h1><h2 id="_1-验证接口" tabindex="-1">1. 验证接口 <a class="header-anchor" href="#_1-验证接口" aria-label="Permalink to &quot;1. 验证接口&quot;">​</a></h2><p>先验证发送短信得接口是否有用，调用正在运行的接口</p><div class="language-地址 vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">地址</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">https://beferp.com/befapi/push/sendTemplate</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">https://beferp.com/befapi/push/sendTemplate</span></span></code></pre></div><div class="language-参数 vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">参数</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">{</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;phone&quot;: &quot;18857131213&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;signName&quot;: &quot;杭州比弗企业管理&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;templateId&quot;: &quot;2004020&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;string&quot;: [</span></span>
<span class="line"><span style="color:#e1e4e8;">		&quot;桂B426H5&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">	]</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">{</span></span>
<span class="line"><span style="color:#24292e;">	&quot;phone&quot;: &quot;18857131213&quot;,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;signName&quot;: &quot;杭州比弗企业管理&quot;,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;templateId&quot;: &quot;2004020&quot;,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;string&quot;: [</span></span>
<span class="line"><span style="color:#24292e;">		&quot;桂B426H5&quot;</span></span>
<span class="line"><span style="color:#24292e;">	]</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><div class="language-结果 vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">结果</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">{</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;success&quot;: true,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;msg&quot;: &quot;成功&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;data&quot;: &quot;成功&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;code&quot;: 0</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">{</span></span>
<span class="line"><span style="color:#24292e;">	&quot;success&quot;: true,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;msg&quot;: &quot;成功&quot;,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;data&quot;: &quot;成功&quot;,</span></span>
<span class="line"><span style="color:#24292e;">	&quot;code&quot;: 0</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="_2-编码代码" tabindex="-1">2. 编码代码 <a class="header-anchor" href="#_2-编码代码" aria-label="Permalink to &quot;2. 编码代码&quot;">​</a></h2><ol><li>新增一个SmsServiceImpl，用core表达式循环执行发送短信的任务</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">@Scheduled(cron = &quot;0 0 10 * * ?&quot;) //每天上午10点</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">@Scheduled(cron = &quot;0 0 10 * * ?&quot;) //每天上午10点</span></span></code></pre></div><ol start="2"><li>在发送短信之前，我们要先查询出所有的web数据源</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">@Select(&quot;select * from web_datasource&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">List&lt;JSONObject&gt; selectCompany();</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">@Select(&quot;select * from web_datasource&quot;)</span></span>
<span class="line"><span style="color:#24292e;">List&lt;JSONObject&gt; selectCompany();</span></span></code></pre></div><ol start="3"><li>然后再循环每个数据源，通过账号及密码切到对应的数据库中</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">for (JSONObject json : jsonObjects) {</span></span>
<span class="line"><span style="color:#e1e4e8;">	String urls = json.getString(&quot;urls&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String password = json.getString(&quot;password&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String login_name = json.getString(&quot;login_name&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String data_name = json.getString(&quot;data_name&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String ports = json.getString(&quot;ports&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	commonUtils.findDataSource(urls, data_name, login_name, password, ports, true);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">for (JSONObject json : jsonObjects) {</span></span>
<span class="line"><span style="color:#24292e;">	String urls = json.getString(&quot;urls&quot;);</span></span>
<span class="line"><span style="color:#24292e;">	String password = json.getString(&quot;password&quot;);</span></span>
<span class="line"><span style="color:#24292e;">	String login_name = json.getString(&quot;login_name&quot;);</span></span>
<span class="line"><span style="color:#24292e;">	String data_name = json.getString(&quot;data_name&quot;);</span></span>
<span class="line"><span style="color:#24292e;">	String ports = json.getString(&quot;ports&quot;);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	commonUtils.findDataSource(urls, data_name, login_name, password, ports, true);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><ol start="4"><li>切换完成后，调用过程<code>p_bef_auto_sms_s</code></li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">@Select(&quot;{ CALL  p_bef_auto_sms_s()}&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">@Options(statementType = StatementType.CALLABLE)</span></span>
<span class="line"><span style="color:#e1e4e8;">List&lt;JSONObject&gt; selectSms();</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">@Select(&quot;{ CALL  p_bef_auto_sms_s()}&quot;)</span></span>
<span class="line"><span style="color:#24292e;">@Options(statementType = StatementType.CALLABLE)</span></span>
<span class="line"><span style="color:#24292e;">List&lt;JSONObject&gt; selectSms();</span></span></code></pre></div><ol start="5"><li>最后将过程查出来的数据循环遍历，发送<code>txlx</code>为11的生日短信提醒</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">[</span></span>
<span class="line"><span style="color:#e1e4e8;">    {</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;p1&quot;:&quot;浙88888&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;p2&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;p3&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;open_id&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;txlx&quot;:11,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;msm&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;sign&quot;:&quot;杭州比弗企业管理&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;rem_je&quot;:0,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;lx&quot;:&quot;message&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;cp&quot;:&quot;浙88888&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;sms_id&quot;:&quot;2004020&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;use_je&quot;:0,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;tel&quot;:&quot;18857131213&quot;,</span></span>
<span class="line"><span style="color:#e1e4e8;">        &quot;app_id&quot;:&quot;&quot;</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;">]</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">[</span></span>
<span class="line"><span style="color:#24292e;">    {</span></span>
<span class="line"><span style="color:#24292e;">        &quot;p1&quot;:&quot;浙88888&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;p2&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;p3&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;open_id&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;txlx&quot;:11,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;msm&quot;:&quot;&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;sign&quot;:&quot;杭州比弗企业管理&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;rem_je&quot;:0,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;lx&quot;:&quot;message&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;cp&quot;:&quot;浙88888&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;sms_id&quot;:&quot;2004020&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;use_je&quot;:0,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;tel&quot;:&quot;18857131213&quot;,</span></span>
<span class="line"><span style="color:#24292e;">        &quot;app_id&quot;:&quot;&quot;</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;">]</span></span></code></pre></div><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">for (JSONObject js : jsonObjects1) {</span></span>
<span class="line"><span style="color:#e1e4e8;">	JSONObject requestJSon = new JSONObject();</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	String sms_id = js.getString(&quot;sms_id&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String sign = js.getString(&quot;sign&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String tel = js.getString(&quot;tel&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String txlx = js.getString(&quot;txlx&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	if (&quot;11&quot;.equals(txlx)) {   //  生日提醒</span></span>
<span class="line"><span style="color:#e1e4e8;">		String p1 = js.getString(&quot;p1&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">		ArrayList&lt;Object&gt; objects = new ArrayList&lt;&gt;();</span></span>
<span class="line"><span style="color:#e1e4e8;">		objects.add(p1);</span></span>
<span class="line"><span style="color:#e1e4e8;">		requestJSon.put(&quot;string&quot;, objects);</span></span>
<span class="line"><span style="color:#e1e4e8;">	}</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">	requestJSon.put(&quot;phone&quot;, tel);</span></span>
<span class="line"><span style="color:#e1e4e8;">	requestJSon.put(&quot;signName&quot;, sign);</span></span>
<span class="line"><span style="color:#e1e4e8;">	requestJSon.put(&quot;templateId&quot;, sms_id);</span></span>
<span class="line"><span style="color:#e1e4e8;">	String str = HttpClientUtils.doPost(&quot;https://beferp.com/befapi/push/sendTemplate&quot;, requestJSon);</span></span>
<span class="line"><span style="color:#e1e4e8;">	log.info(str);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">for (JSONObject js : jsonObjects1) {</span></span>
<span class="line"><span style="color:#24292e;">	JSONObject requestJSon = new JSONObject();</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	String sms_id = js.getString(&quot;sms_id&quot;);</span></span>
<span class="line"><span style="color:#24292e;">	String sign = js.getString(&quot;sign&quot;);</span></span>
<span class="line"><span style="color:#24292e;">	String tel = js.getString(&quot;tel&quot;);</span></span>
<span class="line"><span style="color:#24292e;">	String txlx = js.getString(&quot;txlx&quot;);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	if (&quot;11&quot;.equals(txlx)) {   //  生日提醒</span></span>
<span class="line"><span style="color:#24292e;">		String p1 = js.getString(&quot;p1&quot;);</span></span>
<span class="line"><span style="color:#24292e;">		ArrayList&lt;Object&gt; objects = new ArrayList&lt;&gt;();</span></span>
<span class="line"><span style="color:#24292e;">		objects.add(p1);</span></span>
<span class="line"><span style="color:#24292e;">		requestJSon.put(&quot;string&quot;, objects);</span></span>
<span class="line"><span style="color:#24292e;">	}</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">	requestJSon.put(&quot;phone&quot;, tel);</span></span>
<span class="line"><span style="color:#24292e;">	requestJSon.put(&quot;signName&quot;, sign);</span></span>
<span class="line"><span style="color:#24292e;">	requestJSon.put(&quot;templateId&quot;, sms_id);</span></span>
<span class="line"><span style="color:#24292e;">	String str = HttpClientUtils.doPost(&quot;https://beferp.com/befapi/push/sendTemplate&quot;, requestJSon);</span></span>
<span class="line"><span style="color:#24292e;">	log.info(str);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div>`,18),l=[p];function o(c,i,r,u,q,d){return n(),e("div",null,l)}const h=s(t,[["render",o]]);export{g as __pageData,h as default};
