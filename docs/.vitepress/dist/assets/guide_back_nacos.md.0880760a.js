import{_ as s,o as a,c as n,S as e}from"./chunks/framework.d7c45c4d.js";const l="/blog/assets/739aa85fcbc49c796ab6e4b769e3e91.c6d7c475.png",g=JSON.parse('{"title":"win使用nacos","description":"","frontmatter":{},"headers":[],"relativePath":"guide/back/nacos.md","filePath":"guide/back/nacos.md","lastUpdated":1700278256000}'),p={name:"guide/back/nacos.md"},o=e('<h1 id="win使用nacos" tabindex="-1">win使用nacos <a class="header-anchor" href="#win使用nacos" aria-label="Permalink to &quot;win使用nacos&quot;">​</a></h1><blockquote><p>nacos文档地址</p></blockquote><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">https://nacos.io/zh-cn/docs/v2/quickstart/quick-start.html</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">https://nacos.io/zh-cn/docs/v2/quickstart/quick-start.html</span></span></code></pre></div><p><img src="'+l+`" alt=""></p><blockquote><p>下载nacos项目</p></blockquote><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">https://github.com/alibaba/nacos/releases</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">https://github.com/alibaba/nacos/releases</span></span></code></pre></div><blockquote><p>运行nacos</p></blockquote><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">解压nacos项目</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">cd conf</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">打开application.properties，找到nacos.core.auth.plugin.nacos.token.secret.key</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">设置key=012345678901234567890123456789012345678901234567890123456789</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">cd nacos/bin</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">打开cmd,启动命令</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">startup.cmd -m standalone</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">(standalone代表着单机模式运行，非集群模式)</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">解压nacos项目</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">cd conf</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">打开application.properties，找到nacos.core.auth.plugin.nacos.token.secret.key</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">设置key=012345678901234567890123456789012345678901234567890123456789</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">cd nacos/bin</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">打开cmd,启动命令</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">startup.cmd -m standalone</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">(standalone代表着单机模式运行，非集群模式)</span></span></code></pre></div><ol><li>浏览器访问</li></ol><p><a href="http://192.168.3.5:8848/nacos/index.html#/login" target="_blank" rel="noreferrer">http://192.168.3.5:8848/nacos/index.html#/login</a></p><ol start="2"><li>默认登录 nacos/nacos</li></ol>`,11),c=[o];function t(i,r,d,h,y,u){return a(),n("div",null,c)}const k=s(p,[["render",t]]);export{g as __pageData,k as default};
