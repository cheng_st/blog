import{_ as s,o as n,c as a,S as e}from"./chunks/framework.d7c45c4d.js";const h=JSON.parse('{"title":"vitepress使用卜蒜子做访问统计","description":"","frontmatter":{},"headers":[],"relativePath":"guide/others/bsz.md","filePath":"guide/others/bsz.md","lastUpdated":1703603305000}'),p={name:"guide/others/bsz.md"},l=e(`<h1 id="vitepress使用卜蒜子做访问统计" tabindex="-1">vitepress使用卜蒜子做访问统计 <a class="header-anchor" href="#vitepress使用卜蒜子做访问统计" aria-label="Permalink to &quot;vitepress使用卜蒜子做访问统计&quot;">​</a></h1><ol><li>打开.vitepress目录下得config.js，加上以下内容</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">module.exports = {</span></span>
<span class="line"><span style="color:#e1e4e8;">	head: [</span></span>
<span class="line"><span style="color:#e1e4e8;">		[</span></span>
<span class="line"><span style="color:#e1e4e8;">			&#39;script&#39;, { type: &#39;text/javascript&#39;, src: &#39;https://busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js&#39; }</span></span>
<span class="line"><span style="color:#e1e4e8;">		]</span></span>
<span class="line"><span style="color:#e1e4e8;">	],</span></span>
<span class="line"><span style="color:#e1e4e8;">	</span></span>
<span class="line"><span style="color:#e1e4e8;">	......</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">module.exports = {</span></span>
<span class="line"><span style="color:#24292e;">	head: [</span></span>
<span class="line"><span style="color:#24292e;">		[</span></span>
<span class="line"><span style="color:#24292e;">			&#39;script&#39;, { type: &#39;text/javascript&#39;, src: &#39;https://busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js&#39; }</span></span>
<span class="line"><span style="color:#24292e;">		]</span></span>
<span class="line"><span style="color:#24292e;">	],</span></span>
<span class="line"><span style="color:#24292e;">	</span></span>
<span class="line"><span style="color:#24292e;">	......</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><ol start="2"><li>在.vitepress下新建一个components文件夹，然后新建一个vue文件<code>Bsz.vue</code></li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;div class=&quot;statistics&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">        &lt;div class=&quot;busuanzi&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;span id=&quot;busuanzi_container_site_pv&quot; style=&quot;margin-right: 20px;&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">                本站总访问量</span></span>
<span class="line"><span style="color:#e1e4e8;">                &lt;span id=&quot;busuanzi_value_site_pv&quot;&gt;&lt;/span&gt;次</span></span>
<span class="line"><span style="color:#e1e4e8;">                &lt;!-- &lt;span class=&quot;post-meta-divider&quot;&gt;|&lt;/span&gt; --&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;/span&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">            ❤️</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;span id=&quot;busuanzi_container_site_uv&quot; style=&quot;margin-left: 20px;&quot;&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">                本站访客数</span></span>
<span class="line"><span style="color:#e1e4e8;">                &lt;span id=&quot;busuanzi_value_site_uv&quot;&gt;&lt;/span&gt;人</span></span>
<span class="line"><span style="color:#e1e4e8;">            &lt;/span&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">        &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;/div&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;style&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">.statistics {</span></span>
<span class="line"><span style="color:#e1e4e8;">    text-align: center;</span></span>
<span class="line"><span style="color:#e1e4e8;">    bottom: 120px;</span></span>
<span class="line"><span style="color:#e1e4e8;">    position: absolute;</span></span>
<span class="line"><span style="color:#e1e4e8;">    margin: 0 auto;</span></span>
<span class="line"><span style="color:#e1e4e8;">    left: 0;</span></span>
<span class="line"><span style="color:#e1e4e8;">    right: 0;</span></span>
<span class="line"><span style="color:#e1e4e8;">    color: grey;</span></span>
<span class="line"><span style="color:#e1e4e8;">    font-size: 15px;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/style&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;div class=&quot;statistics&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">        &lt;div class=&quot;busuanzi&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;span id=&quot;busuanzi_container_site_pv&quot; style=&quot;margin-right: 20px;&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">                本站总访问量</span></span>
<span class="line"><span style="color:#24292e;">                &lt;span id=&quot;busuanzi_value_site_pv&quot;&gt;&lt;/span&gt;次</span></span>
<span class="line"><span style="color:#24292e;">                &lt;!-- &lt;span class=&quot;post-meta-divider&quot;&gt;|&lt;/span&gt; --&gt;</span></span>
<span class="line"><span style="color:#24292e;">            &lt;/span&gt;</span></span>
<span class="line"><span style="color:#24292e;">            ❤️</span></span>
<span class="line"><span style="color:#24292e;">            &lt;span id=&quot;busuanzi_container_site_uv&quot; style=&quot;margin-left: 20px;&quot;&gt;</span></span>
<span class="line"><span style="color:#24292e;">                本站访客数</span></span>
<span class="line"><span style="color:#24292e;">                &lt;span id=&quot;busuanzi_value_site_uv&quot;&gt;&lt;/span&gt;人</span></span>
<span class="line"><span style="color:#24292e;">            &lt;/span&gt;</span></span>
<span class="line"><span style="color:#24292e;">        &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;/div&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;style&gt;</span></span>
<span class="line"><span style="color:#24292e;">.statistics {</span></span>
<span class="line"><span style="color:#24292e;">    text-align: center;</span></span>
<span class="line"><span style="color:#24292e;">    bottom: 120px;</span></span>
<span class="line"><span style="color:#24292e;">    position: absolute;</span></span>
<span class="line"><span style="color:#24292e;">    margin: 0 auto;</span></span>
<span class="line"><span style="color:#24292e;">    left: 0;</span></span>
<span class="line"><span style="color:#24292e;">    right: 0;</span></span>
<span class="line"><span style="color:#24292e;">    color: grey;</span></span>
<span class="line"><span style="color:#24292e;">    font-size: 15px;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span>
<span class="line"><span style="color:#24292e;">&lt;/style&gt;</span></span></code></pre></div><ol start="3"><li>打开theme文件夹下得index.js,添加如下内容</li></ol><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import { h } from &quot;vue&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import Bsz from &quot;../components/Bsz.vue&quot;;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import { h } from &quot;vue&quot;;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import Bsz from &quot;../components/Bsz.vue&quot;;</span></span></code></pre></div><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">export default {</span></span>
<span class="line"><span style="color:#e1e4e8;">	...DefaultTheme,</span></span>
<span class="line"><span style="color:#e1e4e8;">	Layout() {</span></span>
<span class="line"><span style="color:#e1e4e8;">		return h(DefaultTheme.Layout, null, {</span></span>
<span class="line"><span style="color:#e1e4e8;">			&quot;home-features-after&quot;: () =&gt; h(Bsz),</span></span>
<span class="line"><span style="color:#e1e4e8;">		});</span></span>
<span class="line"><span style="color:#e1e4e8;">	},</span></span>
<span class="line"><span style="color:#e1e4e8;">	enhanceApp({</span></span>
<span class="line"><span style="color:#e1e4e8;">		app</span></span>
<span class="line"><span style="color:#e1e4e8;">	}) {},</span></span>
<span class="line"><span style="color:#e1e4e8;">};</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">export default {</span></span>
<span class="line"><span style="color:#24292e;">	...DefaultTheme,</span></span>
<span class="line"><span style="color:#24292e;">	Layout() {</span></span>
<span class="line"><span style="color:#24292e;">		return h(DefaultTheme.Layout, null, {</span></span>
<span class="line"><span style="color:#24292e;">			&quot;home-features-after&quot;: () =&gt; h(Bsz),</span></span>
<span class="line"><span style="color:#24292e;">		});</span></span>
<span class="line"><span style="color:#24292e;">	},</span></span>
<span class="line"><span style="color:#24292e;">	enhanceApp({</span></span>
<span class="line"><span style="color:#24292e;">		app</span></span>
<span class="line"><span style="color:#24292e;">	}) {},</span></span>
<span class="line"><span style="color:#24292e;">};</span></span></code></pre></div><ol start="4"><li>打包后如果报错</li></ol><p><code>If this is expected, you can disable this check via config. Refer: https://vitepress.dev/reference/site-config#ignoredeadlinks</code></p><p>在config.js中加一句话就行</p><div class="language-javascript vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">javascript</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#79B8FF;">module</span><span style="color:#E1E4E8;">.</span><span style="color:#79B8FF;">exports</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">=</span><span style="color:#E1E4E8;"> {</span></span>
<span class="line"><span style="color:#E1E4E8;">	head: [</span></span>
<span class="line"><span style="color:#E1E4E8;">		[</span></span>
<span class="line"><span style="color:#E1E4E8;">			</span><span style="color:#9ECBFF;">&#39;script&#39;</span><span style="color:#E1E4E8;">, { type: </span><span style="color:#9ECBFF;">&#39;text/javascript&#39;</span><span style="color:#E1E4E8;">, src: </span><span style="color:#9ECBFF;">&#39;https://busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js&#39;</span><span style="color:#E1E4E8;"> }</span></span>
<span class="line"><span style="color:#E1E4E8;">		]</span></span>
<span class="line"><span style="color:#E1E4E8;">	],</span></span>
<span class="line"><span style="color:#E1E4E8;">	ignoreDeadLinks: </span><span style="color:#79B8FF;">true</span><span style="color:#E1E4E8;">	</span><span style="color:#6A737D;">//	死链</span></span>
<span class="line"><span style="color:#E1E4E8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#005CC5;">module</span><span style="color:#24292E;">.</span><span style="color:#005CC5;">exports</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">=</span><span style="color:#24292E;"> {</span></span>
<span class="line"><span style="color:#24292E;">	head: [</span></span>
<span class="line"><span style="color:#24292E;">		[</span></span>
<span class="line"><span style="color:#24292E;">			</span><span style="color:#032F62;">&#39;script&#39;</span><span style="color:#24292E;">, { type: </span><span style="color:#032F62;">&#39;text/javascript&#39;</span><span style="color:#24292E;">, src: </span><span style="color:#032F62;">&#39;https://busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js&#39;</span><span style="color:#24292E;"> }</span></span>
<span class="line"><span style="color:#24292E;">		]</span></span>
<span class="line"><span style="color:#24292E;">	],</span></span>
<span class="line"><span style="color:#24292E;">	ignoreDeadLinks: </span><span style="color:#005CC5;">true</span><span style="color:#24292E;">	</span><span style="color:#6A737D;">//	死链</span></span>
<span class="line"><span style="color:#24292E;">}</span></span></code></pre></div>`,12),t=[l];function o(c,i,r,u,y,d){return n(),a("div",null,t)}const v=s(p,[["render",o]]);export{h as __pageData,v as default};
