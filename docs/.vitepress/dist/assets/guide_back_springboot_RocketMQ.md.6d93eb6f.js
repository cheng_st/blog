import{_ as s,o as n,c as e,S as a}from"./chunks/framework.d7c45c4d.js";const p="/blog/assets/image-20230318113334107.3d5ddf80.png",q=JSON.parse('{"title":"springboot集成RocketMQ","description":"","frontmatter":{},"headers":[],"relativePath":"guide/back/springboot/RocketMQ.md","filePath":"guide/back/springboot/RocketMQ.md","lastUpdated":1700278256000}'),l={name:"guide/back/springboot/RocketMQ.md"},o=a('<h1 id="springboot集成rocketmq" tabindex="-1">springboot集成RocketMQ <a class="header-anchor" href="#springboot集成rocketmq" aria-label="Permalink to &quot;springboot集成RocketMQ&quot;">​</a></h1><h2 id="rocketmq安装部署" tabindex="-1">RocketMQ安装部署 <a class="header-anchor" href="#rocketmq安装部署" aria-label="Permalink to &quot;RocketMQ安装部署&quot;">​</a></h2><h2 id="_1-下载-5-0-0" tabindex="-1">1. 下载 5.0.0 <a class="header-anchor" href="#_1-下载-5-0-0" aria-label="Permalink to &quot;1. 下载 5.0.0&quot;">​</a></h2><p>下载地址：<a href="https://rocketmq.apache.org/dowloading/releases/" target="_blank" rel="noreferrer">https://rocketmq.apache.org/dowloading/releases/</a></p><p><img src="'+p+`" alt=""></p><h2 id="_2-设置环境变量" tabindex="-1">2. 设置环境变量： <a class="header-anchor" href="#_2-设置环境变量" aria-label="Permalink to &quot;2. 设置环境变量：&quot;">​</a></h2><p><strong>变量名：</strong> ROCKETMQ_HOME</p><p><strong>变量值：</strong> G:\\rocketmq-all-5.0.0-bin-release\\bin MQ解压路径\\MQ文件夹名</p><h2 id="_3-启动" tabindex="-1">3. 启动 <a class="header-anchor" href="#_3-启动" aria-label="Permalink to &quot;3. 启动&quot;">​</a></h2><p>在<code>rocketmq-all-5.0.0-bin-release\\bin</code>目录下，打开cmd窗口</p><p>先启动 nameServer，启动命令：</p><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">start mqnamesrv.cmd</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">start mqnamesrv.cmd</span></span></code></pre></div><p>然后在启动 Broker，启动命令：</p><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">start mqbroker.cmd -n 127.0.0.1:9876 autoCreateTopicEnable=true</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">start mqbroker.cmd -n 127.0.0.1:9876 autoCreateTopicEnable=true</span></span></code></pre></div><h2 id="_4-下载代码运行" tabindex="-1">4. 下载代码运行 <a class="header-anchor" href="#_4-下载代码运行" aria-label="Permalink to &quot;4. 下载代码运行&quot;">​</a></h2><p><a href="https://github.com/apache/rocketmq-dashboard" target="_blank" rel="noreferrer">https://github.com/apache/rocketmq-dashboard</a></p><p>启动完成之后，浏览器中输入<code>127.0.0.1:8080</code>，成功后即可进行管理端查看。</p><h3 id="pom-xml" tabindex="-1">pom.xml <a class="header-anchor" href="#pom-xml" aria-label="Permalink to &quot;pom.xml&quot;">​</a></h3><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"> &lt;!--引入lombok--&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;groupId&gt;org.projectlombok&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;artifactId&gt;lombok&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;version&gt;1.16.10&lt;/version&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">		</span></span>
<span class="line"><span style="color:#e1e4e8;">		</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;groupId&gt;org.apache.rocketmq&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;artifactId&gt;rocketmq-client-java&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;version&gt;5.0.0&lt;/version&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;groupId&gt;org.apache.rocketmq&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;artifactId&gt;rocketmq-spring-boot-starter&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;version&gt;2.2.1&lt;/version&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/dependency&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"> &lt;!--引入lombok--&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;groupId&gt;org.projectlombok&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;artifactId&gt;lombok&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;version&gt;1.16.10&lt;/version&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;">		</span></span>
<span class="line"><span style="color:#24292e;">		</span></span>
<span class="line"><span style="color:#24292e;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;groupId&gt;org.apache.rocketmq&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;artifactId&gt;rocketmq-client-java&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;version&gt;5.0.0&lt;/version&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;groupId&gt;org.apache.rocketmq&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;artifactId&gt;rocketmq-spring-boot-starter&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;version&gt;2.2.1&lt;/version&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/dependency&gt;</span></span></code></pre></div><h4 id="如果使用了阿里的json-调整下版本" tabindex="-1">如果使用了阿里的json，调整下版本 <a class="header-anchor" href="#如果使用了阿里的json-调整下版本" aria-label="Permalink to &quot;如果使用了阿里的json，调整下版本&quot;">​</a></h4><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;groupId&gt;com.alibaba&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;artifactId&gt;fastjson&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">	&lt;version&gt;2.0.19&lt;/version&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/dependency&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;groupId&gt;com.alibaba&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;artifactId&gt;fastjson&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#24292e;">	&lt;version&gt;2.0.19&lt;/version&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/dependency&gt;</span></span></code></pre></div><h4 id="application-prooerties" tabindex="-1">application.prooerties <a class="header-anchor" href="#application-prooerties" aria-label="Permalink to &quot;application.prooerties&quot;">​</a></h4><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">#rocket消息队列配置</span></span>
<span class="line"><span style="color:#e1e4e8;">rocketmq.name-server=127.0.0.1:9876</span></span>
<span class="line"><span style="color:#e1e4e8;">rocketmq.producer.enable=true</span></span>
<span class="line"><span style="color:#e1e4e8;">rocketmq.producer.group=springBootGroup</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">#rocket消息队列配置</span></span>
<span class="line"><span style="color:#24292e;">rocketmq.name-server=127.0.0.1:9876</span></span>
<span class="line"><span style="color:#24292e;">rocketmq.producer.enable=true</span></span>
<span class="line"><span style="color:#24292e;">rocketmq.producer.group=springBootGroup</span></span></code></pre></div><h2 id="消息实体类" tabindex="-1">消息实体类 <a class="header-anchor" href="#消息实体类" aria-label="Permalink to &quot;消息实体类&quot;">​</a></h2><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">@Data</span></span>
<span class="line"><span style="color:#e1e4e8;">public class MqMsg {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    private String topic;</span></span>
<span class="line"><span style="color:#e1e4e8;">    private String tag;</span></span>
<span class="line"><span style="color:#e1e4e8;">    private Object content;</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">@Data</span></span>
<span class="line"><span style="color:#24292e;">public class MqMsg {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    private String topic;</span></span>
<span class="line"><span style="color:#24292e;">    private String tag;</span></span>
<span class="line"><span style="color:#24292e;">    private Object content;</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="生产者" tabindex="-1">生产者 <a class="header-anchor" href="#生产者" aria-label="Permalink to &quot;生产者&quot;">​</a></h2><p>进行SpringBoot和RocketMQ整合时，关键使用的是RocketMQTemplate类来进行消息发送，其中包含有send()、asyncSend()、sendOneWay()、sendMessageInTransaction()等方法，每个方法都至少包含有参数destination和message。</p><p>destination：消息发送到哪个topic和tag，在SpringBoot中topic和tag使用一个参数发送，其中用英文的冒号（:）进行连接。</p><p>message：消息体，需要使用 MessageBuilder.withPayload方法对消息进行封装。</p><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">public interface RocketMqService {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 同步发送消息</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    void send(MqMsg mqMsg);</span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 异步发送消息，异步返回消息结果</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    void asyncSend(MqMsg mqMsg);</span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 单向发送消息，不关心返回结果，容易消息丢失，适合日志收集、不精确统计等消息发送;</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    void syncSendOrderly(MqMsg mqMsg);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">public interface RocketMqService {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 同步发送消息</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    void send(MqMsg mqMsg);</span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 异步发送消息，异步返回消息结果</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    void asyncSend(MqMsg mqMsg);</span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 单向发送消息，不关心返回结果，容易消息丢失，适合日志收集、不精确统计等消息发送;</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    void syncSendOrderly(MqMsg mqMsg);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import org.apache.rocketmq.client.producer.SendCallback;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.apache.rocketmq.client.producer.SendResult;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.apache.rocketmq.spring.core.RocketMQTemplate;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.beans.factory.annotation.Autowired;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.messaging.support.MessageBuilder;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.stereotype.Service;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">@Service</span></span>
<span class="line"><span style="color:#e1e4e8;">public class RocketMqServiceImpl implements RocketMqService {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @Autowired</span></span>
<span class="line"><span style="color:#e1e4e8;">    private RocketMQTemplate rocketMQTemplate;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    private static final Logger log = LoggerFactory.getLogger(RocketMqServiceImpl.class);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void send(MqMsg msg) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        log.info(&quot;send发送消息:{}&quot;, msg);</span></span>
<span class="line"><span style="color:#e1e4e8;">        rocketMQTemplate.send(msg.getTopic() + &quot;:&quot; + msg.getTag(),</span></span>
<span class="line"><span style="color:#e1e4e8;">                MessageBuilder.withPayload(msg.getContent()).build());</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void asyncSend(MqMsg msg) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        log.info(&quot;asyncSend发送消息:{}&quot;, msg);</span></span>
<span class="line"><span style="color:#e1e4e8;">        rocketMQTemplate.asyncSend(msg.getTopic() + &quot;:&quot; + msg.getTag(), msg.getContent(),</span></span>
<span class="line"><span style="color:#e1e4e8;">                new SendCallback() {</span></span>
<span class="line"><span style="color:#e1e4e8;">                    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">                    public void onSuccess(SendResult sendResult) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                        log.info(&quot;事物消息发送成功:{}&quot;, sendResult.getTransactionId());</span></span>
<span class="line"><span style="color:#e1e4e8;">                    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">                    public void onException(Throwable throwable) {</span></span>
<span class="line"><span style="color:#e1e4e8;">                        log.info(&quot;mqMsg={}消息发送失败&quot;, msg);</span></span>
<span class="line"><span style="color:#e1e4e8;">                    }</span></span>
<span class="line"><span style="color:#e1e4e8;">                });</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void syncSendOrderly(MqMsg msg) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        log.info(&quot;syncSendOrderly发送消息:{}&quot;, msg);</span></span>
<span class="line"><span style="color:#e1e4e8;">        rocketMQTemplate.sendOneWay(msg.getTopic() + &quot;:&quot; + msg.getTag(), msg.getContent());</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import org.apache.rocketmq.client.producer.SendCallback;</span></span>
<span class="line"><span style="color:#24292e;">import org.apache.rocketmq.client.producer.SendResult;</span></span>
<span class="line"><span style="color:#24292e;">import org.apache.rocketmq.spring.core.RocketMQTemplate;</span></span>
<span class="line"><span style="color:#24292e;">import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#24292e;">import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.beans.factory.annotation.Autowired;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.messaging.support.MessageBuilder;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.stereotype.Service;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">@Service</span></span>
<span class="line"><span style="color:#24292e;">public class RocketMqServiceImpl implements RocketMqService {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @Autowired</span></span>
<span class="line"><span style="color:#24292e;">    private RocketMQTemplate rocketMQTemplate;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    private static final Logger log = LoggerFactory.getLogger(RocketMqServiceImpl.class);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @Override</span></span>
<span class="line"><span style="color:#24292e;">    public void send(MqMsg msg) {</span></span>
<span class="line"><span style="color:#24292e;">        log.info(&quot;send发送消息:{}&quot;, msg);</span></span>
<span class="line"><span style="color:#24292e;">        rocketMQTemplate.send(msg.getTopic() + &quot;:&quot; + msg.getTag(),</span></span>
<span class="line"><span style="color:#24292e;">                MessageBuilder.withPayload(msg.getContent()).build());</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @Override</span></span>
<span class="line"><span style="color:#24292e;">    public void asyncSend(MqMsg msg) {</span></span>
<span class="line"><span style="color:#24292e;">        log.info(&quot;asyncSend发送消息:{}&quot;, msg);</span></span>
<span class="line"><span style="color:#24292e;">        rocketMQTemplate.asyncSend(msg.getTopic() + &quot;:&quot; + msg.getTag(), msg.getContent(),</span></span>
<span class="line"><span style="color:#24292e;">                new SendCallback() {</span></span>
<span class="line"><span style="color:#24292e;">                    @Override</span></span>
<span class="line"><span style="color:#24292e;">                    public void onSuccess(SendResult sendResult) {</span></span>
<span class="line"><span style="color:#24292e;">                        log.info(&quot;事物消息发送成功:{}&quot;, sendResult.getTransactionId());</span></span>
<span class="line"><span style="color:#24292e;">                    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                    @Override</span></span>
<span class="line"><span style="color:#24292e;">                    public void onException(Throwable throwable) {</span></span>
<span class="line"><span style="color:#24292e;">                        log.info(&quot;mqMsg={}消息发送失败&quot;, msg);</span></span>
<span class="line"><span style="color:#24292e;">                    }</span></span>
<span class="line"><span style="color:#24292e;">                });</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @Override</span></span>
<span class="line"><span style="color:#24292e;">    public void syncSendOrderly(MqMsg msg) {</span></span>
<span class="line"><span style="color:#24292e;">        log.info(&quot;syncSendOrderly发送消息:{}&quot;, msg);</span></span>
<span class="line"><span style="color:#24292e;">        rocketMQTemplate.sendOneWay(msg.getTopic() + &quot;:&quot; + msg.getTag(), msg.getContent());</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="消费者" tabindex="-1">消费者 <a class="header-anchor" href="#消费者" aria-label="Permalink to &quot;消费者&quot;">​</a></h2><p>1.消息消费者需要实现接口RocketMQListener并重写onMessage消费方法 2.@RocketMQMessageListener注解参数解释</p><ul><li><p>topic:表示需要监听哪个topic的消息</p></li><li><p>consumerGroup:表示消费者组</p></li><li><p>selectorExpression:表示需要监听的tag</p></li></ul><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.apache.rocketmq.spring.core.RocketMQListener;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.stereotype.Service;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">@Service</span></span>
<span class="line"><span style="color:#e1e4e8;">@RocketMQMessageListener(topic = &quot;topicA&quot;, consumerGroup = &quot;consumer1&quot;, selectorExpression = &quot;send&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">public class SendConsumer implements RocketMQListener&lt;String&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    private static final Logger log = LoggerFactory.getLogger(SendConsumer.class);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void onMessage(String s) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        log.info(&quot;消费者成功消费消息:{}&quot;, s);</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;</span></span>
<span class="line"><span style="color:#24292e;">import org.apache.rocketmq.spring.core.RocketMQListener;</span></span>
<span class="line"><span style="color:#24292e;">import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#24292e;">import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.stereotype.Service;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">@Service</span></span>
<span class="line"><span style="color:#24292e;">@RocketMQMessageListener(topic = &quot;topicA&quot;, consumerGroup = &quot;consumer1&quot;, selectorExpression = &quot;send&quot;)</span></span>
<span class="line"><span style="color:#24292e;">public class SendConsumer implements RocketMQListener&lt;String&gt; {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    private static final Logger log = LoggerFactory.getLogger(SendConsumer.class);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @Override</span></span>
<span class="line"><span style="color:#24292e;">    public void onMessage(String s) {</span></span>
<span class="line"><span style="color:#24292e;">        log.info(&quot;消费者成功消费消息:{}&quot;, s);</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;</span></span>
<span class="line"><span style="color:#e1e4e8;">    import org.apache.rocketmq.spring.core.RocketMQListener;</span></span>
<span class="line"><span style="color:#e1e4e8;">    import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#e1e4e8;">    import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#e1e4e8;">    import org.springframework.stereotype.Service;</span></span>
<span class="line"><span style="color:#e1e4e8;">    </span></span>
<span class="line"><span style="color:#e1e4e8;">    @Service</span></span>
<span class="line"><span style="color:#e1e4e8;">    @RocketMQMessageListener(topic = &quot;topicA&quot;, consumerGroup = &quot;consumer2&quot;, selectorExpression = &quot;asyncSend&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">    public class AsyncSendConsumer implements RocketMQListener&lt;String&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">    </span></span>
<span class="line"><span style="color:#e1e4e8;">        private static final Logger log = LoggerFactory.getLogger(AsyncSendConsumer.class);</span></span>
<span class="line"><span style="color:#e1e4e8;">    </span></span>
<span class="line"><span style="color:#e1e4e8;">        @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">        public void onMessage(String s) {</span></span>
<span class="line"><span style="color:#e1e4e8;">            log.info(&quot;消费者成功消费消息:{}&quot;, s);</span></span>
<span class="line"><span style="color:#e1e4e8;">        }</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;</span></span>
<span class="line"><span style="color:#24292e;">    import org.apache.rocketmq.spring.core.RocketMQListener;</span></span>
<span class="line"><span style="color:#24292e;">    import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#24292e;">    import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#24292e;">    import org.springframework.stereotype.Service;</span></span>
<span class="line"><span style="color:#24292e;">    </span></span>
<span class="line"><span style="color:#24292e;">    @Service</span></span>
<span class="line"><span style="color:#24292e;">    @RocketMQMessageListener(topic = &quot;topicA&quot;, consumerGroup = &quot;consumer2&quot;, selectorExpression = &quot;asyncSend&quot;)</span></span>
<span class="line"><span style="color:#24292e;">    public class AsyncSendConsumer implements RocketMQListener&lt;String&gt; {</span></span>
<span class="line"><span style="color:#24292e;">    </span></span>
<span class="line"><span style="color:#24292e;">        private static final Logger log = LoggerFactory.getLogger(AsyncSendConsumer.class);</span></span>
<span class="line"><span style="color:#24292e;">    </span></span>
<span class="line"><span style="color:#24292e;">        @Override</span></span>
<span class="line"><span style="color:#24292e;">        public void onMessage(String s) {</span></span>
<span class="line"><span style="color:#24292e;">            log.info(&quot;消费者成功消费消息:{}&quot;, s);</span></span>
<span class="line"><span style="color:#24292e;">        }</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span></code></pre></div><h2 id="controller" tabindex="-1">controller <a class="header-anchor" href="#controller" aria-label="Permalink to &quot;controller&quot;">​</a></h2><div class="language-Plain vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">Plain</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.beans.factory.annotation.Autowired;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.web.bind.annotation.GetMapping;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.web.bind.annotation.RestController;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import java.util.HashMap;</span></span>
<span class="line"><span style="color:#e1e4e8;">import java.util.Map;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">@RestController</span></span>
<span class="line"><span style="color:#e1e4e8;">public class RocketMqController {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    private static final Logger logger = LoggerFactory.getLogger(RocketMqController.class);</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @Autowired</span></span>
<span class="line"><span style="color:#e1e4e8;">    private RocketMqServiceImpl rocketMqService;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @GetMapping(&quot;/send&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void send(){</span></span>
<span class="line"><span style="color:#e1e4e8;">        MqMsg msg = new MqMsg();</span></span>
<span class="line"><span style="color:#e1e4e8;">        msg.setTopic(&quot;topicA&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        msg.setTag(&quot;send&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        Map&lt;String, String&gt; map = new HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#e1e4e8;">        map.put(&quot;name&quot;, &quot;jack&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        map.put(&quot;age&quot;, &quot;23&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        map.put(&quot;tel&quot;, &quot;10086&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        msg.setContent(map.toString());</span></span>
<span class="line"><span style="color:#e1e4e8;">        rocketMqService.send(msg);</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    @GetMapping(&quot;/asyncSend&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void asyncSend(){</span></span>
<span class="line"><span style="color:#e1e4e8;">        MqMsg msg = new MqMsg();</span></span>
<span class="line"><span style="color:#e1e4e8;">        msg.setTopic(&quot;topicA&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        msg.setTag(&quot;asyncSend&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        Map&lt;String, String&gt; userinfo = new HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#e1e4e8;">        userinfo.put(&quot;name&quot;, &quot;lee&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        userinfo.put(&quot;age&quot;, &quot;24&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        userinfo.put(&quot;tel&quot;, &quot;10010&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">        msg.setContent(userinfo.toString());</span></span>
<span class="line"><span style="color:#e1e4e8;">        rocketMqService.asyncSend(msg);</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">import org.slf4j.Logger;</span></span>
<span class="line"><span style="color:#24292e;">import org.slf4j.LoggerFactory;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.beans.factory.annotation.Autowired;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.web.bind.annotation.GetMapping;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.web.bind.annotation.RestController;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import java.util.HashMap;</span></span>
<span class="line"><span style="color:#24292e;">import java.util.Map;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">@RestController</span></span>
<span class="line"><span style="color:#24292e;">public class RocketMqController {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    private static final Logger logger = LoggerFactory.getLogger(RocketMqController.class);</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @Autowired</span></span>
<span class="line"><span style="color:#24292e;">    private RocketMqServiceImpl rocketMqService;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @GetMapping(&quot;/send&quot;)</span></span>
<span class="line"><span style="color:#24292e;">    public void send(){</span></span>
<span class="line"><span style="color:#24292e;">        MqMsg msg = new MqMsg();</span></span>
<span class="line"><span style="color:#24292e;">        msg.setTopic(&quot;topicA&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        msg.setTag(&quot;send&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        Map&lt;String, String&gt; map = new HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#24292e;">        map.put(&quot;name&quot;, &quot;jack&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        map.put(&quot;age&quot;, &quot;23&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        map.put(&quot;tel&quot;, &quot;10086&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        msg.setContent(map.toString());</span></span>
<span class="line"><span style="color:#24292e;">        rocketMqService.send(msg);</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    @GetMapping(&quot;/asyncSend&quot;)</span></span>
<span class="line"><span style="color:#24292e;">    public void asyncSend(){</span></span>
<span class="line"><span style="color:#24292e;">        MqMsg msg = new MqMsg();</span></span>
<span class="line"><span style="color:#24292e;">        msg.setTopic(&quot;topicA&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        msg.setTag(&quot;asyncSend&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        Map&lt;String, String&gt; userinfo = new HashMap&lt;&gt;();</span></span>
<span class="line"><span style="color:#24292e;">        userinfo.put(&quot;name&quot;, &quot;lee&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        userinfo.put(&quot;age&quot;, &quot;24&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        userinfo.put(&quot;tel&quot;, &quot;10010&quot;);</span></span>
<span class="line"><span style="color:#24292e;">        msg.setContent(userinfo.toString());</span></span>
<span class="line"><span style="color:#24292e;">        rocketMqService.asyncSend(msg);</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div>`,38),t=[o];function c(r,i,g,y,d,u){return n(),e("div",null,t)}const k=s(l,[["render",c]]);export{q as __pageData,k as default};
