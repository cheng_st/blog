import{_ as s,o as n,c as a,S as e}from"./chunks/framework.d7c45c4d.js";const k=JSON.parse('{"title":"springboot集成sa-token","description":"","frontmatter":{},"headers":[],"relativePath":"guide/back/springboot/satoken.md","filePath":"guide/back/springboot/satoken.md","lastUpdated":1705138198000}'),p={name:"guide/back/springboot/satoken.md"},l=e(`<h1 id="springboot集成sa-token" tabindex="-1">springboot集成sa-token <a class="header-anchor" href="#springboot集成sa-token" aria-label="Permalink to &quot;springboot集成sa-token&quot;">​</a></h1><p>sa-token官网：<code>https://sa-token.cc/doc.html#/</code></p><h2 id="_1-maven依赖" tabindex="-1">1. maven依赖 <a class="header-anchor" href="#_1-maven依赖" aria-label="Permalink to &quot;1. maven依赖&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">&lt;!-- Sa-Token 权限认证，在线文档：https://sa-token.cc --&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;groupId&gt;cn.dev33&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;artifactId&gt;sa-token-spring-boot-starter&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">    &lt;version&gt;1.37.0&lt;/version&gt;</span></span>
<span class="line"><span style="color:#e1e4e8;">&lt;/dependency&gt;</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">&lt;!-- Sa-Token 权限认证，在线文档：https://sa-token.cc --&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;dependency&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;groupId&gt;cn.dev33&lt;/groupId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;artifactId&gt;sa-token-spring-boot-starter&lt;/artifactId&gt;</span></span>
<span class="line"><span style="color:#24292e;">    &lt;version&gt;1.37.0&lt;/version&gt;</span></span>
<span class="line"><span style="color:#24292e;">&lt;/dependency&gt;</span></span></code></pre></div><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;"># 端口</span></span>
<span class="line"><span style="color:#e1e4e8;">server.port=8081</span></span>
<span class="line"><span style="color:#e1e4e8;">    </span></span>
<span class="line"><span style="color:#e1e4e8;">############## Sa-Token 配置 (文档: https://sa-token.cc) ##############</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;"># token 名称（同时也是 cookie 名称）</span></span>
<span class="line"><span style="color:#e1e4e8;">sa-token.token-name=satoken</span></span>
<span class="line"><span style="color:#e1e4e8;"># token 有效期（单位：秒） 默认30天，-1 代表永久有效</span></span>
<span class="line"><span style="color:#e1e4e8;">sa-token.timeout=2592000</span></span>
<span class="line"><span style="color:#e1e4e8;"># token 最低活跃频率（单位：秒），如果 token 超过此时间没有访问系统就会被冻结，默认-1 代表不限制，永不冻结</span></span>
<span class="line"><span style="color:#e1e4e8;">sa-token.active-timeout=-1</span></span>
<span class="line"><span style="color:#e1e4e8;"># 是否允许同一账号多地同时登录 （为 true 时允许一起登录, 为 false 时新登录挤掉旧登录）</span></span>
<span class="line"><span style="color:#e1e4e8;">sa-token.is-concurrent=true</span></span>
<span class="line"><span style="color:#e1e4e8;"># 在多人登录同一账号时，是否共用一个 token （为 true 时所有登录共用一个 token, 为 false 时每次登录新建一个 token）</span></span>
<span class="line"><span style="color:#e1e4e8;">sa-token.is-share=true</span></span>
<span class="line"><span style="color:#e1e4e8;"># token 风格（默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik）</span></span>
<span class="line"><span style="color:#e1e4e8;">sa-token.token-style=uuid</span></span>
<span class="line"><span style="color:#e1e4e8;"># 是否输出操作日志 </span></span>
<span class="line"><span style="color:#e1e4e8;">sa-token.is-log=true</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;"># 端口</span></span>
<span class="line"><span style="color:#24292e;">server.port=8081</span></span>
<span class="line"><span style="color:#24292e;">    </span></span>
<span class="line"><span style="color:#24292e;">############## Sa-Token 配置 (文档: https://sa-token.cc) ##############</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;"># token 名称（同时也是 cookie 名称）</span></span>
<span class="line"><span style="color:#24292e;">sa-token.token-name=satoken</span></span>
<span class="line"><span style="color:#24292e;"># token 有效期（单位：秒） 默认30天，-1 代表永久有效</span></span>
<span class="line"><span style="color:#24292e;">sa-token.timeout=2592000</span></span>
<span class="line"><span style="color:#24292e;"># token 最低活跃频率（单位：秒），如果 token 超过此时间没有访问系统就会被冻结，默认-1 代表不限制，永不冻结</span></span>
<span class="line"><span style="color:#24292e;">sa-token.active-timeout=-1</span></span>
<span class="line"><span style="color:#24292e;"># 是否允许同一账号多地同时登录 （为 true 时允许一起登录, 为 false 时新登录挤掉旧登录）</span></span>
<span class="line"><span style="color:#24292e;">sa-token.is-concurrent=true</span></span>
<span class="line"><span style="color:#24292e;"># 在多人登录同一账号时，是否共用一个 token （为 true 时所有登录共用一个 token, 为 false 时每次登录新建一个 token）</span></span>
<span class="line"><span style="color:#24292e;">sa-token.is-share=true</span></span>
<span class="line"><span style="color:#24292e;"># token 风格（默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik）</span></span>
<span class="line"><span style="color:#24292e;">sa-token.token-style=uuid</span></span>
<span class="line"><span style="color:#24292e;"># 是否输出操作日志 </span></span>
<span class="line"><span style="color:#24292e;">sa-token.is-log=true</span></span></code></pre></div><h2 id="_2-创建启动类" tabindex="-1">2. 创建启动类 <a class="header-anchor" href="#_2-创建启动类" aria-label="Permalink to &quot;2. 创建启动类&quot;">​</a></h2><p>在项目中新建包 com.pj ，在此包内新建主类 SaTokenDemoApplication.java，复制以下代码：</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">@SpringBootApplication</span></span>
<span class="line"><span style="color:#e1e4e8;">public class SaTokenDemoApplication {</span></span>
<span class="line"><span style="color:#e1e4e8;">    public static void main(String[] args) throws JsonProcessingException {</span></span>
<span class="line"><span style="color:#e1e4e8;">        SpringApplication.run(SaTokenDemoApplication.class, args);</span></span>
<span class="line"><span style="color:#e1e4e8;">        System.out.println(&quot;启动成功，Sa-Token 配置如下：&quot; + SaManager.getConfig());</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">@SpringBootApplication</span></span>
<span class="line"><span style="color:#24292e;">public class SaTokenDemoApplication {</span></span>
<span class="line"><span style="color:#24292e;">    public static void main(String[] args) throws JsonProcessingException {</span></span>
<span class="line"><span style="color:#24292e;">        SpringApplication.run(SaTokenDemoApplication.class, args);</span></span>
<span class="line"><span style="color:#24292e;">        System.out.println(&quot;启动成功，Sa-Token 配置如下：&quot; + SaManager.getConfig());</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="_3-测试" tabindex="-1">3. 测试 <a class="header-anchor" href="#_3-测试" aria-label="Permalink to &quot;3. 测试&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">@RestController</span></span>
<span class="line"><span style="color:#e1e4e8;">@RequestMapping(&quot;/user/&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">public class UserController {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    // 测试登录，浏览器访问： http://localhost:8081/user/doLogin?username=zhang&amp;password=123456</span></span>
<span class="line"><span style="color:#e1e4e8;">    @RequestMapping(&quot;doLogin&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">    public String doLogin(String username, String password) {</span></span>
<span class="line"><span style="color:#e1e4e8;">        // 此处仅作模拟示例，真实项目需要从数据库中查询数据进行比对 </span></span>
<span class="line"><span style="color:#e1e4e8;">        if(&quot;zhang&quot;.equals(username) &amp;&amp; &quot;123456&quot;.equals(password)) {</span></span>
<span class="line"><span style="color:#e1e4e8;">            StpUtil.login(10001);</span></span>
<span class="line"><span style="color:#e1e4e8;">            return &quot;登录成功&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">        }</span></span>
<span class="line"><span style="color:#e1e4e8;">        return &quot;登录失败&quot;;</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    // 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin</span></span>
<span class="line"><span style="color:#e1e4e8;">    @RequestMapping(&quot;isLogin&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">    public String isLogin() {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return &quot;当前会话是否登录：&quot; + StpUtil.isLogin();</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;">    </span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">@RestController</span></span>
<span class="line"><span style="color:#24292e;">@RequestMapping(&quot;/user/&quot;)</span></span>
<span class="line"><span style="color:#24292e;">public class UserController {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    // 测试登录，浏览器访问： http://localhost:8081/user/doLogin?username=zhang&amp;password=123456</span></span>
<span class="line"><span style="color:#24292e;">    @RequestMapping(&quot;doLogin&quot;)</span></span>
<span class="line"><span style="color:#24292e;">    public String doLogin(String username, String password) {</span></span>
<span class="line"><span style="color:#24292e;">        // 此处仅作模拟示例，真实项目需要从数据库中查询数据进行比对 </span></span>
<span class="line"><span style="color:#24292e;">        if(&quot;zhang&quot;.equals(username) &amp;&amp; &quot;123456&quot;.equals(password)) {</span></span>
<span class="line"><span style="color:#24292e;">            StpUtil.login(10001);</span></span>
<span class="line"><span style="color:#24292e;">            return &quot;登录成功&quot;;</span></span>
<span class="line"><span style="color:#24292e;">        }</span></span>
<span class="line"><span style="color:#24292e;">        return &quot;登录失败&quot;;</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    // 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin</span></span>
<span class="line"><span style="color:#24292e;">    @RequestMapping(&quot;isLogin&quot;)</span></span>
<span class="line"><span style="color:#24292e;">    public String isLogin() {</span></span>
<span class="line"><span style="color:#24292e;">        return &quot;当前会话是否登录：&quot; + StpUtil.isLogin();</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;">    </span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="_4-登录鉴权" tabindex="-1">4. 登录鉴权 <a class="header-anchor" href="#_4-登录鉴权" aria-label="Permalink to &quot;4. 登录鉴权&quot;">​</a></h2><p>暴露出登录接口，其他接口参与校验:</p><div class="language-SaTokenConfigure.java vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">SaTokenConfigure.java</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">package base.config;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.interceptor.SaInterceptor;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.stp.StpUtil;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.context.annotation.Configuration;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.web.servlet.config.annotation.InterceptorRegistry;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * Created by Intellij IDEA.</span></span>
<span class="line"><span style="color:#e1e4e8;"> * User:  Administrator</span></span>
<span class="line"><span style="color:#e1e4e8;"> * Date:  2024-01-03</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">@Configuration</span></span>
<span class="line"><span style="color:#e1e4e8;">public class SaTokenConfigure implements WebMvcConfigurer {</span></span>
<span class="line"><span style="color:#e1e4e8;">    // 注册 Sa-Token 拦截器，打开注解式鉴权功能</span></span>
<span class="line"><span style="color:#e1e4e8;">    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void addInterceptors(InterceptorRegistry registry) {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        // 注册 Sa-Token 拦截器，校验规则为 StpUtil.checkLogin() 登录校验。</span></span>
<span class="line"><span style="color:#e1e4e8;">        registry.addInterceptor(new SaInterceptor(handle -&gt; StpUtil.checkLogin()))</span></span>
<span class="line"><span style="color:#e1e4e8;">                .addPathPatterns(&quot;/**&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">                .excludePathPatterns(&quot;/api/manager/login-manager&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">package base.config;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.interceptor.SaInterceptor;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.stp.StpUtil;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.context.annotation.Configuration;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.web.servlet.config.annotation.InterceptorRegistry;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * Created by Intellij IDEA.</span></span>
<span class="line"><span style="color:#24292e;"> * User:  Administrator</span></span>
<span class="line"><span style="color:#24292e;"> * Date:  2024-01-03</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">@Configuration</span></span>
<span class="line"><span style="color:#24292e;">public class SaTokenConfigure implements WebMvcConfigurer {</span></span>
<span class="line"><span style="color:#24292e;">    // 注册 Sa-Token 拦截器，打开注解式鉴权功能</span></span>
<span class="line"><span style="color:#24292e;">    @Override</span></span>
<span class="line"><span style="color:#24292e;">    public void addInterceptors(InterceptorRegistry registry) {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        // 注册 Sa-Token 拦截器，校验规则为 StpUtil.checkLogin() 登录校验。</span></span>
<span class="line"><span style="color:#24292e;">        registry.addInterceptor(new SaInterceptor(handle -&gt; StpUtil.checkLogin()))</span></span>
<span class="line"><span style="color:#24292e;">                .addPathPatterns(&quot;/**&quot;)</span></span>
<span class="line"><span style="color:#24292e;">                .excludePathPatterns(&quot;/api/manager/login-manager&quot;);</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div><h2 id="_5-前后端分离使用" tabindex="-1">5. 前后端分离使用 <a class="header-anchor" href="#_5-前后端分离使用" aria-label="Permalink to &quot;5. 前后端分离使用&quot;">​</a></h2><p>后端登录时，将token带出到前端:</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">//  so-token登录</span></span>
<span class="line"><span style="color:#e1e4e8;">StpUtil.login(manager1.getAccount());</span></span>
<span class="line"><span style="color:#e1e4e8;">SaTokenInfo tokenInfo = StpUtil.getTokenInfo();</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">//  so-token登录</span></span>
<span class="line"><span style="color:#24292e;">StpUtil.login(manager1.getAccount());</span></span>
<span class="line"><span style="color:#24292e;">SaTokenInfo tokenInfo = StpUtil.getTokenInfo();</span></span></code></pre></div><p>前端代码：</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">let token  =  localStorage.getItem(&quot;tokenValue&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">headers: {</span></span>
<span class="line"><span style="color:#e1e4e8;">	&quot;satoken&quot;: token // 在请求头中添加token</span></span>
<span class="line"><span style="color:#e1e4e8;">},</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">let token  =  localStorage.getItem(&quot;tokenValue&quot;)</span></span>
<span class="line"><span style="color:#24292e;">headers: {</span></span>
<span class="line"><span style="color:#24292e;">	&quot;satoken&quot;: token // 在请求头中添加token</span></span>
<span class="line"><span style="color:#24292e;">},</span></span></code></pre></div><h2 id="_6-配置全局过滤" tabindex="-1">6. 配置全局过滤 <a class="header-anchor" href="#_6-配置全局过滤" aria-label="Permalink to &quot;6. 配置全局过滤&quot;">​</a></h2><p>在前后端分离时，一定会有跨域的问题，前端每次请求时都会发送一个OPTIONS预检请求，因为使用了satoken的拦截，所以还需要配置一个全局过滤放掉OPTIONS之类的:</p><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#e1e4e8;">package base.config;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.SaManager;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.context.SaHolder;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.filter.SaServletFilter;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.interceptor.SaInterceptor;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.router.SaHttpMethod;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.router.SaRouter;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.stp.StpUtil;</span></span>
<span class="line"><span style="color:#e1e4e8;">import cn.dev33.satoken.util.SaResult;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.context.annotation.Bean;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.context.annotation.Configuration;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.web.servlet.config.annotation.CorsRegistry;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.web.servlet.config.annotation.InterceptorRegistry;</span></span>
<span class="line"><span style="color:#e1e4e8;">import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">/**</span></span>
<span class="line"><span style="color:#e1e4e8;"> * Created by Intellij IDEA.</span></span>
<span class="line"><span style="color:#e1e4e8;"> * User:  Administrator</span></span>
<span class="line"><span style="color:#e1e4e8;"> * Date:  2024-01-03</span></span>
<span class="line"><span style="color:#e1e4e8;"> */</span></span>
<span class="line"><span style="color:#e1e4e8;">@Configuration</span></span>
<span class="line"><span style="color:#e1e4e8;">public class SaTokenConfigure implements WebMvcConfigurer {</span></span>
<span class="line"><span style="color:#e1e4e8;">    // 注册 Sa-Token 拦截器，打开注解式鉴权功能</span></span>
<span class="line"><span style="color:#e1e4e8;">    @Override</span></span>
<span class="line"><span style="color:#e1e4e8;">    public void addInterceptors(InterceptorRegistry registry) {</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">        // 注册 Sa-Token 拦截器，校验规则为 StpUtil.checkLogin() 登录校验。</span></span>
<span class="line"><span style="color:#e1e4e8;">        registry.addInterceptor(new SaInterceptor(handle -&gt; StpUtil.checkLogin()))</span></span>
<span class="line"><span style="color:#e1e4e8;">                .addPathPatterns(&quot;/**&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                .excludePathPatterns(&quot;/api/manager/login-manager&quot;);</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">    /**</span></span>
<span class="line"><span style="color:#e1e4e8;">     * 注册 [Sa-Token 全局过滤器]</span></span>
<span class="line"><span style="color:#e1e4e8;">     */</span></span>
<span class="line"><span style="color:#e1e4e8;">    @Bean</span></span>
<span class="line"><span style="color:#e1e4e8;">    public SaServletFilter getSaServletFilter() {</span></span>
<span class="line"><span style="color:#e1e4e8;">        return new SaServletFilter()</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                // 指定 [拦截路由] 与 [放行路由]</span></span>
<span class="line"><span style="color:#e1e4e8;">                .addInclude(&quot;/**&quot;).addExclude(&quot;/favicon.ico&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                // 认证函数: 每次请求执行</span></span>
<span class="line"><span style="color:#e1e4e8;">                .setAuth(obj -&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">                    SaManager.getLog().debug(&quot;----- 请求path={}  提交token={}&quot;, SaHolder.getRequest().getRequestPath(), StpUtil.getTokenValue());</span></span>
<span class="line"><span style="color:#e1e4e8;">                    // ...</span></span>
<span class="line"><span style="color:#e1e4e8;">                })</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                // 异常处理函数：每次认证函数发生异常时执行此函数</span></span>
<span class="line"><span style="color:#e1e4e8;">                .setError(e -&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">                    return SaResult.error(e.getMessage());</span></span>
<span class="line"><span style="color:#e1e4e8;">                })</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                // 前置函数：在每次认证函数之前执行</span></span>
<span class="line"><span style="color:#e1e4e8;">                .setBeforeAuth(obj -&gt; {</span></span>
<span class="line"><span style="color:#e1e4e8;">                    SaHolder.getResponse()</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                            // ---------- 设置跨域响应头 ----------</span></span>
<span class="line"><span style="color:#e1e4e8;">                            // 允许指定域访问跨域资源</span></span>
<span class="line"><span style="color:#e1e4e8;">                            .setHeader(&quot;Access-Control-Allow-Origin&quot;, &quot;*&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">                            // 允许所有请求方式</span></span>
<span class="line"><span style="color:#e1e4e8;">                            .setHeader(&quot;Access-Control-Allow-Methods&quot;, &quot;*&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">                            // 允许的header参数</span></span>
<span class="line"><span style="color:#e1e4e8;">                            .setHeader(&quot;Access-Control-Allow-Headers&quot;, &quot;*&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">                            // 有效时间</span></span>
<span class="line"><span style="color:#e1e4e8;">                            .setHeader(&quot;Access-Control-Max-Age&quot;, &quot;3600&quot;)</span></span>
<span class="line"><span style="color:#e1e4e8;">                    ;</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">                    // 如果是预检请求，则立即返回到前端</span></span>
<span class="line"><span style="color:#e1e4e8;">                    SaRouter.match(SaHttpMethod.OPTIONS)</span></span>
<span class="line"><span style="color:#e1e4e8;">                            .free(r -&gt; System.out.println(&quot;--------OPTIONS预检请求，不做处理&quot;))</span></span>
<span class="line"><span style="color:#e1e4e8;">                            .back();</span></span>
<span class="line"><span style="color:#e1e4e8;">                })</span></span>
<span class="line"><span style="color:#e1e4e8;">                ;</span></span>
<span class="line"><span style="color:#e1e4e8;">    }</span></span>
<span class="line"><span style="color:#e1e4e8;"></span></span>
<span class="line"><span style="color:#e1e4e8;">}</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292e;">package base.config;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.SaManager;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.context.SaHolder;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.filter.SaServletFilter;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.interceptor.SaInterceptor;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.router.SaHttpMethod;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.router.SaRouter;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.stp.StpUtil;</span></span>
<span class="line"><span style="color:#24292e;">import cn.dev33.satoken.util.SaResult;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.context.annotation.Bean;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.context.annotation.Configuration;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.web.servlet.config.annotation.CorsRegistry;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.web.servlet.config.annotation.InterceptorRegistry;</span></span>
<span class="line"><span style="color:#24292e;">import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">/**</span></span>
<span class="line"><span style="color:#24292e;"> * Created by Intellij IDEA.</span></span>
<span class="line"><span style="color:#24292e;"> * User:  Administrator</span></span>
<span class="line"><span style="color:#24292e;"> * Date:  2024-01-03</span></span>
<span class="line"><span style="color:#24292e;"> */</span></span>
<span class="line"><span style="color:#24292e;">@Configuration</span></span>
<span class="line"><span style="color:#24292e;">public class SaTokenConfigure implements WebMvcConfigurer {</span></span>
<span class="line"><span style="color:#24292e;">    // 注册 Sa-Token 拦截器，打开注解式鉴权功能</span></span>
<span class="line"><span style="color:#24292e;">    @Override</span></span>
<span class="line"><span style="color:#24292e;">    public void addInterceptors(InterceptorRegistry registry) {</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">        // 注册 Sa-Token 拦截器，校验规则为 StpUtil.checkLogin() 登录校验。</span></span>
<span class="line"><span style="color:#24292e;">        registry.addInterceptor(new SaInterceptor(handle -&gt; StpUtil.checkLogin()))</span></span>
<span class="line"><span style="color:#24292e;">                .addPathPatterns(&quot;/**&quot;)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                .excludePathPatterns(&quot;/api/manager/login-manager&quot;);</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">    /**</span></span>
<span class="line"><span style="color:#24292e;">     * 注册 [Sa-Token 全局过滤器]</span></span>
<span class="line"><span style="color:#24292e;">     */</span></span>
<span class="line"><span style="color:#24292e;">    @Bean</span></span>
<span class="line"><span style="color:#24292e;">    public SaServletFilter getSaServletFilter() {</span></span>
<span class="line"><span style="color:#24292e;">        return new SaServletFilter()</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                // 指定 [拦截路由] 与 [放行路由]</span></span>
<span class="line"><span style="color:#24292e;">                .addInclude(&quot;/**&quot;).addExclude(&quot;/favicon.ico&quot;)</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                // 认证函数: 每次请求执行</span></span>
<span class="line"><span style="color:#24292e;">                .setAuth(obj -&gt; {</span></span>
<span class="line"><span style="color:#24292e;">                    SaManager.getLog().debug(&quot;----- 请求path={}  提交token={}&quot;, SaHolder.getRequest().getRequestPath(), StpUtil.getTokenValue());</span></span>
<span class="line"><span style="color:#24292e;">                    // ...</span></span>
<span class="line"><span style="color:#24292e;">                })</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                // 异常处理函数：每次认证函数发生异常时执行此函数</span></span>
<span class="line"><span style="color:#24292e;">                .setError(e -&gt; {</span></span>
<span class="line"><span style="color:#24292e;">                    return SaResult.error(e.getMessage());</span></span>
<span class="line"><span style="color:#24292e;">                })</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                // 前置函数：在每次认证函数之前执行</span></span>
<span class="line"><span style="color:#24292e;">                .setBeforeAuth(obj -&gt; {</span></span>
<span class="line"><span style="color:#24292e;">                    SaHolder.getResponse()</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                            // ---------- 设置跨域响应头 ----------</span></span>
<span class="line"><span style="color:#24292e;">                            // 允许指定域访问跨域资源</span></span>
<span class="line"><span style="color:#24292e;">                            .setHeader(&quot;Access-Control-Allow-Origin&quot;, &quot;*&quot;)</span></span>
<span class="line"><span style="color:#24292e;">                            // 允许所有请求方式</span></span>
<span class="line"><span style="color:#24292e;">                            .setHeader(&quot;Access-Control-Allow-Methods&quot;, &quot;*&quot;)</span></span>
<span class="line"><span style="color:#24292e;">                            // 允许的header参数</span></span>
<span class="line"><span style="color:#24292e;">                            .setHeader(&quot;Access-Control-Allow-Headers&quot;, &quot;*&quot;)</span></span>
<span class="line"><span style="color:#24292e;">                            // 有效时间</span></span>
<span class="line"><span style="color:#24292e;">                            .setHeader(&quot;Access-Control-Max-Age&quot;, &quot;3600&quot;)</span></span>
<span class="line"><span style="color:#24292e;">                    ;</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">                    // 如果是预检请求，则立即返回到前端</span></span>
<span class="line"><span style="color:#24292e;">                    SaRouter.match(SaHttpMethod.OPTIONS)</span></span>
<span class="line"><span style="color:#24292e;">                            .free(r -&gt; System.out.println(&quot;--------OPTIONS预检请求，不做处理&quot;))</span></span>
<span class="line"><span style="color:#24292e;">                            .back();</span></span>
<span class="line"><span style="color:#24292e;">                })</span></span>
<span class="line"><span style="color:#24292e;">                ;</span></span>
<span class="line"><span style="color:#24292e;">    }</span></span>
<span class="line"><span style="color:#24292e;"></span></span>
<span class="line"><span style="color:#24292e;">}</span></span></code></pre></div>`,21),o=[l];function t(c,r,i,y,u,g){return n(),a("div",null,o)}const h=s(p,[["render",t]]);export{k as __pageData,h as default};
