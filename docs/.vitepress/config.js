module.exports = {
	head: [
		[
			'script', { type: 'text/javascript', src: 'https://busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js' }
		]
	],
	ignoreDeadLinks: true,
	base: "/blog/",
	title: '程有财 Blog',
	description: '欢迎来到技术文档',
	lastUpdated: true,
	themeConfig: {
		logo: "/avatar.jpg",
		search: {
			provider: 'local'
		},

		// 社交账户链接
		socialLinks: [{
			icon: {
				svg: '<svg t="1671270414569" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2135" width="64" height="64"><path d="M512 0c282.784 0 512 229.216 512 512s-229.216 512-512 512S0 794.784 0 512 229.216 0 512 0z m189.952 752l11.2-108.224c-31.904 9.536-100.928 16.128-147.712 16.128-134.464 0-205.728-47.296-195.328-146.304 11.584-110.688 113.152-145.696 232.64-145.696 54.784 0 122.432 8.8 151.296 18.336L768 272.704C724.544 262.24 678.272 256 599.584 256c-203.2 0-388.704 94.88-406.4 263.488C178.336 660.96 303.584 768 535.616 768c80.672 0 138.464-6.432 166.336-16z" fill="#CE000D" p-id="2136"></path></svg>'
			},
			link: 'https://blog.csdn.net/qq_44795340?type=blog'
		}, {
			icon: 'github',
			link: 'https://github.com/183628867'
		}, {
			icon: {
				svg: '<img src="https://www.gitee.com/favicon.ico" alt="Gitee Logo" style="width:20px;">'
			},
			link: 'https://gitee.com/cheng_st'
		}],
		lastUpdatedText: "最近更新时间",
		footer: {
			message: '浙ICP备2022027593号-1',
			copyright: 'Copyright © ys99.top'
		}, 
		nav: [{
			text: "首页",
			link: "/"
		}, {
			text: '博客笔记',
			link: '/guide/',
		}, {
			text: '关于',
			link: '/my/',
		}, {
			text: '相册',
			link: '/picture/',
		}],
		// 右侧边栏标题
		outline: 'deep',
		outlineTitle: '章节导航',
		sidebar: [{
			text: '前端',
			items: [{
				text: 'vue',
				items: [{
					text: 'vue+elementui搭建后台系统',
					link: '/guide/web/vue/elementui'
				},{
					text: 'Promise使用示例',
					link: '/guide/web/vue/Promise'
				}]

			},{
				text: 'ts+vue3',
				items: [{
					text: 'vite+ts+vue3搭建后台系统',
					link: '/guide/web/ts/index'
				}]

			}],
			// collapsible: true,
			// collapsed: true
		}, {
			text: '后端',
			items: [{
					text: 'springboot',
					items: [{
						text: '1. 跨域解决',
						link: '/guide/back/springboot/cross'
					}, {
						text: '2. springboot集成RocketMQ',
						link: '/guide/back/springboot/RocketMQ'
					}, {
						text: '3. ajax上传文件',
						link: '/guide/back/springboot/file'
					}, {
						text: '4. 微信第三方平台集成公众号发送模板消息',
						link: '/guide/back/springboot/wxTemplate'
					}, {
						text: '5. 集成sa-token',
						link: '/guide/back/springboot/satoken'
					}, {
						text: '6. 集成支付宝支付',
						link: '/guide/back/springboot/aliPay'
					}]
				},
				{
					text: 'Nacos配置中心',
					link: '/guide/back/nacos'
				}, {
					text: 'mybatis-plus常用api',
					link: '/guide/back/mybatisplus'
				}
			],
			/* collapsible: true,
			collapsed: true */
		}, {
			text: '移动端',
			items: [{
					text: 'uniapp',
					// link: '/guide/mobile/uniapp',
					items: [{
						text: 'uniapp自定义组件',
						link: '/guide/mobile/uniapp/keybord'
					}, {
						text: 'uniapp上传视频到后端',
						link: '/guide/mobile/uniapp/uploadVideo'
					}]
				},
				{
					text: 'IOS',
					// link: 'guide/mobile/ios',
				},
			],
			/* collapsible: true,
			collapsed: true */
		}, {
			text: '其他',
			items: [{
				text: 'ios / 安卓手机打包',
				link: '/guide/others/pack'
			}, {
				text: 'vitepress使用卜蒜子',
				link: '/guide/others/bsz'
			},],
			/* collapsible: true,
			collapsed: true */
		}],
		carbonAds: {
			code: 'your-carbon-code',
			placement: 'your-carbon-placement'
		}
	},
	
}